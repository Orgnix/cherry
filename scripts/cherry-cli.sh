#!/bin/bash

# Use like:
# ~: scripts/cherry-cli.sh 'var_dump(\Cherry::Request()->isAjax());'

SUBSITE='default';
COMMAND='';
if [ "$1" == "-s" ]; then
  SUBSITE=$2;
  COMMAND=$3;
else
  COMMAND=$1;
fi

php -r "define('SUBSITE', '$SUBSITE'); include('web/index.php'); $COMMAND";
printf "\n";