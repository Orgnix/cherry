<?php

$cherry_settings = [];

// ---------------------------------------- //
//         Start Default subsite            //
// ---------------------------------------- //
$cherry_settings['default'] = [];
$cherry_settings['default']['url'] = 'localhost';
$cherry_settings['default']['shield'] = ['cherry' => 'cherry'];
$cherry_settings['default']['cache_backend'] = '\Cherry\Cache\Cache';
/**
 * Example of REDIS cache backend:
 *
 * $cherry_settings['default']['cache_backend'] = '\Cherry\Cache\Redis';
 * $cherry_settings['default']['redis'] = [
 *   'host' => 'localhost',
 *   'port' => '6379',
 *   'config' => [
 *     'compression' => true,
 *     'lazy' => false,
 *     'persistent' => 0,
 *     'persistent_id' => null,
 *     'tcp_keepalive' => 0,
 *     'timeout' => 30,
 *     'read_timeout' => 0,
 *     'retry_interval' => 0,
 *   ],
 * ];
 */
$cherry_settings['default']['database'] = [
  'type' => 'mysql',
  'hostname' => 'hostname',
  'username' => 'username',
  'database' => 'database',
  'password' => 'password',
  'port' => '3306',
];
$scheme = $_SERVER['HTTP_X_FORWARDED_PROTO']
    ?? $_SERVER['REQUEST_SCHEME']
    ?? 'http';
$host = $_SERVER['HTTP_X_FORWARDED_HOST']
    ?? $_SERVER['HTTP_HOST']
    ?? $_SERVER['SERVER_NAME']
    ?? 'localhost';
$port = $_SERVER['HTTP_X_FORWARDED_PORT']
    ?? $_SERVER['SERVER_PORT']
    ?? 80;
$port = $port !== 80 && strpos($host, ':') === FALSE ? ':' . $port : '';
$cherry_settings['default']['root'] = [
    'folder' => __DIR__,
    'web' => [
        'url' => $scheme . '://' . $host . $port,
        'root' => '/',
    ],
];
$cherry_settings['default']['config'] = [
  'folder' => __DIR__ . '/config',
];
$cherry_settings['default']['themes'] = [
  'folder' => __DIR__ . '/web/themes',
  'url' => $cherry_settings['default']['root']['web']['url'] . '/themes',
];
$cherry_settings['default']['files'] = [
  'public' => $cherry_settings['default']['root']['web']['url'] . '/files/default/public',
  'private' => $cherry_settings['default']['root']['web']['url'] . '/files/default/private',
];
$cherry_settings['default']['debugging'] = FALSE;
$cherry_settings['default']['twig_debugging'] = FALSE;
$cherry_settings['default']['development'] = FALSE;
// ---------------------------------------- //
//           End Default subsite            //
// ---------------------------------------- //