<?php

use Cherry\Cache\CacheInterface;
use Cherry\Cache\KillSwitch;
use Cherry\Container\PrepareBuilder;
use Cherry\Cookie;
use Cherry\Core;
use Cherry\Database\Database;
use Cherry\Element\ElementManager;
use Cherry\Entity\EntityInterface;
use Cherry\Entity\EntityTypeManager;
use Cherry\Entity\EntityRenderer;
use Cherry\Event\Event;
use Cherry\Event\EventInterface;
use Cherry\Event\EventManager;
use Cherry\File\Filesystem;
use Cherry\Form\Form;
use Cherry\Logger\BrokenLogger;
use Cherry\Logger\LoggerInterface;
use Cherry\Manifest\Manifest;
use Cherry\Manifest\ManifestInterface;
use Cherry\Person\Entity\Person;
use Cherry\Person\Entity\PersonInterface;
use Cherry\Request;
use Cherry\Route\Route;
use Cherry\Route\RouteInterface;
use Cherry\Settings;
use Cherry\Translation\TranslationTempStorage;
use Cherry\Utils\Strings\StringManipulation;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie as SymfonyCookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Cherry helper class.
 */
class Cherry {

  /**
   * The container object.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  private static ContainerInterface $container;

  /**
   * The database object.
   *
   * @var \Cherry\Database\Database
   */
  private static Database $database;

  /**
   * The Cache killswitch.
   *
   * @var \Cherry\Cache\KillSwitch
   */
  private static KillSwitch $killSwitch;

  /**
   * The current request.
   *
   * @var \Cherry\Request
   */
  public static Request $request;

  /**
   * Keeps statistics on how many times a certain query has been used.
   *
   * @var array
   */
  public static array $queries = [];

  /**
   * Whether the container has been initialized or not.
   *
   * @var bool
   */
  public static bool $initialized = FALSE;

  /**
   * Returns Cache object
   *
   * @return CacheInterface|bool
   */
  public static function Cache() {
    return $GLOBALS['cache'] ?? FALSE;
  }

  /**
   * Returns static Container object
   *
   * @return ContainerInterface
   */
  public static function Container(): ContainerInterface {
    return static::$container;
  }

  /**
   * Returns new Cookie object
   *
   * @param \Symfony\Component\HttpFoundation\Cookie|null $cookie
   *
   * @return Cookie
   */
  public static function Cookie(?SymfonyCookie $cookie = NULL): Cookie {
    return Core::loadClass(Cookie::class, [$cookie]);
  }

  /**
   * Returns current Person object
   *
   * @return bool|PersonInterface
   *
   * @throws \Exception
   */
  public static function CurrentPerson() {
    return Person::load(Person::getCurrentPerson());
  }

  /**
   * Returns the current route object.
   *
   * @return RouteInterface
   */
  public static function CurrentRoute(): RouteInterface {
    return Route::getCurrent();
  }

  /**
   * Returns cached Database object
   *
   * @param string|null $database
   *
   * @return Database
   *
   * @throws \Exception
   */
  public static function Database(?string $database = NULL): Database {
    if (!is_null($database)) {
      $object = static::Container()->get('database');
      if (!$object instanceof Database) {
        throw new Exception('The database object could not be initialized.');
      }
      $object->setDatabaseName($database);
      return $object;
    }
    return static::$database;
  }

  /**
   * Returns ElementManager object
   *
   * @return ElementManager
   */
  public static function ElementManager(): ElementManager {
    return static::Container()->get('element.manager');
  }

  /**
   * Returns EntityTypeManager object.
   *
   * @return EntityTypeManager
   */
  public static function EntityTypeManager(): EntityTypeManager {
    return Core::loadClass(EntityTypeManager::class);
  }

  /**
   * Returns EntityRenderer object
   *
   * @param EntityInterface $entity
   *
   * @return EntityRenderer
   */
  public static function EntityRenderer(EntityInterface $entity): EntityRenderer {
    return Core::loadClass(EntityRenderer::class, [$entity]);
  }

  /**
   * Returns Event object
   *
   * @param string $eventName
   *
   * @return EventInterface
   */
  public static function Event(string $eventName): EventInterface {
    return Core::loadClass(Event::class, [$eventName]);
  }

  /**
   * Returns Filesystem object
   *
   * @param array|null $available
   *
   * @return Filesystem
   */
  public static function Filesystem(?array $available = NULL): Filesystem {
    return new Filesystem($available);
  }

  /**
   * Returns Form object
   *
   * @param EntityInterface|null $entity
   *
   * @return Form
   */
  public static function Form(EntityInterface $entity = NULL): Form {
    return new Form($entity);
  }

  /**
   * Returns whether Cherry is fully initialized (for edge cases where
   * items need to run very early on in the bootstrapping process)
   *
   * @return bool
   */
  public static function isInitialized(): bool {
    return static::$initialized;
  }

  /**
   * Returns KillSwitch object
   *
   * @return KillSwitch
   */
  public static function KillSwitch(): KillSwitch {
    return static::$killSwitch;
  }

  /**
   * Returns Logger object
   *
   * @return LoggerInterface
   */
  public static function Logger(): LoggerInterface {
    if (!static::isInitialized()) {
      return Core::loadClass(BrokenLogger::class);
    }
    return static::Container()->get('logger');
  }

  /**
   * Returns Manifest object
   *
   * @param string $type
   *
   * @return ManifestInterface
   */
  public static function Manifest(string $type): ManifestInterface {
    return Core::loadClass(Manifest::class, [$type]);
  }

  /**
   * Returns the static request object.
   *
   * @return \Cherry\Request
   */
  public static function Request(): Request {
    return static::$request;
  }

  /**
   * Initialize Cherry's dependencies
   *
   * @throws Exception
   */
  public static function Init(Request $request) {
    $core = new Core();
    $core->setSystemSpecifics();
    static::$request = $request;

    // Initialize Database.
    static::$database = Core::loadClass(Database::class);

    // Initialize Container.
    $builder = new ContainerBuilder();
    $prepareBuilder = Core::loadClass(PrepareBuilder::class, [$builder]);
    $prepareBuilder->prepare();
    static::$container = $prepareBuilder->getContainer();

    // Initialize Cache and KillSwitch.
    $GLOBALS['cache'] = Core::getCacheClass(static::Container());
    $GLOBALS['cache']->initializeCache();
    $request->attributes->set('cache', $GLOBALS['cache']);
    static::$killSwitch = Core::loadClass(KillSwitch::class);
    static::$initialized = TRUE;

    // Set list of events.
    EventManager::setEventListeners();
    // Set tempstore UUID.
    TranslationTempStorage::$uuid = Core::createUUID();

    // If person has an active session cookie, set session.
    $person_cookie = static::Cookie()->setValues('PersonID')->get();
    if (!empty($person_cookie) && $person_cookie !== '0') {
      Person::changeTo((int) $person_cookie, TRUE);

      // Show PHPINFO if the URL contains it and user is logged in.
      if (StringManipulation::contains($request->getRequestUri(), 'phpinfo')) {
        phpinfo();
        exit;
      }
    }

    // Set current route.
    $uri = StringManipulation::replace($request->getUri(), Settings::get('root.web.url'), '');
    $route = static::Container()->get('route.manager')->routeMatch($uri);
    if (!$route instanceof RouteInterface) {
      if ($route instanceof RedirectResponse) {
        return $route;
      }
      $route = static::Container()->get('route')->load('dashboard');
    }
    Route::setCurrent($route);

    // If form post data exists, submit form.
    if ($request->request->has('form-id')) {
      /**
       * @var \Cherry\Form\FormManager
       *   The form manager.
       */
      $formManager = static::Container()->get('form.manager');

      $formId = $request->request->get('form-id');
      $entityType = $request->request->get('entity-type') ?? NULL;
      $entityId = $request->request->get('entity-id') ?? NULL;
      /**
       * @var \Cherry\Form\Form $object
       *   The form object.
       */
      $object = $formManager->get($formId, $entityType, $entityId);
      if ($object instanceof Form) {
        $form = $object->build();
        $object->submit($form, $formId);
      } else {
        static::Logger()->addError('Unable to build form for form id ' . $formId, 'bootstrap');
      }
    }

    static::Event('postInit')->fireWithoutReference(static::Request());
    return TRUE;
  }

  /**
   * Bootstraps Cherry/
   */
  public static function Bootstrap(Request $request) {
    $init = static::Init($request);
    if ($init instanceof RedirectResponse) {
      $init->send();
      exit;
    }
    $route = static::CurrentRoute();

    // Render rest api calls without rendering everything else.
    if ($route->isRest()) {
      $route->render();
      exit;
    }

    try {
      $variables = [];
      $variables['elements'] = [];
      $variables['elements']['navbar'] = [];
      $variables['elements']['page_top'] = [];

      $variables['current_route'] = $route->getRoute();
      $variables['page'] = [
        'title' => $route->getObject()->get('title') ?: NULL,
        'author' => $route->getObject()->get('author') ?: NULL,
        'summary' => $route->getObject()->get('summary') ?: NULL,
      ];

      $header = static::ElementManager()->getElementRender('header', $variables, $route);
      $content = static::ElementManager()->getElementRender('content', $variables, $route);
      $footer = static::ElementManager()->getElementRender('footer', $variables, $route);

      $response = new Response();
      $response->headers->set('Content-Type', 'text/html');
      $response->setContent($header . $content . $footer);
      $response->setStatusCode(Response::HTTP_OK);
      static::Event('preSend')->fire($response);
      $response->prepare($request);
      $response->send();
    }
    catch (Exception $exception) {
      static::Logger()->addFailure('Could not render Cherry.' . PHP_EOL . $exception->getMessage(), 'Bootstrap', $exception->getTraceAsString());
    }
  }

}
