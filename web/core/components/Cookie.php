<?php

namespace Cherry;

use \Symfony\Component\HttpFoundation\Cookie as SymfonyCookie;

/**
 * Class Cookie
 *
 * @package Cherry
 */
class Cookie {

  /** @var string $name */
  protected string $name;

  /** @var string|null $value */
  protected ?string $value;

  /** @var string|null $path */
  protected ?string $path;

  /** @var string|null $domain */
  protected ?string $domain;

  /** @var int $expire */
  protected int $expire;

  /**
   * Cookie constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Cookie|null $cookie
   */
  public function __construct(?SymfonyCookie $cookie = NULL) {
    if (is_null($cookie)) {
      return;
    }

    $this->setValues(
      $cookie->getName(),
      $cookie->getValue(),
      $cookie->getPath(),
      $cookie->getDomain(),
      $cookie->getExpiresTime()
    );
  }

  /**
   * Sets cookie's values
   *
   * @param string|array $name
   * @param string|null  $value
   * @param string|null  $path
   * @param string|null  $domain
   * @param int          $expire
   *
   * @return $this
   */
  public function setValues($name, ?string $value = '', ?string $path = '', ?string $domain = '', int $expire = 0): self {
    if (is_array($name)) {
      if (!isset($name['name']) || !isset($name['value']) || !isset($name['path']) || !isset($name['domain']) || !isset($name['expire'])) {
        $this->name = $name['name'];
        $this->value = $name['value'];
        $this->path = $name['path'];
        $this->domain = $name['domain'];
        $this->expire = $name['expire'];
      }
    } else {
      $this->name = $name;
      $this->value = $value;
      $this->path = $path;
      $this->domain = $domain;
      $this->expire = $expire;
    }

    return $this;
  }

  /**
   * Returns cookie
   *
   * @return mixed
   */
  public function get() {
    return $_COOKIE[$this->getValue('name')] ?? NULL;
  }

  /**
   * Sets cookie
   *
   * @return bool
   */
  public function set(): bool {
    return setcookie(
      $this->getValue('name'),
      $this->getValue('value'),
      $this->getValue('expire'),
      $this->getValue('path'),
      $this->getValue('domain')
    );
  }

  /**
   * Returns cookie parameter
   *
   * @param $key
   *
   * @return null
   */
  public function getValue($key) {
    return $this->{$key} ?? NULL;
  }

  /**
   * Remove cookie
   *
   * @param $key
   */
  public function remove($key) {
    if (!isset($_COOKIE[$key])) {
      return;
    }
    unset($_COOKIE[$key]);
  }

}