<?php

namespace Cherry\Entity;

use BadMethodCallException;

/**
 * Allows an entity to use bundles.
 */
trait EntityBundleableTrait {

  /**
   * Returns entity bundle
   *
   * @return string
   */
  public function bundle() {
    if (!method_exists($this, 'getValue')) {
      throw new BadMethodCallException('This entity does not seem to have the getValue method.');
    }
    return $this->getValue('bundle') ?: 'default';
  }

  /**
   * Returns bundle field.
   *
   * @return array[]
   */
  protected static function addBundleField() {
    return [
      'bundle' => [
        'type' => 'varchar',
        'length' => 25,
        'default_value' => 'default',
        'form' => [
          'title' => 'Bundle',
          'type' => 'hidden',
          'name' => 'bundle',
          'options' => [],
        ],
      ],
    ];
  }

}