<?php

namespace Cherry\Entity\Entity;

use Cherry\Entity\Entity;

/**
 * Class Bundle
 *
 * @package Cherry\Entity\Entity
 */
class Bundle extends Entity implements BundleInterface {

  /**
   * Bundle constructor.
   *
   * @param array|null $values
   */
  public function __construct(?array $values = NULL) {
    $this->setValues($values);
    $this->setType('bundle');
  }

  /**
   * @return array
   */
  public static function initialFields(): array {
    return [
      'entity_type' => [
        'type' => 'varchar',
        'length' => 255,
      ],
      'machine_name' => [
        'type' => 'varchar',
        'length' => 255,
        'form' => [
          'title' => 'Machine name',
          'type' => 'textbox',
          'name' => 'machine_name',
          'required' => TRUE,
        ],
      ],
      'label' => [
        'type' => 'varchar',
        'length' => 255,
        'form' => [
          'title' => 'Label',
          'type' => 'textbox',
          'name' => 'label',
          'required' => TRUE,
        ],
      ],
      'fields' => [
        'type' => 'blob',
        'form' => [
          'title' => 'Fields',
          'type' => 'CreateFields',
          'name' => 'fields',
          'multiple' => TRUE,
          'required' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityType(): string {
    return $this->getValue('entity_type');
  }

  /**
   * {@inheritDoc}
   */
  public function getMachineName(): string {
    return $this->getValue('machine_name');
  }

  /**
   * {@inheritDoc}
   */
  public function setMachineName(string $machine_name) {
    return $this->setValue('machine_name', $machine_name);
  }

  /**
   * {@inheritDoc}
   */
  public function getLabel(): string {
    return $this->getValue('label');
  }

  /**
   * {@inheritDoc}
   */
  public function setLabel(string $title) {
    return $this->setValue('label', $title);
  }

  /**
   * {@inheritDoc}
   */
  public function getBundleFields(): array {
    return unserialize($this->getValue('fields'));
  }

  /**
   * {@inheritDoc}
   */
  public function setBundleFields(array $fields) {
    return $this->setValue('title', serialize($fields));
  }

}
