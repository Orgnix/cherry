<?php

namespace Cherry\Entity\Entity;

use Cherry\Entity\EntityInterface;

/**
 * Interface BundleInterface
 *
 * @package Cherry\Entity\Entity
 */
interface BundleInterface extends EntityInterface {

  /**
   * Gets the entity type.
   *
   * @return string
   */
  public function getEntityType(): string;

  /**
   * Gets bundle machine name
   *
   * @return string
   */
  public function getMachineName();

  /**
   * Sets bundle machine name
   *
   * @param string $machine_name
   *
   * @return bool
   */
  public function setMachineName(string $machine_name);

  /**
   * Gets bundle label
   *
   * @return string
   */
  public function getLabel();

  /**
   * Sets bundle label
   *
   * @param string $title
   *
   * @return bool
   */
  public function setLabel(string $title);

  /**
   * Gets bundle fields
   *
   * @return array
   */
  public function getBundleFields();

  /**
   * Sets bundle fields
   *
   * @param array $fields
   *
   * @return bool
   */
  public function setBundleFields(array $fields);

}