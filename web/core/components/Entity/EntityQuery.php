<?php

namespace Cherry\Entity;

use BadMethodCallException;
use Cherry\Database\Database;
use Cherry\Utils\ClassStepTrait;

/**
 * Class EntityQuery.
 *
 * @package Cherry\Entity;
 */
class EntityQuery implements EntityQueryInterface {
  use ClassStepTrait;

  /**
   * The database object.
   *
   * @var Database
   */
  protected Database $database;

  /**
   * The entity type.
   *
   * @var string
   */
  protected string $entityType;

  /**
   * Conditions to filter the entities on.
   *
   * @var array
   */
  protected array $conditions = [];

  /**
   * Amount of entities to retrieve.
   *
   * @var array
   */
  protected array $limit = [];

  /**
   * Sort by given field and direction.
   *
   * @var array
   */
  protected array $sort = [];

  /**
   * EntityQuery constructor.
   *
   * @param Database $database
   */
  public function __construct(Database $database) {
    $this->database = $database;
  }

  /**
   * Sets the entity type.
   *
   * @param string $entity_type
   *
   * @return self
   */
  public function type(string $entity_type): self {
    if ($this->step('entityType')) {
      throw new BadMethodCallException('This method has already been called.');
    }

    $this->entityType = $entity_type;

    return $this;
  }

  /**
   * Returns the entity type.
   *
   * @return string
   */
  protected function getType(): string {
    return $this->entityType;
  }

  /**
   * {@inheritDoc}
   */
  public function condition(string $field, $value, string $operator = '='): self {
    if (!$this->step('entityType')) {
      throw new BadMethodCallException('You have to set the entity type before setting a condition.');
    }

    $this->conditions[] = [
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function exists(string $field): self {
    if (!$this->step('entityType')) {
      throw new BadMethodCallException('You have to set the entity type before checking if a field exists.');
    }

    return $this->condition($field, NULL, 'IS NOT NULL');
  }

  /**
   * {@inheritDoc}
   */
  public function notExists(string $field): self {
    if (!$this->step('entityType')) {
      throw new BadMethodCallException('You have to set the entity type before checking if a field exists.');
    }

    return $this->condition($field, NULL, 'IS NULL');
  }

  /**
   * {@inheritDoc}
   */
  public function limit($offset, $limit): self {
    if (!$this->step('entityType')) {
      throw new BadMethodCallException('You have to set the entity type before setting a limit.');
    }

    $this->limit = [
      'offset' => $offset,
      'limit' => $limit,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function sort(string $field, string $direction): self {
    if (!$this->step('entityType')) {
      throw new BadMethodCallException('You have to set the entity type before setting a sorting method.');
    }

    $this->sort = [
      'field' => $field,
      'direction' => $direction,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function execute() {
    if (!$this->step('entityType')) {
      throw new BadMethodCallException('You have to set the entity type before executing the query.');
    }
    if (!$this->step('conditions')) {
      throw new BadMethodCallException('You have to set at least one condition before executing the query.');
    }

    $query = $this->database
      ->select('entity__' . $this->getType(), 'e')
      ->fields('e', ['id']);
    foreach ($this->conditions as $condition) {
      $query->condition($condition['field'], $condition['value'], $condition['operator']);
    }
    if (!empty($this->limit)) {
      $query->limit($this->limit['offset'], $this->limit['limit']);
    }
    if (!empty($this->sort)) {
      $query->orderBy($this->sort['field'], $this->sort['direction']);
    }
    $result = $query->execute();
    return $result->fetchAll() ? array_map(function($item) {
      return $item['id'];
    }, $result->fetchAll()) : [];
  }

}
