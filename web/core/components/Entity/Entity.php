<?php

namespace Cherry\Entity;

use Cherry\Bags\ValueBag;
use Cherry\Database\Database;
use Cherry\Database\Result;
use Cherry\Person\Entity\Person;
use Cherry\Translation\StringTranslation;
use Cherry\Utils\Classes\ArrayableClassTrait;
use Cherry\Utils\Strings\StringManipulation;

/**
 * Class Entity
 *
 * @package Cherry\Entity
 */
class Entity implements EntityInterface {
  use ValueBag;
  use ArrayableClassTrait;
  use StringTranslation;

  /** Constants for status values */
  const UNPUBLISHED = 0;
  const PUBLISHED = 1;
  /** Constants for cardinality */
  const CARDINALITY_UNLIMITED = 0;
  const CARDINALITY_DEFAULT = 25;

  /** @var string $type */
  protected string $type;

  /** @var array|bool $fields */
  protected $fields = FALSE;

  protected array $permissions = [];

  /**
   * @param int $id
   * @param bool $massage
   *
   * @return EntityInterface|bool
   *
   * @throws \Exception
   */
  public static function load(int $id, bool $massage = FALSE) {
    $entityClassName = static::class;
    /** @var EntityInterface $entity */
    $entity = new $entityClassName;

    // Sometimes ID could be 0, and the type not equal to person
    // This is an invalid entity and can't be returned.
    if ($id === 0 && $entity->getType() !== 'person') {
      return FALSE;
    }

    return \Cherry::EntityTypeManager()
      ->getStorage($entity->getType())
      ->load(
        $id,
        $massage
      );
  }

  /**
   * Loads all entities of entity type
   *
   * @param array $ids
   * @param bool $massage
   *
   * @return array|bool
   *
   * @throws \Exception
   */
  public static function loadMultiple(array $ids = [], bool $massage = FALSE) {
    $entityClassName = static::class;
    /** @var EntityInterface $entity */
    $entity = new $entityClassName;
    return \Cherry::EntityTypeManager()
      ->getStorage($entity->getType())
      ->loadMultiple(
        $ids,
        $massage
      );
  }

  /**
   * {@inheritDoc}
   */
  public function isPublished(): bool {
    return ((int) $this->getValue('status') === 1);
  }

  public function setStatus(int $status): EntityInterface {
    return $this->setValue('status', $status);
  }

  /**
   * Creates the entity type in database
   *
   * @return bool
   */
  public static function create(): bool {
    $entityClassName = static::class;
    /** @var EntityInterface $entity */
    $entity = new $entityClassName;
    $type = $entity->getType();
    if (EntityTypeManager::entityInstalled($type)) {
      return FALSE;
    }
    $database = \Cherry::Database();
    $results = [];
    $auto_increment = FALSE;
    $fields_storage = Entity::fields() + static::initialFields();
    $fields = '';
    foreach ($fields_storage as $field => $options) {
      if (!in_array(strtoupper($options['type']), Database::getFieldTypes())) {
        \Cherry::Logger()->addWarning('Field type ' . $options['type'] . ' does not comply to possible field types.', 'Entity');
      }
      if ($fields !== '') {
        $fields .= ',' . PHP_EOL;
      }
      $fields .= '  ' . Database::createFieldQuery($field, $options);

      if (isset($options['auto_increment']) && $options['auto_increment'] !== FALSE) {
        $auto_increment = $field;
      }
    }

    $fullClassName = StringManipulation::explode($entityClassName, '\\');
    $className = array_pop($fullClassName);
    $database->insert('entity_storage')
      ->values([
        'type' => $type,
        'label' => $className,
        'description' => 'This item creates the ' . $className . ' entity.',
        'fields' => serialize(static::initialFields()),
      ])
      ->execute();
    $query = $database->query('CREATE TABLE `entity__' . $type . '` (
' . $fields . '
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
    if (!$results[] = $query) {
      \Cherry::Logger()->addFailure('Something went wrong while querying the create function.', 'Entity');
    }

    if ($auto_increment !== FALSE) {
      $add_primary_key = $database->query('ALTER TABLE `entity__' . $type . '`
ADD PRIMARY KEY (`' . $auto_increment . '`);');
      $results[] = $add_primary_key;
      $add_auto_increment = $database->query('ALTER TABLE `entity__' . $type . '`
  MODIFY ' . Database::createFieldQuery($auto_increment, $fields_storage[$auto_increment]) . ' AUTO_INCREMENT;');
      $results[] = $add_auto_increment;
    }

    foreach ($results as $result) {
      if (!$result) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function massageProperties(array $entity, bool $massage = TRUE) {
    if ($massage) {
      foreach ($entity as $ci_key => $ci_value) {
        if ($ci_key === 'status') {
          $entity[$ci_key] = static::intToStatus($ci_value);
          continue;
        } elseif ($ci_key === 'id' || !isset(static::fields()[$ci_key]['class'])) {
          continue;
        }
        $className = static::fields()[$ci_key]['class'];
        $class = new $className;
        $entity[$ci_key] = $class::load($ci_value);
      }
    }
    $entityClass = \Cherry::EntityTypeManager()::getEntityClassFromType($this->getType());
    if ($entityClass !== FALSE) {
      return new $entityClass($entity);
    }

    return FALSE;
  }

  /**
   * @param string|int $int
   *
   * @return string
   */
  public static function intToStatus($int): string {
    // Cast to int
    $int = (int) $int;

    $statuses = [
      0 => translate('Unpublished'),
      1 => translate('Published'),
      2 => translate('Draft'),
      3 => translate('Under review'),
    ];

    \Cherry::Event('alterStatusList')
      ->fire($statuses);

    return $statuses[$int] ?? translate('Undefined');
  }

  /**
   * {@inheritDoc}
   */
  public static function fields(): array {
    return [
      'id' => [
        'type' => 'int',
        'length' => 25,
        'unique' => TRUE,
      ],
      'owner' => [
        'type' => 'int',
        'length' => 25,
        'default_value' => Person::getCurrentPerson(),
        'class' => '\\Cherry\\Person\\Entity\\Person',
      ],
      'status' => [
        'type' => 'tinyint',
        'length' => 1,
        'default_value' => self::UNPUBLISHED,
        'form' => [
          'title' => 'Status',
          'type' => 'select',
          'name' => 'status',
          'options' => [
            self::PUBLISHED => 'Published',
            self::UNPUBLISHED => 'Unpublished',
          ],
        ],
      ],
    ];
  }

  /**
   * Returns bundle options.
   *
   * @return array
   */
  public function bundleOptions() {
    $bundle_storage = \Cherry::EntityTypeManager()
        ->getStorage('bundle')
        ->loadByProperties(['entity_type' => $this->getType()], TRUE) ?? [];

    $bundles = [];
    /** @var \Cherry\Entity\Entity\BundleInterface $bundle */
    foreach ($bundle_storage as $bundle) {
      $bundles[$bundle->getMachineName()] = $bundle->getLabel();
    }

    return $bundles;
  }

  /**
   * {@inheritDoc}
   */
  public function hasField(string $field) {
    $fields = $this->getAllFields();
    return isset($fields[$field]);
  }

  /**
   * {@inheritDoc}
   */
  public function getType(): ?string {
    return $this->type;
  }

  /**
   * Sets entity type
   *
   * @param $type
   *
   * @return Entity
   */
  public function setType($type): self {
    // Set default permissions for this entity.
    $this->addPermission('view', 'view ' . $type . ' entities');
    $this->addPermission('edit', 'edit ' . $type . ' entities');
    $this->addPermission('delete', 'delete ' . $type . ' entities');
    $this->addPermission('translate', 'translate ' . $type . ' entities');

    $this->type = $type;
    return $this;
  }

  /**
   * @return array
   */
  public function getPermissions(): array {
    return $this->permissions;
  }

  /**
   * @param string $action
   * @param string $permission
   *
   * @return $this
   */
  public function addPermission(string $action, string $permission): self {
    $this->permissions[$action] = $permission;

    return $this;
  }

  /**
   * Adds options to default field options
   *
   * @param array  $default_fields
   * @param string $field
   * @param array  $options
   *
   * @return array
   */
  public function addToDefaultOptions(array $default_fields, string $field, $options = []): array {
    $fields = $default_fields;
    foreach ($options as $key => $value) {
      if ($fields[$field]['form']['type'] !== 'select') {
        continue;
      }
      $fields[$field]['form']['options'][$key] = $value;
    }

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getAllFields() {
    if (!$this->getType()) {
      return FALSE;
    }

    $fields_storage = \Cherry::Database()
      ->select('entity_storage')
      ->fields(NULL, ['fields'])
      ->condition('type', $this->getType());
    /** @var Result $fields_result */
    if (!$fields_result = $fields_storage->execute()) {
      return FALSE;
    }

    $fields = $fields_result->fetchAllAssoc();
    return self::fields() + unserialize($fields[0]['fields']);
  }

  /**
   * Validates data.
   * Will also serialize/decode data if needed.
   *
   * @return bool
   */
  protected function validate() {
    foreach ($this->values as $field => $value) {
      $fields = $this->getAllFields();
      if (!isset($fields[$field])) {
        \Cherry::Logger()->addError($this->translate('Attempting to save field on entity that does not exist. @field', [
          '@field' => $field,
        ]));
        return FALSE;
      }

      if (isset($field['serialize']) && $field['serialize'] === TRUE) {
        $this->values[$field] = serialize($value);
      }
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function save(): bool {
    // Fire presave event
    \Cherry::Event('EntityPreSave')
      ->fire($this);

    if (!$this->validate()) {
      return FALSE;
    }

    $table = 'entity__' . $this->type;
    // Check if item exists, update existing item or insert new item.
    if ($this->id() !== NULL && $this->id() !== 0) {
      $check = \Cherry::Database()->select($table)
        ->condition('id', $this->id());
      if (!$result = $check->execute()) {
        \Cherry::Logger()->addFailure('Something went wrong trying to execute query', 'Entity');
        return FALSE;
      }
      if (!$result->fetchAllAssoc()) {
        $check_existing = \Cherry::Database()->select($table);
        foreach ($this->getUniqueFields() as $field) {
          $check_existing->condition($field, $this->getValue($field));
        }
        if (!$result_existing = $check_existing->execute()) {
          \Cherry::Logger()->addFailure('Something went wrong trying to execute Entity Save query', 'Entity');
          return FALSE;
        }
        if ($result = $result_existing->fetchAllAssoc()) {
          \Cherry::Logger()->addFailure('Some unique field already exists during Entity Save Query.', 'Entity');
          return FALSE;
        }

        $values = $this->massageValueArray();
        $query = \Cherry::Database()
          ->insert($table)
          ->values($values);
      } else {
        $values = $this->massageValueArray();
        $query = \Cherry::Database()->update($table)
          ->condition('id', $this->id())
          ->values($values);
      }
    } else {
      if (!$result = $this->addEntity($this->getType())) {
        kint($result);
        \Cherry::Logger()->addFailure('Something went wrong trying to execute Entity Save query', 'Entity');
        return FALSE;
      }
      $id = \Cherry::Database()->select('INFORMATION_SCHEMA.TABLES')
        ->fields(NULL, ['AUTO_INCREMENT'])
        ->condition('TABLE_SCHEMA', \Cherry::Database()->getDatabaseName())
        ->condition('TABLE_NAME', 'entity');
      if (!$id_result = $id->execute()) {
        \Cherry::Logger()->addFailure('Something went wrong trying to execute Entity Save query', 'Entity');
        return FALSE;
      }
      $result = $id_result->fetchAllAssoc();
      // Autoincrement ID (next ID) minus 1 equals current ID
      $id = reset($result)['AUTO_INCREMENT'] - 1;
      $values = ['id' => $id] + $this->massageValueArray();
      $query = \Cherry::Database()
        ->insert($table)
        ->values($values);
    }

    if (!$query->execute()) {
      return FALSE;
    }

    // Set the ID in the current object.
    if (isset($id)) {
      $this->setValue('id', $id);
    }

    // Fire postsave event
    \Cherry::Event('EntityPostSave')
      ->fire($this);

    \Cherry::Cache()->invalidateTags(['entity:' . $this->type . ':overview']);

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function id() {
    return (int)$this->getValue('id');
  }

  /**
   * @return array
   */
  protected function getUniqueFields(): array {
    $fields = $this->getAllFields();
    $unique_fields = [];
    foreach ($fields as $field => $options) {
      if (isset($options['unique']) && $options['unique']) {
        $unique_fields[] = $field;
      }
    }
    return $unique_fields;
  }

  /**
   * Massages value array to comply with fields order
   *
   * @return array
   */
  protected function massageValueArray(): array {
    $values = $this->getValues();
    $fields = $this->getAllFields();
    $new_value_array = [];
    foreach ($fields as $field => $options) {
      if ($field === 'id') {
        continue;
      }
      if (!isset($values[$field])) {
        continue;
      } elseif ($values[$field] === FALSE) {
        $values[$field] = 0;
      } elseif ($values[$field] instanceof EntityInterface) {
        $values[$field] = $values[$field]->id();
      }

      $new_value_array[$field] = $values[$field];
    }
    return $new_value_array;
  }

  /**
   * Default function to return title, to be overwritten in entities
   *
   * @return null
   */
  public function getTitle() {
    return NULL;
  }

  /**
   * @param string $type
   *
   * @return bool|Result
   */
  protected function addEntity(string $type) {
    $query = \Cherry::Database()->insert('entity')
      ->values([
        'id' => 0,
        'type' => $type,
      ]);
    return $query->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function delete(): bool {
    if ($this->id() == NULL) {
      \Cherry::Logger()->addFailure('Cannot remove Entity without ID', 'Entity');
      return FALSE;
    }

    // Fire predelete event
    \Cherry::Event('EntityPreDelete')
      ->fire($this);

    $query = \Cherry::Database()
      ->delete('entity__' . $this->getType())
      ->condition('id', $this->id());
    $query_entity = \Cherry::Database()
      ->delete('entity')
      ->condition('id', $this->id());

    if (!$query->execute() || !$query_entity->execute()) {
      \Cherry::Logger()->addFailure('Something went wrong trying to remove Entity ' . $this->getType() . ' [' . $this->id() . ']', 'Entity');
      return FALSE;
    }

    // Fire EntityPostDelete event
    \Cherry::Event('EntityPostDelete')
      ->fire($this);
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function owner() {
    return Person::load($this->getValue('owner'));
  }
}