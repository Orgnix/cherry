<?php

namespace Cherry\Entity;

/**
 * Interface EntityStorageInterface.
 *
 * Defines EntityStorage objects.
 */
interface EntityStorageInterface {

  /**
   * Loads entities of the type by given properties.
   *
   * @param array $properties
   *          An array of properties your Entity should have
   * @param bool $multiple
   *          If you expect multiple results, set this to TRUE
   * @param bool $massage
   *          Whether the values should be massaged, defaults to true.
   * @param array|null $order
   *          The order in which to load them (Should be an array with keys 'field' and 'direction').
   *
   * @return bool|EntityInterface|array
   *
   * @throws \Exception
   */
  public function loadByProperties(array $properties = [], bool $multiple = FALSE, bool $massage = TRUE, ?array $order = NULL);

  /**
   * Loads all entities of entity type or the given ids.
   *
   * @param array $ids
   * @param bool $massage
   *
   * @return array|bool
   *
   * @throws \Exception
   */
  public function loadMultiple(array $ids = [], bool $massage = FALSE);

  /**
   * Loads entity of entity type by a given id.
   *
   * @param int $id
   * @param bool $massage
   *
   * @return array|bool
   *
   * @throws \Exception
   */
  public function load(int $id, bool $massage = FALSE);

}
