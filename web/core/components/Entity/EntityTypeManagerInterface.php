<?php

namespace Cherry\Entity;

/**
 * Defines entity type managers functionality.
 */
interface EntityTypeManagerInterface {

  /**
   * Uninstalls entity type from database, along with all entities for that type.
   *
   * @param string $type
   *   The entity type.
   *
   * @return bool
   *
   * @throws \Exception
   */
  public static function uninstallEntityType(string $type): bool;

  /**
   * Creates entities on bootstrapping Cherry
   */
  public function createEntities();

  /**
   * Removes field from entity
   *
   * @param EntityInterface $entity
   * @param string $field
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function removeEntityField(EntityInterface $entity, string $field): bool;

  /**
   * Updates entity fields with initial ones.
   */
  public function updateEntities();

  /**
   * @return array
   */
  public static function getAllEntities(): array;

  /**
   * Checks whether entity is installed
   *
   * @param string $type
   *   Machine readable label of entity type.
   *
   * @return bool
   *
   * @throws \Exception
   */
  public static function entityInstalled(string $type): bool;

  /**
   * Returns entity class from a given type id.
   *
   * @param string $type
   *
   * @return EntityInterface|bool
   */
  public static function getEntityClassFromType(string $type);

  /**
   * Returns entity's type from given id.
   *
   * @param int $id
   *
   * @return string|null
   *
   * @throws \Exception
   */
  public static function getEntityTypeFromId(int $id): ?string;

  /**
   * Returns EntityStorage object
   *
   * @param string|null $type
   *
   * @return EntityStorage
   */
  public function getStorage(?string $type = NULL): EntityStorage;

}
