<?php

namespace Cherry\Entity;

/**
 * Defines the Entity Cloning interface.
 */
interface EntityCloneInterface {

  /**
   * Returns the cloned entity.
   *
   * @return \Cherry\Entity\EntityInterface
   */
  public function getClonedEntity(): EntityInterface;

  /**
   * Sets the entity to be cloned.
   *
   * @param \Cherry\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return self
   */
  public function setEntity(EntityInterface $entity): self;

  /**
   * Clones the entity.
   *
   * @return bool|self
   */
  public function clone();

}
