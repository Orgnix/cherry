<?php

namespace Cherry\Entity;

/**
 * Interface EntityInterface
 *
 * @package Cherry\Entity
 */
interface EntityInterface {

  /**
   * Returns the extra fields over the initial fields.
   *
   * @return array
   */
  public static function fields(): array;

  /**
   * Checks whether entity has certain field.
   *
   * @param string $field
   *
   * @return mixed
   */
  public function hasField(string $field);

  /**
   * Returns all fields of current entity type.
   *
   * @return bool|array
   */
  public function getAllFields();

  /**
   * Returns current entity's type.
   *
   * @return string|NULL
   */
  public function getType(): ?string;

  /**
   * Attempts to save current entity.
   *
   * @return bool
   */
  public function save(): bool;

  /**
   * Massages the entity properties.
   *
   * @param array $entity
   * @param bool $massage
   *
   * @return mixed
   */
  public function massageProperties(array $entity, bool $massage = TRUE);

  /**
   * Checks whether entity is published.
   *
   * @return bool
   */
  public function isPublished(): bool;

  /**
   * Sets the entity status to a given value.
   *
   * @param int $status
   *
   * @return $this
   */
  public function setStatus(int $status): self;

  /**
   * Attempts to delete current entity
   *
   * @return bool
   */
  public function delete(): bool;

  /**
   * Returns the id of the current entity
   *
   * @return int|string|NULL
   */
  public function id();

  /**
   * Returns the owner of the current entity
   *
   * @return EntityInterface|bool
   */
  public function owner();

  /**
   * Inherited from: ValueBag
   *
   * Returns all values.
   *
   * @return array
   */
  public function getValues(): array;

  /**
   * Inherited from: ValueBag
   *
   * Returns value for given key if it exists, or false if it doesn't.
   *
   * @param string|int $key
   *
   * @return mixed
   */
  public function getValue($key);

  /**
   * Inherited from: ValueBag
   *
   * Sets all values.
   *
   * @param array|null $values
   *
   * @return self
   */
  public function setValues(?array $values): self;

  /**
   * Inherited from: ValueBag
   *
   * Returns bool, checks if values exists in bag.
   *
   * @param $key
   *
   * @return bool
   */
  public function hasValue($key): bool;

  /**
   * Inherited from: ValueBag
   *
   * Sets a single value.
   *
   * @param $key
   * @param $value
   *
   * @return self
   */
  public function setValue($key, $value): self;

}