<?php

namespace Cherry\Entity;

interface EntityQueryInterface {

  /**
   * Sets a condition to filter the entities on.
   *
   * @param string $field
   * @param mixed  $value
   * @param string $operator
   *
   * @return self
   */
  public function condition(string $field, $value, string $operator = '='): self;

  /**
   * Checks whether value exists in entity.
   *
   * @param string $field
   *
   * @return self
   */
  public function exists(string $field): self;

  /**
   * Checks whether value doesn't exist in entity.
   *
   * @param string $field
   *
   * @return self
   */
  public function notExists(string $field): self;

  /**
   * Amount of entities to retrieve.
   *
   * @param string|int $offset
   * @param string|int $limit
   *
   * @return self
   */
  public function limit($offset, $limit): self;

  /**
   * Sort on a given field and direction.
   *
   * @param string $field
   * @param string $direction
   *
   * @return self
   */
  public function sort(string $field, string $direction): self;

  /**
   * Executes query and returns IDs of eligible entities.
   */
  public function execute();

}
