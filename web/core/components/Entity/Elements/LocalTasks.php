<?php

namespace Cherry\Entity\Elements;

use Cherry;
use Cherry\Element\Element;
use Cherry\Route\RouteInterface;

/**
 * Class LocalTasks
 *
 * @package Cherry\Entity\Elements
 */
class LocalTasks extends Element {

  /**
   * LocalTasks constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameter('id', 'local-tasks');
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'element.local-tasks',
      'context' => 'element',
      'tags' => ['element:local-tasks'],
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    if (parent::render() === NULL) {
      return NULL;
    }

    if (!$this->hasParameter('entity')) {
      return '';
    }

    /** @var Cherry\Entity\EntityInterface $entity */
    $entity = $this->get('entity');

    $tasks = [
      'entity.view' => [
        'weight' => 10,
        'parameters' => [
          'eid' => $entity->id(),
          'type' => $entity->getType(),
        ],
        'active_class' => 'btn-primary',
        'classes' => ['btn'],
        'label' => $this->translate('View')->__toString(),
      ],
      'entity.edit' => [
        'weight' => 20,
        'parameters' => [
          'eid' => $entity->id(),
          'type' => $entity->getType(),
        ],
        'active_class' => 'btn-primary',
        'classes' => ['btn'],
        'label' => $this->translate('Edit')->__toString(),
      ],
      'entity.clone' => [
        'weight' => 30,
        'parameters' => [
          'eid' => $entity->id(),
          'type' => $entity->getType(),
        ],
        'active_class' => 'btn-primary',
        'classes' => ['btn'],
        'label' => $this->translate('Clone')->__toString(),
      ],
      'entity.delete' => [
        'weight' => 40,
        'parameters' => [
          'eid' => $entity->id(),
          'type' => $entity->getType(),
        ],
        'active_class' => 'btn-danger',
        'classes' => ['btn'],
        'label' => $this->translate('Delete')->__toString(),
      ],
    ];

    // Send an event so the tasks can be altered
    \Cherry::Event('localTasksAlter')->fire($tasks);

    // Sort the tasks by weight
    foreach ($tasks as $route => &$options) {
      if (\Cherry::CurrentRoute()->getRoute() === $route) {
        $options['classes'][] = $options['active_class'];
      } else {
        $options['classes'][] = 'btn-secondary';
      }
      uasort($tasks[$route], function ($a, $b) {
        $weight1 = $a['weight'] ?? 0;
        $weight2 = $b['weight'] ?? 0;
        return $weight1 <=> $weight2;
      });
    }

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('local-tasks')
      ->render(['tasks' => $tasks]);
  }

}
