<?php

namespace Cherry\Entity;

/**
 * Allows an entity to use bundles.
 */
interface EntityBundleableInterface {

  /**
   * Returns entity bundle
   *
   * @return string
   */
  public function bundle();

}