<?php

namespace Cherry\Entity;

use Cherry;
use Cherry\Core;
use Cherry\Entity\EntityInterface;
use Cherry\Event\EventListener;
use Twig\TwigFunction;

/**
 * Class Events
 *
 * @package Cherry\Entity
 */
class Events extends EventListener {

  /**
   * {@inheritDoc}
   */
  public function twigExtensionsAlter(?array &$functions) {
    $functions[] = new TwigFunction('entityField', [$this, 'getEntityField'], ['is_safe' => ['html']]);
  }

  /**
   * Returns entity field
   *
   * @param EntityInterface $entity
   * @param string          $field
   *
   * @return array|string|NULL
   */
  public function getEntityField(EntityInterface $entity, string $field) {
    return $entity->getValue($field);
  }

}
