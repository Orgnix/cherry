<?php

namespace Cherry\Entity\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Entity\EntityInterface;
use Cherry\Entity\EntityTypeManager;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Overview
 *
 * @package Cherry\Entity\Pages
 */
class Overview extends Page {

  /**
   * Overview constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.overview',
      'title' => $this->translate('Entity overview'),
      'summary' => $this->translate('List of existing entities.'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['view all ' . $this->get('type') . ' entities']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.entity.overview',
      'context' => $this->get('viewmode') ?: 'table',
      'tags' => [],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    if ($this->hasParameter('type')) {
      $this->caching['key'] = 'page.entity.' . $this->get('type') . '.overview';
      $this->caching['tags'] = ['entity:' . $this->get('type') . ':overview'];
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    $entity_class = EntityTypeManager::getEntityClassFromType($this->get('type'));

    if (!$entity_class instanceof EntityInterface) {
      throw new \InvalidArgumentException('Entity type ' . $this->get('type') . ' does not exist.');
    }

    $conditional_field = NULL;
    if ($entity_class->hasField('title')) {
      $conditional_field = 'title';
    } elseif ($entity_class->hasField('label')) {
      $conditional_field = 'label';
    } elseif ($entity_class->hasField('name')) {
      $conditional_field = 'name';
    }

    $manifest = \Cherry::Manifest($this->get('type'))->fields([
      'id', $conditional_field, 'status', 'owner'
    ]);
    $content = \Cherry::Container()->get('manifest.renderer')
      ->setManifest($manifest)
      ->setViewMode($this->get('viewmode') ?: 'table')
      ->hideField('id')
      ->noLink('owner')
      ->addActionLinks('entity')
      ->render(TRUE);

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('overview')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'entity' => [
          'type' => $this->get('type'),
          'content' => $content,
        ],
      ]);
  }

}
