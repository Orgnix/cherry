<?php

namespace Cherry\Entity\Pages;

use Cherry;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class EntityList
 *
 * @package Cherry\Entity\Pages
 */
class EntityList extends Page {

  /**
   * {@inheritDoc}
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.list',
      'title' => $this->translate('Entity list'),
      'summary' => $this->translate('List page for all entities'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['manage entities']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.entity.list',
      'context' => 'page',
      'tags' => ['entity:list'],
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    $entities = \Cherry::EntityTypeManager()::getAllEntities();

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('list')
      ->render(['entities' => $entities]);
  }

}
