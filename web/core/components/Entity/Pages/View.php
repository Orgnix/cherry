<?php

namespace Cherry\Entity\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Entity\EntityInterface;
use Cherry\Entity\EntityRenderer;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class View
 *
 * @package Cherry\Entity\Pages
 */
class View extends Page {

  /**
   * View constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.view',
      'title' => $this->translate('Entity'),
      'summary' => $this->translate('The view page of an entity.'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['view ' . $this->get('type') . ' entity']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.entity.view',
      'context' => 'page',
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    if (!$this->hasParameter('eid') || empty($this->get('eid'))) {
      return $this;
    }

    /** @var EntityInterface $entityObject */
    $entityObject = \Cherry::EntityTypeManager()::getEntityClassFromType($this->get('type'));
    if (!$entityObject instanceof EntityInterface) {
      return $this;
    }
    /** @var EntityInterface $entity */
    $entity = $entityObject::load($this->get('eid'));
    if (method_exists($entity, 'getName')) {
      $title = $entity->getName();
    } elseif (method_exists($entity, 'getTitle')) {
      $title = $entity->getTitle();
    } else {
      $title = $entity->getType();
    }

    $this->setParameter('title', $title);
    $this->caching['key'] = $this->caching['key'] . '.' . $entity->id();
    $this->caching['tags'] = ['entity:' . $entity->getType() . ':' . $entity->id()];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    if (!$this->hasParameter('eid') || empty($this->get('eid'))) {
      return NULL;
    }

    /** @var EntityInterface $entityObject */
    $entityObject = \Cherry::EntityTypeManager()::getEntityClassFromType($this->get('type'));
    if (!$entityObject instanceof EntityInterface) {
      return NULL;
    }
    $entity = $entityObject::load($this->get('eid'));
    $entityRenderer = \Cherry::EntityRenderer($entity);
    $content = $entityRenderer->render();

    $tasks = \Cherry::ElementManager()
      ->getElementRender('local-tasks', ['entity' => $entity], $this->getRoute());

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('view')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'entity' => [
          'id' => $this->hasParameter('eid') ? $this->get('eid') : NULL,
          'type' => $entity instanceof EntityInterface ? $entity->getType() : NULL,
          'content' => $content,
        ],
        'local_tasks' => $tasks,
      ]);
  }

}
