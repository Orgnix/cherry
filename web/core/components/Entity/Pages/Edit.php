<?php

namespace Cherry\Entity\Pages;

use Cherry;
use Cherry\Entity\EntityInterface;
use Cherry\Form\Form;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Edit
 *
 * @package Cherry\Entity\Pages
 */
class Edit extends Page {

  /**
   * Entity constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.edit',
      'title' => $this->translate('Entity edit'),
      'summary' => $this->translate('Edit page for an entity'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['edit ' . $this->get('type') . ' entity']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.entity.edit',
      'context' => 'page',
      'max-age' => 0,
    ];

    if ($this->hasParameter('eid') && !empty($this->get('eid'))) {
      /** @var EntityInterface $entityObject */
      $entityObject = \Cherry::EntityTypeManager()::getEntityClassFromType($this->get('type'));
      if (!$entityObject instanceof EntityInterface) {
        return $this;
      }
      $entity = $entityObject::load($this->get('eid'));
      $this->setParameter('title', $this->translate('Edit :title', [':title' => $entity->getTitle()]));
      $this->caching['key'] .= '.' . $entity->id();
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    if (!$this->hasParameter('eid') || empty($this->get('eid'))) {
      return NULL;
    }

    /** @var EntityInterface $entityObject */
    $entityObject = \Cherry::EntityTypeManager()::getEntityClassFromType($this->get('type'));
    if (!$entityObject instanceof EntityInterface) {
      return $this;
    }
    $entity = $entityObject::load($this->get('eid'));

    $tasks = \Cherry::ElementManager()
      ->getElementRender('local-tasks', ['entity' => $entity], $this->getRoute());

    $form = new Form($entity);
    $content = $form->result();

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('edit')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'entity' => [
          'id' => $this->hasParameter('eid') ? $this->get('eid') : NULL,
          'type' => $entity instanceof EntityInterface ? $entity->getType() : NULL,
          'content' => $content,
        ],
        'local_tasks' => $tasks,
      ]);
  }

}
