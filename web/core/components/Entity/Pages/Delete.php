<?php

namespace Cherry\Entity\Pages;

use Cherry\Entity\EntityInterface;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Delete
 *
 * @package Cherry\Entity\Pages
 */
class Delete extends Page {

  /**
   * Entity constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.delete',
      'title' => $this->translate('Entity Delete'),
      'summary' => $this->translate('Delete page for an entity'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['delete ' . $this->get('type') . ' entity']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.entity.delete',
      'context' => 'page',
      'max-age' => 0,
    ];

    if (isset($parameters[2]) && !empty($parameters[2])) {
      /** @var EntityInterface $entityObject */
      $entityObject = \Cherry::EntityTypeManager()::getEntityClassFromType($parameters[1]);
      if (!$entityObject instanceof EntityInterface) {
        return $this;
      }
      $entity = $entityObject::load($parameters[2]);
      $this->setParameter('title', $this->translate('Delete :title', [':title' => $entity->getTitle()]));
      $this->caching['key'] = $this->caching['key'] . '.' . $entity->getType() . '.' . $entity->id();
      $this->caching['max-age'] = 0;
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    if (!$this->hasParameter('eid') || empty($this->get('eid'))) {
      return NULL;
    }

    /** @var EntityInterface $entity */
    $entity = \Cherry::EntityTypeManager()
      ->getStorage($this->hasParameter('type') ? $this->get('type') : NULL)
      ->loadByProperties([
        'id' => $this->get('eid'),
      ], FALSE);

    if (!$entity instanceof EntityInterface) {
      return NULL;
    }

    if ($this->hasParameter('confirm')) {
      $type = $entity->getType();
      $entity->delete();
      $route = $this->getRoute()
        ->load('entity.overview')
        ->setValue('type', $type);
      $this->redirect($route);
    }

    $tasks = \Cherry::ElementManager()
      ->getElementRender('local-tasks', ['entity' => $entity], $this->getRoute());

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('delete')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'entity' => [
          'id' => $this->hasParameter('eid') ? $this->get('eid') : NULL,
          'type' => $entity->getType() ,
        ],
        'local_tasks' => $tasks,
      ]);
  }

}
