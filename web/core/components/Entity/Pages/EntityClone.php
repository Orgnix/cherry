<?php

namespace Cherry\Entity\Pages;

use Cherry;
use Cherry\Entity\EntityInterface;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Edit
 *
 * @package Cherry\Entity\Pages
 */
class EntityClone extends Page {

  /**
   * Entity constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.clone',
      'title' => $this->translate('Entity clone'),
      'summary' => $this->translate('Clone page for an entity'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['clone ' . $this->get('type') . ' entity']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.entity.clone',
      'context' => 'page',
      'max-age' => 0,
    ];

    if ($this->hasParameter('eid') && !empty($this->get('eid'))) {
      /** @var EntityInterface $entityObject */
      $entityObject = \Cherry::EntityTypeManager()::getEntityClassFromType($this->get('type'));
      if (!$entityObject instanceof EntityInterface) {
        return $this;
      }
      $entity = $entityObject::load($this->get('eid'));
      $this->setParameter('title', $this->translate('Clone :title', [':title' => $entity->getTitle()]));
      $this->caching['key'] .= '.' . $entity->id();
      $this->caching['max-age'] = 0;
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    if (!$this->hasParameter('eid') || empty($this->get('eid'))) {
      return NULL;
    }

    /** @var EntityInterface $entityObject */
    $entityObject = \Cherry::EntityTypeManager()::getEntityClassFromType($this->get('type'));
    if (!$entityObject instanceof EntityInterface) {
      return $this;
    }
    $entity = $entityObject::load($this->get('eid'));

    $tasks = \Cherry::ElementManager()
      ->getElementRender('local-tasks', ['entity' => $entity], $this->getRoute());

    $variables = [
      'page' => [
        'id' => $this->get('id'),
        'title' => $this->get('title'),
        'summary' => $this->get('summary'),
      ],
      'entity' => [
        'id' => $this->hasParameter('eid') ? $this->get('eid') : NULL,
        'type' => $entity instanceof EntityInterface ? $entity->getType() : NULL,
      ],
      'local_tasks' => $tasks,
    ];

    if ($this->hasParameter('confirm')) {
      /** @var Cherry\Entity\EntityClone $entityClone */
      $entityClone = \Cherry::Container()->get('entity.clone');

      $clonedEntity = $entityClone
        ->setEntity($entity)
        ->clone()
        ->getClonedEntity();

      /** @var RouteInterface $route */
      $route = \Cherry::Container()->get('route');
      return $this->redirect($route->load('entity.view')
        ->setValue('eid', $clonedEntity->id())
        ->setValue('type', $clonedEntity->getType()));
    }

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('clone')
      ->render($variables);
  }

}
