<?php

namespace Cherry\Entity\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Add
 *
 * @package Cherry\Entity\Pages
 */
class Add extends Page {

  /**
   * Edit constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.add',
      'title' => $this->translate('Add entity'),
      'summary' => $this->translate('Add page for an entity'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['add ' . $this->get('type') . ' entity']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.entity.add',
      'context' => 'entity_type:' . $this->get('type'),
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    if ($this->getRoute()->getRoute() === 'entity.add') {
      $this->caching['context'] .= ':' . $this->get('bundle');
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    /** @var \Cherry\Entity\EntityInterface $entity */
    $entity = \Cherry::EntityTypeManager()::getEntityClassFromType($this->get('type'));
    // Decide which form should be used based on current route
    switch ($this->getRoute()->getRoute()) {
      case 'entity.add':
        // Set entity bundle based on route parameter
        $entity->setValue('bundle', $this->get('bundle'));
        $form = \Cherry::Form($entity);
        break;
      case 'entity.choose_bundle':
      default:
        if ($entity->getType() === 'bundle') {
          /** @var Cherry\Route\RouteInterface $route */
          $route = \Cherry::Container()->get('route')
            ->load('entity.add')
            ->setValue('type', 'bundle')
            ->setValue('bundle', 'default');
          $this->redirect($route);
        }
        /** @var \Cherry\Form\FormManager $form_manager */
        $form_manager = \Cherry::Container()->get('form.manager');
        $form = $form_manager->get('entity-choose-bundle-form', $entity);
        break;
    }
    $content = $form->result();

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('add')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'entity' => [
          'type' => $entity->getType(),
          'content' => $content,
        ],
      ]);
  }

}
