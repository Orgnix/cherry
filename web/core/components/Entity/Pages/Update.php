<?php

namespace Cherry\Entity\Pages;

use Cherry;
use Cherry\Page\Page;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;
use Cherry\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class Update
 *
 * @package Cherry\Entity\Pages
 */
class Update extends Page {

  /**
   * Update constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.update',
      'title' => $this->translate('Entity update'),
      'summary' => $this->translate('Update entities.'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['manage entities']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.entity.update',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    \Cherry::EntityTypeManager()->updateEntities();

    $this->redirect($this->hasParameter('destination')
      ? urldecode($this->get('destination'))
      : Url::fromRoute(\Cherry::Container()->get('route')->load('dashboard')));
  }

}
