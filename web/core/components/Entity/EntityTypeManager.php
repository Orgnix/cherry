<?php

namespace Cherry\Entity;

use Cherry\Database\Result;
use Cherry\Logger\Logger;
use Cherry\Translation\StringTranslation;
use Cherry\YamlReader;

/**
 * Class EntityTypeManager
 *
 * @package Cherry\Entity
 */
class EntityTypeManager implements EntityTypeManagerInterface {
  use StringTranslation;

  /**
   * {@inheritDoc}
   */
  public static function uninstallEntityType(string $type): bool {
    if (!self::entityInstalled($type)) {
      return FALSE;
    }
    \Cherry::Database()->delete('entity')
      ->condition('type', $type)
      ->execute();
    \Cherry::Database()->delete('entity_storage')
      ->condition('type', $type)
      ->execute();
    \Cherry::Database()->query('DROP TABLE entity__' . $type);
    \Cherry::Logger()->addInfo('Uninstalled ' . ucfirst($type) . ' entity type and entities', Logger::TYPE_INFO, ucfirst($type));

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function createEntities() {
    // Create tables for Entities.
    $entities = self::getAllEntities();
    foreach ($entities as $entity => $info) {
      if (self::entityInstalled($entity)) {
        unset($entities[$entity]);
      }
    }

    foreach ($entities as $entity => $info) {
      $object = new $info['class'];
      \Cherry::Logger()->addInfo($this->translate(':entity is not an instance of EntityInterface', [':entity' => $entity]), 'EntityTypeManager');
      if (!$object instanceof EntityInterface) {
        continue;
      }

      if (!method_exists($object, 'create')) {
        continue;
      }

      $object::create();
      \Cherry::Logger()->addInfo($this->translate('Created entity :entity', [':entity' => $entity]), 'EntityTypeManager');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function removeEntityField(EntityInterface $entity, string $field): bool {
    $type = $entity->getType();
    $type_storage = \Cherry::Database()->removeField('entity__' . $type, $field);
    $entity_storage_fields = \Cherry::Database()
      ->select('entity_storage')
      ->condition('entity', $type)
      ->fields(NULL, ['fields'])
      ->execute();
    if (!$entity_storage_fields instanceof Result) {
      return FALSE;
    }

    $results = $entity_storage_fields->fetchAllAssoc();
    $result = reset($results);
    $result['fields'] = unserialize($result['fields']);
    if (isset($result['fields'][$field])) {
      unset($result['fields'][$field]);
    }
    $result['fields'] = serialize($result['fields']);

    $update_storage_fields = \Cherry::Database()
      ->update('entity_storage')
      ->condition('entity', $type)
      ->values(['fields' => $result['fields']])
      ->execute();

    if (!$type_storage || $update_storage_fields) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function updateEntities() {
    $entities = self::getAllEntities();
    $updated_entities = [];
    foreach ($entities as $entity => $info) {
      if (!self::entityInstalled($entity)) {
        continue;
      }

      $object = new $info['class'];
      if (!$object instanceof EntityInterface) {
        continue;
      }

      $serialized_fields = serialize($object::initialFields());
      $stored_fields_storage = \Cherry::Database()->select('entity_storage')
        ->condition('type', $entity)
        ->fields(NULL, ['fields'])
        ->execute();
      if (!$stored_fields_storage instanceof Result) {
        \Cherry::Logger()->addError($this->translate('Could not retrieve fields storage information for :entity', [':entity' => $entity]), 'EntityTypeManager');
        continue;
      }
      $results = $stored_fields_storage->fetchAllAssoc();
      $result = reset($results);
      if (isset($result['fields']) && $result['fields'] === $serialized_fields) {
        continue;
      }

      foreach ($object::initialFields() as $field => $options) {
        $field_storage = \Cherry::Database()->field('entity__' . $entity, $field, $options);
        if (!$field_storage) {
          \Cherry::Logger()->addError($this->translate('Something went wrong trying to add/modify field [:field]', [':field' => $field]), 'EntityTypeManager');
          continue;
        }
      }

      $storage_query = \Cherry::Database()->update('entity_storage')
        ->condition('type', $entity)
        ->values([
          'fields' => serialize($object::initialFields()),
        ])
        ->execute();
      if (!$storage_query) {
        \Cherry::Logger()->addError($this->translate('Something went wrong trying to add/modify field [:field] to entity_storage', [':field' => $field]), 'EntityTypeManager');
        continue;
      }

      \Cherry::Logger()->addInfo($this->translate('Updated entity storage for entity :entity', [':entity' => $entity]), 'EntityTypeManager');
      $updated_entities[] = $entity;
    }

    if (count($updated_entities) === 0) {
      \Cherry::Logger()->addInfo($this->translate('No entities in need of an update.'), 'EntityTypeManager');
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getAllEntities(): array {
    $entities = [];
    $extensions = \Cherry::Container()->get('extension.manager')->getInstalledExtensions();
    foreach ($extensions as $extension) {
      $extensionInfo = YamlReader::readExtension($extension->getExtension());
      $componentInfo = YamlReader::readComponent($extension->getExtension());
      if (!is_array($extensionInfo) && !is_array($componentInfo)) {
        \Cherry::Logger()->addFailure($extension->getExtension() . ' entry exists in database but no extension info file is found.', 'EntityTypeManager');
        continue;
      }
      $info = is_array($extensionInfo) ? $extensionInfo : $componentInfo;
      if (!isset($info['entities'])) {
        continue;
      }

      foreach ($info['entities'] as $entity => $info) {
        $entities[$entity] = $info;
      }
    }
    return $entities;
  }

  /**
   * {@inheritDoc}
   */
  public static function entityInstalled(string $type): bool {
    $database = \Cherry::Database();
    $type = strtolower($type);
    $entity = $database
      ->select('entity__' . $type)
      ->execute();
    if (!$entity instanceof Result) {
      return FALSE;
    }
    $entity_storage = $database
      ->select('entity_storage')
      ->condition('type', $type)
      ->execute();
    if (!$entity_storage instanceof Result) {
      return FALSE;
    }
    if ($result = $entity_storage->fetchAllAssoc()) {
      if (count($result) > 0) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public static function getEntityClassFromType(string $type) {
    $entities = self::getAllEntities();
    if (!isset($entities[$type]['class'])) {
      return FALSE;
    }
    return new $entities[$type]['class'];
  }

  /**
   * {@inheritDoc}
   */
  public static function getEntityTypeFromId(int $id): ?string {
    $query = \Cherry::Database()
      ->select('entity')
      ->condition('id', $id)
      ->fields(NULL, ['type']);

    if (!$result = $query->execute()) {
      return FALSE;
    }

    if (!$result instanceof Result) {
      return FALSE;
    }

    $items = $result->fetchAllAssoc();
    $item = reset($items);

    return $item['type'] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getStorage(?string $type = NULL): EntityStorage {
    return new EntityStorage($type);
  }

}