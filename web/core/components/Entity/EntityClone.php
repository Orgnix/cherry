<?php

namespace Cherry\Entity;


/**
 * Class EntityClone
 *
 * @package Cherry\Entity
 */
class EntityClone implements EntityCloneInterface {

  /** @var EntityInterface $entity */
  protected EntityInterface $entity;

  /** @var EntityInterface $clonedEntity */
  protected EntityInterface $clonedEntity;

  /**
   * {@inheritDoc}
   */
  public function getClonedEntity(): EntityInterface {
    if (!$this->clonedEntity) {
      throw new \BadMethodCallException('You need to clone the entity before you can request the cloned entity.');
    }
    return $this->clonedEntity;
  }

  /**
   * Sets Entity object
   *
   * @param EntityInterface $entity
   *
   * @return EntityClone
   */
  protected function setClonedEntity(EntityInterface $entity): self {
    $this->clonedEntity = $entity;

    return $this;
  }

  /**
   * Returns Entity object
   *
   * @return EntityInterface
   */
  protected function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritDoc}
   */
  public function setEntity(EntityInterface $entity): self {
    $this->entity = $entity;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function clone() {
    if (!\Cherry::CurrentPerson()->hasPermission('clone entity')) {
      \Cherry::Logger()->addWarning('You do not have the permission to clone entities.', 'EntityClone');
      return FALSE;
    }

    $values = $this->getEntity()->getValues();
    $type = $this->getEntity()->getType();

    if (isset($values['id'])) {
      unset($values['id']);
    }

    /** @var EntityInterface $clonedEntity */
    $class = \Cherry::EntityTypeManager()::getEntityClassFromType($type);
    $clonedEntity = new $class($values);
    try {
      $clonedEntity->save();
    } catch(\Exception $e) {
      \Cherry::Logger()->addError($e->getMessage(), 'EntityClone');
    }

    $this->setClonedEntity($clonedEntity);
    return $this;
  }

}
