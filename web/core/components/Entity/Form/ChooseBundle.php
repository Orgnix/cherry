<?php

namespace Cherry\Entity\Form;

use Cherry;
use Cherry\Entity\EntityInterface;
use Cherry\Form\Form;
use Cherry\Form\FormInterface;
use Cherry\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ChooseBundle
 *
 * @package Cherry\Entity\Form
 */
class ChooseBundle extends Form implements FormInterface {

  /**
   * ChooseBundle constructor.
   *
   * @param EntityInterface|null $entity
   */
  public function __construct(EntityInterface $entity) {
    parent::__construct($entity);
    $this->setId('entity-choose-bundle-form');

    $bundle_storage = \Cherry::EntityTypeManager()
        ->getStorage('bundle')
        ->loadByProperties(['entity_type' => $entity->getType()], TRUE) ?? [];

    $bundles = [];
    /** @var Cherry\Entity\Entity\BundleInterface $bundle */
    foreach ($bundle_storage as $bundle) {
      $bundles[$bundle->getMachineName()] = $bundle->getLabel();
    }

    $fields = [
      'bundle' => [
        'form' => [
          'type' => 'select',
          'title' => $this->translate('Bundle'),
          'options' => $bundles,
        ],
      ],
    ];

    $submit = [
      'submit' => [
        'form' => [
          'type' => 'button',
          'text' => $this->translate('Create'),
          'attributes' => [
            'type' => 'submit',
          ],
          'classes' => [
            'btn-success'
          ],
          'handler' => [$this, 'submitForm'],
        ],
      ],
    ];

    $fields = [
      'fields' => $fields,
      'submit' => $submit,
    ];

    // Submit handler gets added at the end so it doesn't get surpassed by other fields
    \Cherry::Event('entityChooseBundleForm')
      ->fire($fields, [$this->getId()]);

    $fields = $fields['fields'] + $fields['submit'];

    $this->setFields($fields);
  }

  /**
   * Submits choose bundle form.
   *
   * @param array $form
   * @param array $values
   */
  public function submitForm(array $form = [], array $values = []) {
    /** @var Cherry\Route\RouteInterface $route */
    $route = \Cherry::Container()->get('route')
      ->load('entity.add')
      ->setValue('type', $this->getEntity()->getType())
      ->setValue('bundle', $values['bundle']);
    $url = Url::fromRoute($route);
    $response = new RedirectResponse($url);
    $response->send();
  }

}