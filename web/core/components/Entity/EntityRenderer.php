<?php

namespace Cherry\Entity;

use Cherry\Container\ArgumentsAwareContainerInjectionInterface;
use Cherry\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityRenderer
 *
 * @package Cherry\Entity
 */
class EntityRenderer implements ArgumentsAwareContainerInjectionInterface {

  /** @var EntityInterface $entity */
  protected EntityInterface $entity;

  /** @var Renderer $renderer */
  protected Renderer $renderer;

  /**
   * EntityRenderer constructor.
   *
   * @param Renderer        $renderer
   * @param EntityInterface $entity
   */
  public function __construct(Renderer $renderer, EntityInterface $entity) {
    $this->setEntity($entity);
    $this->setRenderer($renderer);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $arguments) {
    return new static(
      $container->get('renderer'),
      ...$arguments
    );
  }

  /**
   * Returns Entity object
   *
   * @return EntityInterface
   */
  protected function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Sets Entity object
   *
   * @param EntityInterface $entity
   *
   * @return self
   */
  protected function setEntity(EntityInterface $entity): self {
    $this->entity = $entity;
    return $this;
  }

  /**
   * Returns Renderer object
   *
   * @return Renderer
   */
  protected function getRenderer(): Renderer {
    return $this->renderer;
  }

  /**
   * Sets Renderer object
   *
   * @return $this
   */
  protected function setRenderer($renderer): self {
    $this->renderer = $renderer;
    return $this;
  }

  /**
   * Returns rendered Entity object
   *
   * @param array       $variables
   * @param string|null $view_mode
   *
   * @return string|NULL
   */
  public function render(array $variables = [], $view_mode = NULL): ?string {
    $entity = $this->getEntity();
    $view_mode = $view_mode ?? 'default';
    $values = $entity->getValues();
    $values = array_merge($values, $variables);

    $template = $view_mode;
    if ($entity instanceof EntityBundleableInterface) {
      $template = $entity->bundle() . '--' . $view_mode;
    }

    foreach ($values as $key => $value) {
      $values[$key] = htmlspecialchars_decode($value);
    }
    return $this->getRenderer()
                ->setType('entity/' . $entity->getType(), 'entity/default')
                ->setTemplate($template, $view_mode)
                ->render($values);
  }

}