<?php

namespace Cherry\Entity;

use Cherry\Core;
use Exception;
use Cherry\Database\Result;

/**
 * Class EntityStorage
 *
 * @package Cherry\Entity
 */
class EntityStorage implements EntityStorageInterface {

  /** @var string|null $type */
  protected ?string $type;

  /**
   * EntityStorage constructor.
   *
   * @param string|null $type
   */
  public function __construct(?string $type = NULL) {
    $this->setType($type);
  }

  /**
   * Gets storage type
   *
   * @return string|null
   */
  protected function getType(): ?string {
    return $this->type;
  }

  /**
   * Sets storage type
   *
   * @param string|null $type
   */
  protected function setType(string $type = NULL) {
    $this->type = $type;
  }

  /**
   * Returns Entity object based on type
   *
   * @param string|null $type
   *
   * @return EntityInterface
   */
  public function getObject(?string $type = NULL): EntityInterface {
    $type = $type ?? $this->getType() ?? NULL;
    return EntityTypeManager::getEntityClassFromType($type);
  }

  /**
   * @return \Cherry\Entity\EntityQueryInterface
   */
  public function getQuery(): EntityQueryInterface {
    /**
     * @var \Cherry\Entity\EntityQueryInterface $query
     */
    $query = Core::loadClass('entity.query');
    return $query->type($this->getType());
  }

  /**
   * {@inheritDoc}
   */
  public function loadByProperties(array $properties = [], bool $multiple = FALSE, bool $massage = TRUE, ?array $order = NULL) {
    $type = $this->getType() ?? $properties['type'] ?? NULL;
    if ($type === NULL && isset($properties['id'])) {
      $type = EntityTypeManager::getEntityTypeFromId((int) $properties['id']);
    }
    $entity = $this->getObject($type);
    if (!$entity instanceof EntityInterface) {
      \Cherry::Logger()->addError('Entity object not an instance of EntityInterface', 'EntityTypeManager');
      return FALSE;
    }
    unset($properties['type']);
    $query = \Cherry::Database()
      ->select('entity__' . $type);
    if (!empty($order)) {
        $query->orderBy($order['field'], $order['direction'] ?? 'ASC');
    } else {
        $query->orderBy('id', 'DESC');
    }
    foreach ($properties as $field => $value) {
      if (is_array($value)) {
        $query->condition($field, $value, 'IN');
      } else {
        $query->condition($field, $value);
      }
    }
    try {
      /** @var Result $result */
      $result = $query->execute();
    } catch (Exception $exception) {
      \Cherry::Logger()->addFailure($exception, 'Entity');
      return FALSE;
    }
    if (!$results = $result->fetchAllAssoc('id')) {
      return FALSE;
    }

    if (count($results) === 1 && $multiple === FALSE) {
      $current = reset($results);
      return $entity->massageProperties($current, $massage);
    }

    $entities = [];
    foreach ($results as $id => $current) {
      $entities[$id] = $entity->massageProperties($current, $massage);
    }
    return $entities;
  }

  /**
   * {@inheritDoc}
   */
  public function loadMultiple(array $ids = [], bool $massage = FALSE) {
    $properties = [];
    if (!empty($ids)) {
      $properties = [
        'id' => $ids,
      ];
    }

    return $this->loadByProperties(
      $properties,
      TRUE,
      $massage
    );
  }

  /**
   * {@inheritDoc}
   */
  public function load(int $id, bool $massage = FALSE) {
    return $this->loadByProperties(
      ['id' => $id],
      FALSE,
      $massage
    );
  }

}