<?php

namespace Cherry\Element;

use Cherry;
use Cherry\Core;
use Cherry\Database\Result;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;
use Cherry\Utils\Callback\Callback;

/**
 * Class ElementManager
 *
 * @package Cherry\Page
 */
class ElementManager {

  /** @var Renderer $renderer */
  protected Renderer $renderer;

  /**
   * ElementManager constructor.
   *
   * @param Renderer $renderer
   */
  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * Creates page in database.
   *
   * @param array $values
   *
   * @return bool
   */
  public function createElement($values = []): bool {
    $page = \Cherry::Database()
      ->insert('elements')
      ->values($values);
    if (!$page->execute()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns cached/fresh page content.
   *
   * @param string $element_id
   * @param array $parameters
   * @param RouteInterface $route
   *
   * @return mixed
   *
   * @throws Cherry\Queue\Exception\CallbackException
   */
  public function getElementRender(string $element_id, array $parameters, RouteInterface $route) {
    $element = $this->getElement($element_id);
    if (!is_array($element)) {
      return $element;
    }
    $element_object = $this->getElementObject($element_id, $parameters, $route);
    if (!$element_object instanceof ElementInterface) {
      return FALSE;
    }
    $callback = new Callback();
    $callback->setClass($element['controller']);
    $callback->setMethod('render');
    $callback->setClassParameters([$parameters, $route, $this->renderer]);
    return \Cherry::Cache()->getContentData($element_object->getCacheOptions(), $callback);
  }

  /**
   * @param string $element_id
   * @param array $parameters
   * @param \Cherry\Route\RouteInterface $route
   *
   * @return bool|ElementInterface
   */
  public function getElementObject(string $element_id, array $parameters, RouteInterface $route) {
    $element = $this->getElement($element_id);
    if (!is_array($element)) {
      return NULL;
    }
    $elementObject = Core::loadClass($element['controller'], [$parameters, $route, $this->renderer]);
    if (!$elementObject instanceof ElementInterface) {
      return FALSE;
    }
    return $elementObject;
  }

  /**
   * @param      $element_id
   *
   * @return array|bool|mixed
   */
  public function getElement($element_id) {
    $element = \Cherry::Database()
      ->select('elements')
      ->condition('id', $element_id)
      ->fields(NULL, ['controller'])
      ->execute();

    if (!$element instanceof Result) {
      \Cherry::Logger()->addError('Couldn\'t load the element [' . $element_id . ']', 'ElementManager');
      return FALSE;
    }

    $element_result = $element->fetchAllAssoc();
    if (count($element_result) == 0) {
      \Cherry::Logger()->addFailure('Element not found [' . $element_id . ']', 'ElementManager');
      return NULL;
    }
    return reset($element_result);
  }

}