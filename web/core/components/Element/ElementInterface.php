<?php

namespace Cherry\Element;

use Cherry\Route\RouteInterface;

/**
 * Interface ElementInterface
 *
 * @package Cherry\Page
 */
interface ElementInterface {

  /**
   * Returns permissions required to view this element
   *
   * @return array
   */
  public function getPermissions();

  /**
   * Returns caching options for this element.
   *
   * @return array
   */
  public function getCacheOptions();

  /**
   * Sets caching for element.
   *
   * @param array|null $options
   *
   * @return self
   */
  public function setCacheOptions(?array $options = NULL): self;

  /**
   * Merges new cache tags with Element's base tags.
   *
   * @param array $tags
   *   Array of tags to merge.
   *
   * @return self
   */
  public function mergeCacheTags(array $tags = []): self;

  /**
   * Returns element parameter
   *
   * @param string $parameter
   *
   * @return mixed
   */
  public function get(string $parameter);

  /**
   * Returns the twig render of the current element.
   * To be overwritten in the element's class.
   *
   * @return NULL|string
   */
  public function render();

}
