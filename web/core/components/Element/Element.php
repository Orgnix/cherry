<?php

namespace Cherry\Element;

use Cherry\Cache\CacheBase;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;
use Cherry\Utils\Strings\StringManipulation;
use Cherry\Translation\StringTranslation;

/**
 * Class Element
 *
 * @package Cherry\Element
 */
class Element implements ElementInterface {
  use StringTranslation;

  /** @var array $caching */
  protected array $caching;

  /** @var array $parameters */
  protected array $parameters = [];

  /** @var RouteInterface $route */
  protected RouteInterface $route;

  /** @var Renderer $renderer */
  protected Renderer $renderer;

  /** @var array $permissions */
  protected array $permissions = [];

  /**
   * Element constructor.
   *
   * @param array          $parameters
   * @param RouteInterface $route
   * @param Renderer       $renderer
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters($parameters);
    $this->setRoute($route);
    $this->setRenderer($renderer);
    $this->setCacheOptions();
  }

  /**
   * {@inheritDoc}
   */
  public function getPermissions(): array {
    return $this->permissions;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheOptions(): array {
    \Cherry::Event('alterElementCacheOptions')
      ->fire($this->caching, [$this->get('id')]);

    return $this->caching;
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(?array $options = NULL): self {
    if ($options !== NULL) {
      $this->caching = $options;
      return $this;
    }

    $this->caching = [
      'key' => 'element',
      'tags' => ['element'],
      'context' => ['element'],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function mergeCacheTags(array $tags = []): self {
    $this->caching['tags'] = CacheBase::mergeTags($this->caching['tags'], $tags);

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    \Cherry::Event('elementPreRender')
      ->fire($this->parameters, [$this->get('id'), &$this]);

    // Sort the elements by weight
    if (isset($this->parameters['elements']) && is_array($this->parameters['elements'])) {
      foreach ($this->parameters['elements'] as $region => $element) {
        usort($this->parameters['elements'][$region], function ($a, $b) {
          $weight1 = $a['weight'] ?? 0;
          $weight2 = $b['weight'] ?? 0;
          return $weight1 <=> $weight2;
        });
      }
    }

    foreach ($this->getPermissions() as $permission) {
      if (!\Cherry::CurrentPerson()->hasPermission($permission)) {
        return NULL;
      }
    }

    return '';
  }

  /**
   * {@inheritDoc}
   */
  public function get(string $parameter) {
    return $this->parameters[$parameter] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Checks if parameter exists.
   *
   * @param string $parameter
   *
   * @return bool
   */
  protected function hasParameter(string $parameter): bool {
    return $this->get($parameter) !== NULL;
  }

  /**
   * Sets element parameters
   *
   * @param array $parameters
   *
   * @return Element
   */
  protected function setParameters($parameters = []): self {
    $this->parameters = $this->parameters + $parameters;
    return $this;
  }

  /**
   * Clones a parameter to a different key.
   *
   * @param string $originalKey
   * @param string $cloneKey
   *
   * @return self
   */
  protected function cloneParameter(string $originalKey, string $cloneKey): self {
    return $this->setParameter($cloneKey, $this->get($originalKey) ?? '');
  }

  /**
   * @param string $key
   * @param $value
   *
   * @return self
   */
  protected function setParameter(string $key, $value): self {
    if (StringManipulation::contains($key, '.')) {
      $keys = StringManipulation::explode($key, '.');
      $key = end($keys);
      $return = &$this->parameters;
      foreach ($keys as $item) {
        $return = &$return[$item];
        if ($item === $key) {
          $return = $value;
        }
      }
      return $this;
    }
    $this->parameters[$key] = $value;
    return $this;
  }

  /**
   * Sets renderer object
   *
   * @param Renderer $renderer
   *
   * @return self
   */
  protected function setRenderer(Renderer $renderer): self {
    $this->renderer = $renderer;
    return $this;
  }

  /**
   * Returns renderer object
   *
   * @return Renderer
   */
  protected function getRenderer(): Renderer {
    return $this->renderer;
  }

  /**
   * @param RouteInterface $route
   *
   * @return self
   */
  protected function setRoute(RouteInterface $route): self {
    $this->route = $route;
    return $this;
  }

  /**
   * @return RouteInterface
   */
  protected function getRoute(): RouteInterface {
    return $this->route;
  }

  /**
   * Adds Element to list of elements.
   *
   * @param ElementInterface $element
   *
   * @return self
   */
  protected function addElement(ElementInterface $element): self {
    $this->elements[] = $element;
    return $this;
  }

  /**
   * Sets permissions required to view this element
   *
   * @param array $permissions
   *
   * @return Element
   */
  protected function setPermissions(array $permissions): self {
    $this->permissions = $permissions;
    return $this;
  }

}
