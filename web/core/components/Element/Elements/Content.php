<?php

namespace Cherry\Element\Elements;

use Cherry;
use Cherry\Element\Element;
use Cherry\Route\RouteInterface;

/**
 * Class Content
 *
 * @package Cherry\Element\Elements
 */
class Content extends Element {

  /**
   * Content constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'content',
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions($parameters = []): self {
    // Don't cache Element, since the route already has caching on it.
    $this->caching = [
      'key' => 'element.content',
      'context' => 'element',
      'tags' => ['element:content'],
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    if (parent::render() === NULL) {
      return '';
    }

    return $this->getRoute()->render();
  }

}
