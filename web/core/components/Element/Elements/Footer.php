<?php

namespace Cherry\Element\Elements;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Element\Element;
use Cherry\Route\RouteInterface;

/**
 * Class Footer
 *
 * @package Cherry\Element\Elements
 */
class Footer extends Element {

  /**
   * Footer constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'footer',
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'element.footer',
      'context' => 'element',
      'tags' => ['element:footer'],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    if (parent::render() === NULL) {
      return '';
    }

    return $this->getRenderer()
      ->setType()
      ->setTemplate('footer')
      ->render($this->getParameters() ?? []);
  }

}
