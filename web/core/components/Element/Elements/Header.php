<?php

namespace Cherry\Element\Elements;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Element\Element;
use Cherry\Language\LanguageManager;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;

/**
 * Class Header
 *
 * @package Cherry\Element\Elements
 */
class Header extends Element {

  /**
   * Header constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'header',
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions($parameters = []): self {
    $route_parameters = [];
    foreach ($this->getRoute()->getParameters() as $key => $value) {
      $route_parameters[] = $key . '=' . $this->getRoute()->getValue($key);
    }

    $this->caching = [
      'key' => 'element.header',
      'context' => 'header:' . $this->getRoute()->getRoute() . '[' . implode(',', $route_parameters) . '][lang=' .
        \Cherry::Container()->get('language.manager')->getLangCode(LanguageManager::CURRENT_LANGUAGE) . ']',
      'tags' => ['element:header'],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    if (parent::render() === NULL) {
      return '';
    }

    return $this->getRenderer()
      ->setType()
      ->setTemplate('header')
      ->render($this->getParameters() ?? []);
  }

}
