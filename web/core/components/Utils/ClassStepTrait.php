<?php

namespace Cherry\Utils;

/**
 * You can use this trait when your class requires deep knowledge of
 * what step in the construction you are in by checking properties.
 */
trait ClassStepTrait {

  /**
   * Check what step the construction of the class is on.
   *
   * @param string $key
   *
   * @return bool
   */
  protected function step(string $key): bool {
    return property_exists($this, $key) && !empty($this->{$key});
  }

}
