<?php

namespace Cherry\Utils\Callback;

use Cherry\Core;
use Cherry\Queue\Exception\CallbackException;
use Serializable;

/**
 * Class Callback.
 *
 * @package Cherry\Utils\Callback
 */
class Callback implements CallbackInterface, Serializable {

  /**
   * Whether the callback is initialized or not.
   *
   * @var bool
   */
  protected bool $initialized = FALSE;

  /**
   * Class parameter, if this is false the given method will be treated as a plain function.
   *
   * @var string|bool
   */
  protected $class = FALSE;

  /**
   * The method or function to call.
   *
   * @var string
   */
  protected string $method;

  /**
   * The parameters to pass onto the method or function.
   *
   * @var array
   */
  protected array $parameters = [];

  /**
   * The parameters to pass onto the class.
   *
   * @var array
   */
  protected array $classParameters = [];

  /**
   * {@inheritDoc}
   */
  public function execute() {
    if (!$this->initialized) {
      return FALSE;
    }

    if ($this->getClass() !== FALSE) {
      $class = Core::loadClass($this->getClass(), $this->getClassParameters());
      return call_user_func([$class, $this->getMethod()], ...$this->getParameters());
    }

    return call_user_func($this->getMethod(), ...$this->getParameters());
  }

  /**
   * {@inheritDoc}
   */
  public function getParameters(): array {
    return $this->parameters;
  }

  /**
   * {@inheritDoc}
   */
  public function setParameters(array $parameters): CallbackInterface {
    $this->parameters = $parameters;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getMethod(): string {
    return $this->method;
  }

  /**
   * {@inheritDoc}
   */
  public function setMethod(string $method): CallbackInterface {
    if ($this->getClass() !== FALSE) {
      if (!method_exists($this->getClass(), $method)) {
        throw new CallbackException('Method <em>' . $method . '</em>  does not exist in ' . $this->getClass());
      }
    } else {
      if (!function_exists($method)) {
        throw new CallbackException('Function <em>' . $method . '</em> does not exist.');
      }
    }

    $this->initialized = TRUE;
    $this->method = $method;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getClass() {
    return $this->class;
  }

  /**
   * {@inheritDoc}
   */
  public function setClass(string $class): CallbackInterface {
    if (!class_exists($class)) {
      throw new CallbackException('The given class <em>' . $class . '</em> does not exist.');
    }

    $this->class = $class;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getClassParameters(): array {
    return $this->classParameters;
  }

  /**
   * {@inheritDoc}
   */
  public function setClassParameters($parameters): self {
    $this->classParameters = $parameters;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function toCallable() {
    if ($this->getClass() !== FALSE) {
      $class = Core::loadClass($this->getClass(), $this->getClassParameters());
      return [$class, $this->getMethod()];
    }

    return FALSE;
  }

  /**
   * @inheritDoc
   */
  public function serialize() {
    return [
      'class' => $this->getClass(),
      'class_parameters' => $this->getClassParameters(),
      'method' => $this->getMethod(),
      'method_parameters' => $this->getParameters(),
    ];
  }

  /**
   * @inheritDoc
   */
  public function unserialize($data) {
    $data = unserialize($data);
    return $this
      ->setClass($data['class'])
      ->setClassParameters($data['class_parameters'])
      ->setMethod($data['method'])
      ->setParameters($data['method_parameters']);
  }

}
