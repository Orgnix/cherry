<?php

namespace Cherry\Utils\Callback;

/**
 * Interface CallbackInterface.
 *
 * @package Cherry\Utils\Callback
 */
interface CallbackInterface extends CallableInterface {

  /**
   * Executes the given queue action.
   *
   * @return mixed
   *
   * @throws \Cherry\Queue\Exception\CallbackException
   */
  public function execute();

  /**
   * Returns the parameters for the callback method/function.
   *
   * @return array
   */
  public function getParameters(): array;

  /**
   * Sets the parameters for the callback method/function.
   *
   * @param array $parameters
   *   The parameters.
   *
   * @return self
   */
  public function setParameters(array $parameters): self;

  /**
   * The method or function to call.
   *
   * @return string
   */
  public function getMethod(): string;

  /**
   * Sets the method or function to call.
   *
   * @param string $method
   *   The method or function to call.
   *
   * @return self
   *
   * @throws \Cherry\Queue\Exception\CallbackException
   */
  public function setMethod(string $method): self;

  /**
   * Gets the class the method belongs to.
   *
   * @return string|bool
   */
  public function getClass();

  /**
   * Sets the class the method belongs to.
   *
   * @param string $class
   *   The class the method belongs to.
   *
   * @return self
   *
   * @throws \Cherry\Queue\Exception\CallbackException
   */
  public function setClass(string $class): self;

  /**
   * Returns the paraneters to be passed on to the class.
   *
   * @return array
   */
  public function getClassParameters(): array;

  /**
   * Sets the paraneters to be passed on to the class.
   *
   * @param array $parameters
   *
   * @return $this
   */
  public function setClassParameters(array $parameters): self;

}
