<?php

namespace Cherry\Utils\Callback;

/**
 * Interface CallableInterface.
 *
 * This provides a function to return a callable object.
 * Usually in the form of ['@class', '@method'] or '::@method'
 *
 * @package Cherry\Utils\Callback
 */
interface CallableInterface {

  /**
   * @return callable|bool
   */
  public function toCallable();

}
