<?php

namespace Cherry\Utils\Strings;

/**
 * Interface StringableInterface
 *
 * @package Cherry\Utils\Strings
 */
interface StringableInterface {

  /**
   * Returns class value to string.
   *
   * @return string
   */
  public function __toString();

}
