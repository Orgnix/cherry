<?php

namespace Cherry\Utils\Classes;

/**
 * Adds weight to class.
 */
trait WeightableClassTrait {

  /**
   * The weight of this item.
   *
   * @var int
   */
  protected int $weight;

  /**
   * Returns the weight of this item.
   *
   * @return int
   */
  public function weight(): int {
    return $this->weight;
  }

  /**
   * Sets the item's weight.
   *
   * @param int $weight
   *   The item's weight.
   */
  protected function setWeight(int $weight) {
    $this->weight = $weight;
  }

}
