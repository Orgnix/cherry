<?php

namespace Cherry\Utils\Classes;

/**
 * Turns class properties into an array object.
 */
trait ArrayableClassTrait {

  /**
   * Return class properties as array.
   *
   * @return array
   */
  public function toArray(): array {
    $array = [];
    foreach (get_object_vars($this) as $key => $item) {
      if (is_object($item)) {
        if (!isset($array[$key])) {
          $array[$key] = 'stdObject(' . get_class($item) . ')';
        }
        foreach (class_uses($item) as $class_use) {
          if (strpos(ArrayableClassTrait::class, $class_use) === FALSE) {
            continue;
          }

          $array[$key] = $item->toArray();
          break;
        }
      } else {
        $array[$key] = $item;
      }
    }

    return $array;
  }

}
