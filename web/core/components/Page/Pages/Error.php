<?php

namespace Cherry\Page\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Page\Page;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;

/**
 * Class Error
 *
 * @package Cherry\Page
 */
class Error extends Page {

  /**
   * Error constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'error',
      'title' => $this->translate('Error'),
      'summary' => $this->translate('There was an error trying to reach a certain page.'),
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  protected function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'page.error.' . $this->get('key'),
      'context' => 'page',
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    if ($this->hasParameter('key')) {
      $this->caching['key'] .= '.' . $this->get('key');
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();
    switch ($this->get('key')) {
      case '404':
        $title = 'Page not found';
        break;
      case '403':
        $title = 'Forbidden';
        break;
      case '301':
        $title = 'Moved permanently';
        break;
      case '302':
        $title = 'Moved temporarily';
        break;
      default:
        $this->setParameter('key', 500);
        $title = 'Internal server error';
        break;
    }

    if (\Cherry::Container()->get('config')->get('site.log_404')) {
      \Cherry::Logger()->addFailure($title, $this->get('key'));
    }

    $this->setParameter('page.title', $title);
    return $this->getRenderer()
      ->setType('error')
      ->setTemplate($this->get('key'))
      ->render($this->getParameters());
  }

}