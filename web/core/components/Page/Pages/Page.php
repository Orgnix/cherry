<?php

namespace Cherry\Page\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Page\Page as PageBase;
use Cherry\Route\RouteInterface;

/**
 * Class Page
 *
 * @package Cherry\Page\Pages
 */
class Page extends PageBase {

  /**
   * {@inheritDoc}
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'page',
      'title' => $this->translate('page'),
      'summary' => $this->translate('Welcome to your Cherry Dashboard!'),
    ]);
    parent::__construct($parameters, $route, $renderer);

    $elementManager = \Cherry::ElementManager();

    kint($elementManager->getElementObject('header', [], $route));

    $this->addElement($elementManager->getElementObject('header', [], $route));
    $this->addElement($elementManager->getElementObject('content', [], $route));
    $this->addElement($elementManager->getElementObject('footer', [], $route));
  }

  /**
   * {@inheritDoc}
   */
  protected function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'page',
      'context' => 'page',
      'tags' => ['page'],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    kint($this->getElements());
    kint($this->getRenderedElements());

    return $this->getRenderer()
      ->setType()
      ->setTemplate('dashboard')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'elements' => $this->getRenderedElements(),
        'dashboard' => [
          'installedExtensions' => count(\Cherry::Container()->get('extension.manager')->getInstalledExtensions()),
        ],
      ]);
  }

}