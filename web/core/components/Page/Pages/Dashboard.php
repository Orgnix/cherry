<?php

namespace Cherry\Page\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Dashboard
 *
 * @package Cherry\Page
 */
class Dashboard extends Page {

  /**
   * {@inheritDoc}
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'dashboard',
      'title' => $this->translate('Dashboard'),
      'summary' => $this->translate('Welcome to your Cherry Dashboard!'),
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  protected function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'page.dashboard',
      'context' => 'page',
      'tags' => ['dashboard'],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();
    return $this->getRenderer()
      ->setType()
      ->setTemplate('dashboard')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'dashboard' => [
          'installedExtensions' => count(\Cherry::Container()->get('extension.manager')->getInstalledExtensions()),
        ],
      ]);
  }

}