<?php

namespace Cherry\Page;

use Cherry\Element\ElementInterface;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;
use Cherry\Utils\Strings\StringManipulation;
use Cherry\Translation\StringTranslation;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class Page
 *
 * @package Cherry\Page
 */
class Page implements PageInterface {
  use StringTranslation;

  /** @var array $caching */
  protected array $caching;

  /** @var array $parameters */
  protected array $parameters = [];

  /** @var RouteInterface $route */
  protected RouteInterface $route;

  /** @var Renderer $renderer */
  protected Renderer $renderer;

  /** @var array $permissions */
  protected array $permissions = [];

  /** @var array $elements */
  protected array $elements = [];

  /**
   * Page constructor.
   *
   * @param array $parameters
   * @param RouteInterface $route
   * @param Renderer $renderer
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters($parameters);
    $this->setRoute($route);
    $this->setRenderer($renderer);
    $this->setCacheOptions();
  }

  /**
   * {@inheritDoc}
   */
  public function getPermissions(): array {
    return $this->permissions;
  }

  /**
   * {@inheritDoc}
   */
  public function getElements(): array {
    return $this->elements;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheOptions(): array {
    return $this->caching;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    \Cherry::Event('pagePreRender')
      ->fire($this->parameters, [$this->get('id')]);

    foreach ($this->getPermissions() as $permission) {
      if (!\Cherry::CurrentPerson()->hasPermission($permission)) {
        /** @var RouteInterface $dashboard */
        $dashboard = \Cherry::Container()->get('route')->load('dashboard');
        $redirect = new RedirectResponse($dashboard->getUrl());
        $redirect->setStatusCode(401);
        $redirect->send();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function get(string $parameter) {
    return $this->parameters[$parameter] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Checks if parameter exists.
   *
   * @param string $parameter
   *
   * @return bool
   */
  protected function hasParameter(string $parameter): bool {
    return $this->get($parameter) !== NULL;
  }

  /**
   * Use this function before 'setParameters' if you wish to start off
   * with a clean set of parameters.
   */
  protected function emptyParameters(): self {
    $this->parameters = [];

    return $this;
  }

  /**
   * Sets page parameters
   *
   * @param array $parameters
   *
   * @return Page
   */
  protected function setParameters(array $parameters = []): self {
    $this->parameters = $this->parameters + $parameters;
    return $this;
  }

  /**
   * Clones a parameter to a different key.
   *
   * @param string $originalKey
   * @param string $cloneKey
   *
   * @return self
   */
  protected function cloneParameter(string $originalKey, string $cloneKey): self {
    return $this->setParameter($cloneKey, $this->get($originalKey) ?? '');
  }

  /**
   * Sets parameter
   *
   * @param string $key
   * @param $value
   *
   * @return self
   */
  protected function setParameter(string $key, $value): self {
    if (StringManipulation::contains($key, '.')) {
      $keys = StringManipulation::explode($key, '.');
      $key = end($keys);
      $return = &$this->parameters;
      foreach ($keys as $item) {
        $return = &$return[$item];
        if ($item === $key) {
          $return = $value;
        }
      }
      return $this;
    }
    $this->parameters[$key] = $value;
    return $this;
  }

  /**
   * Sets route object
   *
   * @param RouteInterface $route
   *
   * @return self
   */
  protected function setRoute(RouteInterface $route): self {
    $this->route = $route;
    return $this;
  }

  /**
   * Returns route object
   *
   * @return RouteInterface
   */
  protected function getRoute(): RouteInterface {
    return $this->route;
  }

  /**
   * Sets renderer object
   *
   * @param Renderer $renderer
   *
   * @return self
   */
  protected function setRenderer(Renderer $renderer): self {
    $this->renderer = $renderer;
    return $this;
  }

  /**
   * Returns renderer object
   *
   * @return Renderer
   */
  protected function getRenderer(): Renderer {
    return $this->renderer;
  }

  /**
   * Adds Element to list of elements.
   *
   * @param ElementInterface $element
   *
   * @return self
   */
  protected function addElement(ElementInterface $element): self {
    $this->elements[] = $element;
    return $this;
  }

  /**
   * Returns rendered string of elements
   *
   * @return array
   */
  protected function getRenderedElements(): array {
    $elements = [];
    /** @var ElementInterface $element */
    foreach ($this->getElements() as $element) {
      $elements[$element->get('id')] = $element->render();
    }

    return $elements;
  }

  /**
   * Sets permissions required to view this page
   *
   * @param array $permissions
   *
   * @return Page
   */
  protected function setPermissions(array $permissions): self {
    $this->permissions = $this->permissions + $permissions;
    return $this;
  }

  /**
   * Sets caching for page.
   *
   * @return self
   */
  protected function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page',
      'tags' => ['page'],
      'context' => ['page'],
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * @param RouteInterface|string $route
   *
   * @return RedirectResponse
   */
  protected function redirect($route): RedirectResponse {
    if ($route instanceof RouteInterface) {
      $route = $route->getUrl();
    }
    $response = new RedirectResponse($route);
    return $response->send();
  }

}
