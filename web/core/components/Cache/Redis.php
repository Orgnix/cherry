<?php

namespace Cherry\Cache;

use Cherry\Database\Database;
use Cherry\Event\EventManager;
use Cherry\Logger\LoggerInterface;
use Cherry;
use Cherry\Route\RouteManager;
use Cherry\Settings;
use Cherry\Utils\Callback\CallbackInterface;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Adapter\RedisTagAwareAdapter;
use Symfony\Component\Cache\CacheItem;

/**
 * Class Redis
 *
 * @package Cherry\Cache
 */
class Redis extends CacheBase {

  /** @var RedisTagAwareAdapter $redis */
  protected RedisTagAwareAdapter $redis;

  /**
   * {@inheritDoc}
   */
  public function __construct(Database $database, EventManager $event_manager, RouteManager $route_manager, LoggerInterface $logger) {
    parent::__construct($database, $event_manager, $route_manager, $logger);

    $settings = Settings::getAll();
    if (!isset($settings['redis'])) {
      \Cherry::Logger()->addError('No redis settings present', 'Redis');
    }
    $client = RedisAdapter::createConnection('redis://' . $settings['redis']['host'] . ':' . $settings['redis']['port'], $settings['redis']['config']);
    $this->redis = new RedisTagAwareAdapter($client);
    kint($this->redis);
    exit;
  }

  /**
   * {@inheritDoc}
   */
  public function getContentData(array $cacheOptions, CallbackInterface $callback) {
    return $this->redis->get($cacheOptions['key'], [$callback->getClass(), $callback->getMethod()]);
  }

  /**
   * {@inheritDoc}
   */
  public function updateContentData(array $cacheOptions, $value = []) {
    $this->insertContentData($cacheOptions, $value);
  }

  /**
   * {@inheritDoc}
   */
  public function insertContentData(array $cacheOptions, $value = []) {
    $cacheItem = new CacheItem();
    $cacheItem->set($value);
    $this->redis->save($cacheItem);
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Psr\Cache\InvalidArgumentException
   */
  public function invalidateTags(array $tags): bool {
    return $this->redis->invalidateTags($tags);
  }

  /**
   * {@inheritDoc}
   */
  public function clearAllCaches(array &$caches = []): bool {
    $this->redis->clear('cherry');

    return parent::clearAllCaches($caches);
  }

}
