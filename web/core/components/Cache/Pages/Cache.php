<?php

namespace Cherry\Cache\Pages;

use Cherry;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;
use Cherry\Url;

/**
 * Class Cache
 *
 * @package Cherry\Cache\Pages
 */
class Cache extends Page {

  /**
   * Cache constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'cache',
      'title' => 'Cache',
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['manage cache']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.cache',
      'context' => 'page',
      'tags' => ['cache'],
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Exception
   */
  public function render() {
    parent::render();
    if (!$this->hasParameter(1)) {
      return NULL;
    }

    if ($this->get(1) === 'clear') {
      if (\Cherry::Cache()->clearAllCaches() !== FALSE) {
        \Cherry::Logger()->addSuccess('Successfully cleared all caches.', 'Cache');
      } else {
        \Cherry::Logger()->addFailure('Could not clear caches.', 'Cache');
      }

      \Cherry::Init(\Cherry::Request());

      $this->redirect($this->hasParameter('destination')
        ? urldecode($this->get('destination'))
        : Url::fromRoute(\Cherry::Container()->get('route')->load('dashboard')));
    }
  }

}