<?php

namespace Cherry\Cache;

use Cherry\Utils\Arrays\ArrayManipulation;
use Cherry\Database\Result;
use Cherry\Utils\Callback\CallbackInterface;

/**
 * Class Cache => used to store cache in database.
 *
 * @package Cherry\Cache
 */
class Cache extends CacheBase {

  /**
   * {@inheritDoc}
   */
  public function getContentData(array $cacheOptions, CallbackInterface $callback) {
    // Fires an event to alter cache options before being sent to DB.
    \Cherry::Event('cacheContentAlter')
      ->fire($cacheOptions);

    // Don't cache content if max-age is 0, clearing unneeded queries.
    // Don't cache content if KillSwitch was triggered.
    if ((isset($cacheOptions['max-age']) && $cacheOptions['max-age'] == 0) || \Cherry::KillSwitch()->check()) {
      return parent::getContentData($cacheOptions, $callback);
    }

    $query = $this->database
      ->select('cache_content')
      ->condition('field', $cacheOptions['key']);
    if (isset($cacheOptions['context']) && !empty($cacheOptions['context'])) {
      $query->condition('context', $cacheOptions['context']);
    }
    $result = $query->execute();
    if (!$result instanceof Result) {
      return FALSE;
    }

    $items = $result->fetchAllAssoc();
    if (count($items) == 0) {
      $data = parent::getContentData($cacheOptions, $callback);
      try {
        $this->insertContentData($cacheOptions, $data);
      } catch(\Exception $e) {
        $this->logger->addError('Something went wrong trying to add cache to the database. Error: ' . $e->getMessage(), 'Cache');
        return FALSE;
      }
    } else {
      $cache = $items;
      $cache = reset($cache);
      if ((time() - $cache['created']) > $cache['maxage'] && $cache['maxage'] != '-1') {
        $data = parent::getContentData($cacheOptions, $callback);
        if (!$this->updateContentData($cacheOptions, $data)) {
          return FALSE;
        }
      } else {
        $data = unserialize($cache['value']);
      }
    }

    // Add to cache stats.
    if (!isset($this->cacheStats['content'][$cacheOptions['key']])) {
      $this->cacheStats['content'][$cacheOptions['key']]['created'] = 1;
      $this->cacheStats['content'][$cacheOptions['key']]['called'] = 0;
    }
    $this->cacheStats['content'][$cacheOptions['key']]['called']++;

    return $data;
  }

  /**
   * @param array $cacheOptions
   * @param mixed $value
   *
   * @return bool
   */
  protected function insertContentData(array $cacheOptions, $value = []) {
    $query = $this->database
      ->insert('cache_content')
      ->values([
        'id' => 0,
        'field' => $cacheOptions['key'],
        'value' => serialize($value),
        'tags' => serialize($cacheOptions['tags'] ?? []),
        'context' => $cacheOptions['context'] ?? '',
        'maxage' => $cacheOptions['max-age'] ?? 0,
        'created' => time(),
      ])
      ->execute();
    if (!$query) {
      $this->logger->addFailure('Something went wrong trying to insert cache item [' . $cacheOptions['key'] . ']', 'Cache');
      return FALSE;
    }

    return TRUE;
  }

  /**
   * @param array $cacheOptions
   * @param mixed $value
   *
   * @return bool
   */
  protected function updateContentData(array $cacheOptions, $value = []) {
    $query = $this->database
      ->update('cache_content')
      ->condition('field', $cacheOptions['key'])
      ->values([
        'value' => serialize($value),
        'tags' => serialize($cacheOptions['tags'] ?? []),
        'context' => $cacheOptions['context'] ?? '',
        'maxage' => $cacheOptions['max-age'] ?? 0,
        'created' => time(),
      ])
      ->execute();
    if (!$query) {
      $this->logger->addFailure('Something went wrong trying to update cache item [' . $cacheOptions['key'] . ']', 'Cache');
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function clearAllCaches(array &$caches = []): bool {
    $caches[] = $this->database->query('TRUNCATE TABLE cache_content');

    // Clear routes, event listeners, extensions, ..
    return parent::clearAllCaches($caches);
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateTags(array $tags): bool {
    $cache_storage = $this->database
      ->select('cache_content')
      ->fields(NULL, ['field', 'tags'])
      ->execute();
    if (!$cache_storage instanceof Result) {
      return FALSE;
    }

    $cache = $cache_storage->fetchAllAssoc();
    if (!$cache || count($cache) === 0) {
      return FALSE;
    }

    foreach ($cache as $item) {
      $cur_tags = unserialize($item['tags']);
      if (!isset($cur_tags) || !is_array($cur_tags)) {
        continue;
      }

      foreach ($cur_tags as $tag) {
        if (!ArrayManipulation::contains($tags, $tag)) {
          continue;
        }

        $deletion = $this->database
          ->delete('cache_content')
          ->condition('field', $item['field'])
          ->execute();

        if (!$deletion) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

}