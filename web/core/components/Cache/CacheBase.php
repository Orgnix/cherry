<?php

namespace Cherry\Cache;

use Cherry;
use Cherry\Database\Database;
use Cherry\Event\EventManager;
use Cherry\Utils\Callback\CallbackInterface;
use Cherry\Route\RouteManager;
use Cherry\Logger\LoggerInterface;
use Psr\Log\InvalidArgumentException;

/**
 * Class Cache => used to store cache in database.
 *
 * @package Cherry\Cache
 */
class CacheBase implements CacheInterface {

  /** @var int $defaultCacheTime */
  protected static int $defaultCacheTime = 0;

  /** @var array $cacheableData */
  protected array $cacheableData;

  /** @var array $cacheStats */
  protected array $cacheStats;

  /**
   * Database object
   *
   * @var \Cherry\Database\Database
   */
  protected Database $database;

  /**
   * Database object
   *
   * @var \Cherry\Event\EventManager
   */
  protected EventManager $eventManager;

  /**
   * Database object
   *
   * @var \Cherry\Route\RouteManager
   */
  protected RouteManager $routeManager;

  /**
   * Database object
   *
   * @var \Cherry\Logger\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * @param \Cherry\Database\Database $database
   * @param \Cherry\Event\EventManager $event_manager
   * @param \Cherry\Route\RouteManager $route_manager
   * @param \Cherry\Logger\LoggerInterface $logger
   */
  public function __construct(Database $database, EventManager $event_manager, RouteManager $route_manager, LoggerInterface $logger) {
    $this->database = $database;
    $this->eventManager = $event_manager;
    $this->routeManager = $route_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public static function getDefaultCacheTime(): int {
    if (static::$defaultCacheTime === 0) {
      static::$defaultCacheTime = \Cherry::Container()
        ->get('config')
        ->get('site.cache.default_time') ?: CacheInterface::DEFAULT_CACHE_TIME;
    }
    return static::$defaultCacheTime;
  }

  /**
   * {@inheritDoc}
   */
  public function getData(string $cacheKey, $fallbackClass = '', $fallbackMethod = '', array $methodData = [], array $classData = []) {
    if (!isset($this->cacheStats[$cacheKey])) {
      $this->cacheStats[$cacheKey]['created'] = 0;
      $this->cacheStats[$cacheKey]['called'] = 0;
    }
    if (!isset($this->cacheableData[$cacheKey])) {
      $class = new $fallbackClass(...$classData);
      if (!empty($fallbackMethod)) {
        $this->cacheableData[$cacheKey] = $class->{$fallbackMethod}(...$methodData);
      } else {
        $this->cacheableData[$cacheKey] = $class;
      }
      $this->cacheStats[$cacheKey]['created']++;
    }
    $this->cacheStats[$cacheKey]['called']++;
    return $this->cacheableData[$cacheKey];
  }

  /**
   * {@inheritDoc}
   *
   * @throws Cherry\Queue\Exception\CallbackException
   */
  public function getContentData(array $cacheOptions, CallbackInterface $callback) {
    return $callback->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateTags(array $tags): bool {
    // Method stub
    return TRUE;
  }

  /**
   * @param array $cacheOptions
   * @param mixed $value
   *
   * @return bool
   */
  protected function insertContentData(array $cacheOptions, $value = []) {
    // Method stub
    return TRUE;
  }

  /**
   * @param array $cacheOptions
   * @param mixed $value
   *
   * @return bool
   */
  protected function updateContentData(array $cacheOptions, $value = []) {
    // Method stub
    return TRUE;
  }

  /**
   * {@inheritDoc}
   * @throws \Exception
   */
  public function clearAllCaches(array &$caches = []): bool {
    if (function_exists('opcache_reset')) {
      opcache_reset();
    }
    $this->cacheableData = [];
    $this->initializeCache();

    $caches[] = $this->database->query('TRUNCATE TABLE event_listeners');
    $caches[] = $this->database->query('TRUNCATE TABLE routes');

    // Start refreshing the cached objects
    $this->eventManager->installEventListeners();
    $this->routeManager->installRoutes();
    \Cherry::EntityTypeManager()->createEntities();

    foreach ($caches as $cache) {
      if (!$cache) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function returnCache() {
    return $this->cacheableData;
  }

  /**
   * {@inheritDoc}
   */
  public function returnCacheStats() {
    return $this->cacheStats;
  }

  /**
   * {@inheritDoc}
   */
  public function initializeCache() {
    $this->cacheableData['CHERRY_VERSION'] = '1';
    $this->cacheableData['CHERRY_VERSION_RELEASE'] = '0';
    $this->cacheableData['CHERRY_VERSION_RELEASE_MINOR'] = '0';
    $this->cacheableData['CHERRY_VERSION_STATUS'] = 'beta';
  }

  /**
   * {@inheritDoc}
   */
  public static function mergeTags(): array {
    if (func_num_args() === 0) {
      throw new InvalidArgumentException('You need to provide at least one argument for the mergeTags method to work.');
    }
    return array_merge(...func_get_args());
  }

}
