<?php

namespace Cherry\Cache;

use Cherry\Utils\Callback\CallbackInterface;

/**
 * Interface CacheInterface
 *
 * @package Cherry\Cache
 */
interface CacheInterface {

  /**
   * Default cache time in seconds.
   */
  const DEFAULT_CACHE_TIME = 3600;

  /**
   * Sets global variables in cache
   */
  public function initializeCache();

  /**
   * Returns default cache time.
   *
   * @return int
   */
  public static function getDefaultCacheTime(): int;

  /**
   * Checks if data is in cache, if so read it from cache.
   * If data not in cache, use fallback and add it to cache.
   * This is mainly for the reuse of classes/methods!
   * For storing content => CacheInterface::getContentData().
   *
   * @param string $cacheKey
   *                  The key under which the data will be stored
   * @param string $fallbackClass
   *                  The fallback class to use when there's no stored data yet
   * @param string $fallbackMethod
   *                  The fallback method to use when there's no stored data yet
   * @param array  $methodData
   *                  The necessary parameters for the given method in array format
   * @param array  $classData
   *                  The necessary parameters for the given class in array format
   *
   * @return mixed
   */
  public function getData(string $cacheKey, $fallbackClass = '', $fallbackMethod = '', array $methodData = [], array $classData = []);

  /**
   * Checks if data is in cache, if so read it from cache.
   * If data not in cache, use fallback and add it to cache.
   * This is mainly for (nearly) permanently storing content data.
   * E.g.: content, user data, ..
   *
   * @param array $cacheOptions
   *                  The options for the content data (key, max-age, ..)
   * @param CallbackInterface $callback
   *                  The Callback to be called.
   *
   * @return mixed
   */
  public function getContentData(array $cacheOptions, CallbackInterface $callback);

  /**
   * Truncates caching table(s)
   *
   * @return bool
   */
  public function clearAllCaches(): bool;

  /**
   * Invalidates tags
   *
   * @param array $tags
   *
   * @return bool
   */
  public function invalidateTags(array $tags): bool;

  /**
   * Merges cache tags, infinite arguments can be given.
   *
   * @return array
   */
  public static function mergeTags(): array;

}
