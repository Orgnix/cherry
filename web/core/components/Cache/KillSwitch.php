<?php

namespace Cherry\Cache;

/**
 * Class KillSwitch.
 *
 * Provides a page caching KillSwitch.
 */
class KillSwitch {

  /** @var bool $kill */
  protected bool $kill = FALSE;

  /**
   * Checks whether kill switch was triggered.
   *
   * @return bool
   */
  public function check(): bool {
    return $this->kill;
  }

  /**
   * Triggers kill switch.
   */
  public function trigger() {
    $this->kill = TRUE;
  }

}
