<?php

namespace Cherry\Cache;

/**
 * Dummy class to fake caching.
 */
class NullBackend extends CacheBase {}