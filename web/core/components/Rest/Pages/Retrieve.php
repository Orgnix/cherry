<?php

namespace Cherry\Rest\Pages;

use Cherry\Page\Page;
use Cherry\Rest\Rest;

/**
 * Class Retrieve
 *
 * @package Cherry\Rest\Pages
 */
class Retrieve extends Page {

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'rest.retrieve',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    Rest::Retrieve($this->getParameters());
  }

}
