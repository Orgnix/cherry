<?php

namespace Cherry\Rest\Pages;

use Cherry\Cache\CacheBase;
use Cherry\Page\Page;
use Cherry\Rest\Entity\Client;
use Cherry\Route\RouteInterface;

/**
 * Class Clients
 *
 * @package Cherry\Rest\Pages
 */
class Clients extends Page {

  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'rest.clients',
      'title' => $this->translate('Rest Clients'),
      'summary' => $this->translate('Welcome to your Cherry Dashboard!'),
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'rest.clients',
      'context' => 'page',
      'tags' => ['entity:client:overview'],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    return $this;
  }

  /**
   * @param array          $parameters
   * @param RouteInterface $route
   *
   * @return string|void|NULL
   */
  public function render() {
    parent::render();

    $clients = Client::loadMultiple();
    kint($clients);

    return NULL;
  }

}
