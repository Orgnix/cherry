<?php

namespace Cherry\Rest\Pages;

use Cherry\Page\Page;
use Cherry\Rest\Rest;

/**
 * Class Transmit
 *
 * @package Cherry\Rest\Pages
 */
class Transmit extends Page {

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'rest.transmit',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    Rest::Transmit($this->getParameters());
  }

}
