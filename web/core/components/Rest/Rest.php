<?php

namespace Cherry\Rest;

use Cherry\Utils\Arrays\ArrayManipulation;
use Cherry\Entity\EntityInterface;
use Cherry\Rest\Entity\Client;
use Cherry\Rest\Entity\ClientInterface;
use Cherry\Utils\Strings\StringManipulation;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class Rest
 *
 * @package Cherry\Rest
 */
class Rest {

  /**
   * Returns a message to the client
   *
   * @param     $message
   * @param int $status
   */
  public static function message($message, int $status = 400) {
    static::data([], $status, $message);
  }

  /**
   * Returns data to the client
   *
   * @param array       $data
   * @param int         $status
   * @param string|null $message
   */
  public static function data(array $data = [], int $status = 200, ?string $message = NULL) {
    $return_array = ['status' => $status ?? 200];
    if (count($data) > 0) {
      $return_array['data'] = $data;
    }
    if (!empty($message)) {
      $return_array['message'] = $message;
    }

    $response = new JsonResponse();
    $response->setContent(json_encode($return_array));
    $response->setStatusCode($status);
    $response->send();
    exit;
  }

  /**
   * @param array $information
   *
   * @return bool
   */
  public static function auth(array $information): bool {
    if (!isset($information['uuid']) || empty($information['uuid'])) {
      static::message('No authorization method was given.', 401);
    }

    /** @var Client $client */
    $client = \Cherry::EntityTypeManager()
      ->getStorage('client')
      ->loadByProperties([
        'uuid' => $information['uuid'],
      ]);
    if (!$client instanceof ClientInterface) {
      static::message('Client does not exist.', 401);
    }
    if (!$client->hasPermission('retrieve')) {
      static::message('Client has no access to this function.', 405);
    }

    return TRUE;
  }

  /**
   * Retrieves information from Cherry to the front-end
   *
   * @param array $information
   */
  public static function Retrieve(array $information) {
    static::auth($information);

    if (!isset($information['entity_type'])) {
      static::message('No entity type was given.');
    }

    // Remove entity_ prefix.
    foreach ($information as $key => $value) {
      if (StringManipulation::contains($key, 'entity_')) {
        ArrayManipulation::moveKey($information, $key, StringManipulation::replace($key, 'entity_', ''));
      }
    }

    $type = $information['type'];
    unset($information[0]);
    unset($information[1]);
    unset($information['uuid']);
    unset($information['type']);

    // Fire preRetrieve event
    \Cherry::Event('preRetrieve')
      ->fire($information);

    /** @var EntityInterface[] $data */
    $data = \Cherry::EntityTypeManager()
      ->getStorage($type)
      ->loadByProperties($information, TRUE);
    if (count($data) === 0) {
      static::message('Requested entity does not exist or is corrupt.', 404);
    }

    // Fire postRetrieve event
    \Cherry::Event('postRetrieve')
      ->fire($data);

    if (count($data) === 1) {
      $data = reset($data);
      static::data([$data->id() => static::massageEntityValues($data)], 200, 'Data was successfully retrieved.');
    } else {
      foreach ($data as &$entity) {
        $entity = static::massageEntityValues($entity);
      }
      static::data($data, 200, 'Data was successfully retrieved.');
    }
  }

  /**
   * Massages Entity value array.
   *
   * @param \Cherry\Entity\EntityInterface $entity
   *
   * @return array
   */
  public static function massageEntityValues(EntityInterface $entity): array {
    $values = $entity->getValues();
    foreach ($values as &$value) {
      if ($value instanceof EntityInterface) {
        $value = static::massageEntityValues($value);
      }
    }
    return $values;
  }

  /**
   * Transmits information from the front-end to the Cherry
   *
   * @param array $information
   */
  public static function Transmit(array $information) {
    static::auth($information);

    if (!isset($information['entity'])) {
      static::message('No entity was given.', 400);
    }

    if (!isset($information['new-values'])) {
      static::message('No new values were given, can\'t save entity. [usage: add data key "new-values" with array of values]', 412);
    }

    // Fire preTransmit event
    \Cherry::Event('preTransmit')
      ->fire($information);

    $type = $information['entity']['entity'] ?? NULL;
    unset($information['entity']['entity']);

    /** @var EntityInterface $data */
    $data = \Cherry::EntityTypeManager()
      ->getStorage($type)
      ->loadByProperties($information['entity']);
    if (!$data instanceof EntityInterface) {
      static::message('Requested entity does not exist or is corrupt.', 404);
    }

    foreach ($information['new-values'] as $key => $value) {
      $data->setValue($key, $value);
    }

    // Fire postTransmit event (Before saving!)
    \Cherry::Event('postTransmit')
      ->fire($data);

    if (!$data->save()) {
      static::message('Something went wrong trying to save the entity', 400);
    }

    static::data($data->getValues(), 200, 'Data was saved to the database');
  }

}