<?php

namespace Cherry;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class TwigExtensions
 *
 * @package Cherry\
 */
class TwigExtensions extends AbstractExtension {

  /**
   * @return array|TwigFunction[]
   */
  public function getFunctions() {
    $functions = [
      new TwigFunction('getEnv', [$this, 'getEnv']),
      new TwigFunction('kint', [$this, 'dump'])
    ];

    \Cherry::Event('twigExtensionsAlter')->fire($functions);

    return $functions;
  }

  /**
   * Returns environment variable (if allowed!)
   *
   * @param string $key
   *
   * @return mixed
   */
  public function getEnv(string $key) {
    return Core::getEnv($key, FALSE);
  }

  /**
   * @param $value
   */
  public function dump($value = NULL) {
    \kint($value);
  }

}
