<?php

namespace Cherry;

use Cherry\Config\Config;

/**
 * Class Theme
 *
 * @package Cherry
 */
class Theme {

  /** @var Config $config */
  protected Config $config;

  /**
   * Theme constructor.
   *
   * @param Config $config
   */
  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * Returns theme from config.
   *
   * @param string|null $key
   *          The type of theme (admin/front).
   *
   * @return mixed
   */
  public function getTheme(?string $key = NULL): string {
    if ($key !== NULL) {
      return $this->config->get('theme.' . $key);
    }
    return $this->config->get('theme');
  }

  /**
   * Sets theme
   *
   * @param string $key
   *          The type of theme (admin/front).
   * @param string $theme
   *          The machine readable name of the theme.
   *
   * @return bool
   */
  public function setTheme(string $key, string $theme): bool {
    return $this->config->set('theme.' . $key, $theme);
  }

  /**
   * Returns folder where theme files/assets are located.
   *
   * @return string
   */
  public function getThemeFolder(): string {
    return $this->getBaseThemeFolder() . DIRECTORY_SEPARATOR . $this->getTheme('admin') . DIRECTORY_SEPARATOR;
  }

  /**
   * Returns the folder where themes are located
   *
   * @return string
   */
  public function getBaseThemeFolder(): string {
    return Settings::get('themes.folder');
  }

  /**
   * Returns theme info array
   *
   * @param string $theme
   *               Theme name
   *
   * @return array|bool
   */
  public function getThemeInfo(string $theme) {
    if (!is_dir($this->getBaseThemeFolder() . DIRECTORY_SEPARATOR . $theme)) {
      return FALSE;
    }

    return YamlReader::fromYamlFile($this->getBaseThemeFolder() . DIRECTORY_SEPARATOR . $theme . DIRECTORY_SEPARATOR . $theme . '.yml');
  }

  /**
   * Returns available themes
   *
   * @return array
   */
  public function getAvailableThemes(): array {
    return array_map(function ($item) {
      return str_replace($this->getBaseThemeFolder() . DIRECTORY_SEPARATOR, '', $item);
    }, glob($this->getBaseThemeFolder() . DIRECTORY_SEPARATOR . '*', GLOB_ONLYDIR));
  }

}