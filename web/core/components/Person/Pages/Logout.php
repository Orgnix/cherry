<?php

namespace Cherry\Person\Pages;

use Cherry;
use Cherry\Page\Page;
use Cherry\Person\Entity\Person;
use Cherry\Route\RouteInterface;

/**
 * Class Logout
 *
 * @package Cherry\Person
 */
class Logout extends Page {

  /**
   * Login constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'logout',
      'title' => $this->translate('Logout'),
      'summary' => $this->translate('Logout from :sitename.', [
        ':sitename' => \Cherry::Container()->get('config')->get('site.name'),
      ]),
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  protected function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'page.logout',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    Person::logout();

    $dashboard = \Cherry::Container()->get('route')->load('dashboard');
    $this->redirect($dashboard);
  }

}
