<?php

namespace Cherry\Person\Pages;

use Cherry;
use Cherry\Page\Page;
use Cherry\Person\Entity\Person;
use Cherry\Person\Entity\PersonInterface;
use Cherry\Route\RouteInterface;

/**
 * Class Login
 *
 * @package Cherry\Person
 */
class Login extends Page {

  /**
   * Login constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'login',
      'title' => $this->translate('Login'),
      'summary' => $this->translate('Login to :sitename.', [
        ':sitename' => \Cherry::Container()->get('config')->get('site.name'),
      ]),
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  protected function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'page.login',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    $request = \Cherry::Request();
    if ($request->request->has('person-name') && $request->request->has('person-password')) {
      $person = [];
      $person['name'] = $request->request->get('person-name');
      $person['password'] = $request->request->get('person-password');
      $person['remember-me'] = in_array($request->request->get('person-remember-me'), ['on', 'true', 'TRUE']);

      /** @var Person $storage */
      $storage = \Cherry::EntityTypeManager()
        ->getStorage('person')
        ->loadByProperties([
        'name' => $person['name'],
      ]);
      if ($storage instanceof PersonInterface) {
        if ($storage->checkPassword($person['password'])) {
          Person::changeTo($storage->id(), $person['remember-me']);
          $message = [
            'type' => 'success',
            'message' => $this->translate('Login successful!'),
          ];
          $dashboard = \Cherry::Container()->get('route')->load('dashboard');
          $this->redirect($dashboard);
        } else {
          $message = [
            'type' => 'danger',
            'message' => $this->translate('Login failed because the password is invalid.'),
          ];
        }
      } else {
        $message = [
          'type' => 'danger',
          'message' => $this->translate('Login failed, person could not be retrieved.'),
        ];
      }
    }

    return $this->getRenderer()
      ->setType()
      ->setTemplate('login')
      ->render([
        'message' => $message ?? [],
      ]);
  }

}