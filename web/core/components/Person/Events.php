<?php

namespace Cherry\Person;

use Cherry\Database\Result;
use Cherry\Event\EventListener;
use Cherry\Person\Entity\Person;

/**
 * Class Events
 *
 * @package Cherry\Person
 */
class Events extends EventListener {

  /**
   * {@inheritDoc}
   */
  public function addSearchResults(?array &$results, string $keyword) {
    $results['People'] = [];
    $keyword = '%' . $keyword . '%';

    $query = \Cherry::Database()
      ->select('entity__person')
      ->fields(NULL, ['id'])
      ->condition('name', $keyword, 'LIKE')
      ->orderBy('id', 'DESC')
      ->limit(0, 10)
      ->execute();

    if (!$query instanceof Result) {
      return FALSE;
    }
    $result = $query->fetchAllAssoc();
    foreach ($result as $item) {
      $results['People'][] = Person::load($item['id']);
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function FormAlter(?array &$form, string $form_id) {
    if ($form_id !== 'entity-edit-form') {
      return;
    }

    if (!isset($form['password']) || !isset($form['status'])) {
      return;
    }

    unset($form['password']);
    unset($form['status']);
  }

}