<?php

namespace Cherry\Person\Entity;

use Cherry\Entity\EntityInterface;

/**
 * Interface PersonInterface
 *
 * @package Cherry\Person
 */
interface PersonInterface extends EntityInterface {

  /**
   * Changes UID to the given ID
   *
   * @param int  $id
   * @param bool $rememberMe
   */
  public static function changeTo(int $id, bool $rememberMe = FALSE);

  /**
   * Destroys a person's session
   */
  public static function logout();

  /**
   * @return string
   */
  public function getTitle(): string;

  /**
   * @return array|mixed|NULL|string
   */
  public function getName();

  /**
   * @param string $name
   *
   * @return self
   */
  public function setName(string $name): self;

  /**
   * @return string
   */
  public function getLanguage(): string;

  /**
   * @param string $language
   *
   * @return self
   */
  public function setLanguage(string $language): self;

  /**
   * @param string $password
   *
   * @return bool
   */
  public function checkPassword(string $password): bool;

  /**
   * @return array
   */
  public function getPermissions(): array;

  /**
   * @param array $permissions
   *
   * @return self
   */
  public function setPermissions(array $permissions): self;

  /**
   * Checks if person has permission
   *
   * @param string $permission
   *
   * @return bool
   */
  public function hasPermission(string $permission): bool;

}