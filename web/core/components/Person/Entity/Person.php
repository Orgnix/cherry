<?php

namespace Cherry\Person\Entity;

use Cherry\Entity\Entity;
use Cherry\Session;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class Person
 *
 * @package Cherry\Person
 */
class Person extends Entity implements PersonInterface {

  /**
   * Person constructor.
   *
   * @param null|array $values
   */
  public function __construct($values = NULL) {
    $this->setType('person');
    $this->setValues($values);
  }

  /**
   * {@inheritDoc}
   */
  public static function changeTo(int $id, bool $rememberMe = FALSE) {
    $session = new Session();
    $session->set('PersonID', $id);
    $session->save();
    if ($rememberMe) {
      $cookie = Cookie::create('PersonID', $id, strtotime('now + 1 year'), '/', $_SERVER['HTTP_HOST'] ?? NULL);
      \Cherry::Cookie($cookie)->set();
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function logout() {
    session_destroy();
    static::changeTo(0);
    $session = new Session();
    $session->remove('PersonID');
    \Cherry::Cookie()->remove('PersonID');
  }

  /**
   * @return int
   */
  public static function getCurrentPerson(): int {
    $session = new Session();
    return $session->get('PersonID', 0);
  }

  /**
   * @return array
   */
  public static function initialFields(): array {
    $language_options = \Cherry::Container()->get('language.manager')->getAvailableLanguages();
    foreach ($language_options as $langcode => $language) {
      $language_options[$langcode] = '[' . $langcode . '] ' . $language['language'] . ' - ' . $language['country'];
    }

    $permissions = [];
    foreach (\Cherry::Container()->get('permission.manager')->getAllPermissions() as $permission) {
      $permissions[$permission->getPermission()] = $permission->getPermission();
    }
    return [
      'name' => [
        'type' => 'varchar',
        'length' => 255,
        'unique' => TRUE,
        'form' => [
          'title' => 'Name',
          'type' => 'textbox',
          'attributes' => [
            'type' => 'username',
          ],
        ],
      ],
      'password' => [
        'type' => 'varchar',
        'length' => '255',
        'form' => [
          'title' => 'Password',
          'type' => 'textbox',
          'attributes' => [
            'type' => 'password',
          ],
        ],
      ],
      'language' => [
        'type' => 'varchar',
        'length' => '10',
        'form' => [
          'title' => 'Language',
          'type' => 'select',
          'options' => $language_options,
        ],
      ],
      'permissions' => [
        'type' => 'blob',
        'serialize' => TRUE,
        'form' => [
          'title' => 'Permissions',
          'type' => 'select',
          'options' => $permissions,
          'attributes' => [
            'multiple' => 'multiple',
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getValues(): array {
    $values = parent::getValues();
    unset($values['password']);
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle(): string {
    return $this->getName();
  }

  /**
   * {@inheritDoc}
   */
  public function getName(): string {
    return $this->getValue('name');
  }

  /**
   * {@inheritDoc}
   */
  public function setName(string $name): self {
    return $this->setValue('name', $name);
  }

  /**
   * {@inheritDoc}
   */
  public function getLanguage(): string {
    return $this->getValue('language');
  }

  /**
   * {@inheritDoc}
   */
  public function setLanguage(string $language): self {
    return $this->setValue('language', $language);
  }

  /**
   * {@inheritDoc}
   */
  public function checkPassword(string $password): bool {
    return password_verify($password, $this->getValue('password'));
  }

  /**
   * {@inheritDoc}
   */
  public function getPermissions(): array {
    return unserialize($this->getValue('permissions')) ?: [];
  }

  /**
   * {@inheritDoc}
   */
  public function setPermissions(array $permissions): self {
    return $this->setValue('permissions', serialize($permissions));
  }

  /**
   * {@inheritDoc}
   */
  public function hasPermission(string $permission): bool {
    if ($this->id() === 1) {
      return TRUE;
    }

    $permissions = $this->getPermissions();
    foreach ($permissions as $item) {
      if ($item === $permission) {
        return TRUE;
      }
    }
    return FALSE;
  }

}