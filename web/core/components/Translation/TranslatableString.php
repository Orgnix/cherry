<?php

namespace Cherry\Translation;

use Cherry\Language\LanguageInterface;

/**
 * Class TranslatableString.
 *
 * @package Cherry\Translation
 */
class TranslatableString implements TranslatableStringInterface {

  /** @var string $untranslatedString */
  protected string $untranslatedString = '';

  /** @var string $translatedString */
  protected string $translatedString = '';

  /** @var null|LanguageInterface $language */
  protected ?LanguageInterface $language;

  /** @var array $args */
  protected array $args;

  /**
   * TranslatableString constructor.
   *
   * @param string                 $untranslated_string
   * @param string                 $translated_string
   * @param array                  $args
   * @param LanguageInterface|null $language
   */
  public function __construct(string $untranslated_string, string $translated_string, array $args, ?LanguageInterface $language = NULL) {
    $this->untranslatedString = $untranslated_string;
    $this->translatedString = $translated_string;
    $this->args = $args;
    $this->language = $language;
  }

  /**
   * Returns untranslated string.
   *
   * @return string
   */
  public function getUntranslatedString(): string {
    return $this->untranslatedString;
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->translatedString;
  }

  /**
   * Returns translation arguments
   *
   * @return array
   */
  public function getArguments(): array {
    return $this->args;
  }

  /**
   * Returns the translation language for this string.
   *
   * @return LanguageInterface
   */
  public function getLanguage(): LanguageInterface {
    return $this->language;
  }

}
