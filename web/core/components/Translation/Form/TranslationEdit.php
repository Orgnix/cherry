<?php

namespace Cherry\Translation\Form;

use Cherry;
use Cherry\Database\Result;
use Cherry\Form\Form;
use Cherry\Form\FormInterface;
use Cherry\Language\LanguageManager;

/**
 * Class SiteSettingsForm
 *
 * @package Cherry\Translation\Form
 */
class TranslationEdit extends Form {

  /**
   * SiteSettingsForm constructor.
   *
   * @param int $id
   */
  public function __construct(int $id = 0) {
    parent::__construct();
    /** @var LanguageManager $languageManager */
    $languageManager = \Cherry::Container()->get('language.manager');
    $this->setId('translation-edit-form');

    $storage = \Cherry::Database()
      ->select('translations')
      ->fields(NULL, ['string', 'translation', 'from_langcode', 'to_langcode'])
      ->condition('id', $id)
      ->execute();

    if (!$storage instanceof Result) {
      \Cherry::Logger()->addFailure('Translation with ID ' . $id . ' not found in database.');
      return;
    }
    $translation = $storage->fetchAllAssoc();
    if ($storage->getCount() > 0) {
      $translation = reset($translation);
    }

    if (isset($translation['from_langcode']) && isset($translation['to_langcode'])) {
      $from_language = $languageManager->getLanguageByLangcode($translation['from_langcode']);
      $to_language = $languageManager->getLanguageByLangcode($translation['to_langcode']);
    } else {
      $from_language = $to_language = $languageManager->getDefaultLanguage();
    }

    $this->setFields([
      'tid' => [
        'form' => [
          'type' => 'hidden',
          'default_value' => $id,
        ],
      ],
      'string' => [
        'form' => [
          'type' => 'text',
          'title' => $this->translate('Original string'),
          'default_value' => $translation['string'] ?? '',
          'attributes' => [
            'type' => 'text',
          ],
        ],
      ],
      'translation' => [
        'form' => [
          'type' => 'textbox',
          'title' => $this->translate('Translation'),
          'default_value' => $translation['translation'] ?? '',
          'attributes' => [
            'type' => 'text',
            'placeholder' => 'Translation',
          ],
        ],
      ],
      'from_langcode' => [
        'form' => [
          'type' => 'text',
          'title' => $this->translate('Original language'),
          'default_value' => $from_language->getLanguage() ?? '',
          'attributes' => [
            'type' => 'text',
          ],
        ],
      ],
      'to_langcode' => [
        'form' => [
          'type' => 'text',
          'title' => $this->translate('Translation language'),
          'default_value' => $to_language->getLanguage() ?? '',
          'attributes' => [
            'type' => 'text',
          ],
        ],
      ],
      'submit' => [
        'form' => [
          'type' => 'button',
          'text' => $this->translate('Save'),
          'attributes' => [
            'type' => 'submit',
          ],
          'classes' => [
            'btn-success'
          ],
          'handler' => [$this, 'submitForm'],
        ],
      ],
    ]);
  }

  /**
   * Submits translation.
   *
   * @param array $form
   * @param array $values
   *
   * @throws \Exception
   */
  public function submitForm(array $form = [], array $values = []) {
    try {
      $storage = \Cherry::Database()
        ->update('translations')
        ->values([
          'translation' => $values['translation'],
        ])
        ->condition('id', $values['tid']);
      $storage->execute();
    } catch (\Error $e) {
      die($e->getMessage());
    }

    \Cherry::Logger()->addSuccess('Successfully saved translation ' . $values['tid'] . '.', 'appearance');
  }

}
