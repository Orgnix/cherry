<?php

namespace Cherry\Translation;

/**
 * Class TranslationTempStorage.
 *
 * @package Cherry\Translation
 */
class TranslationTempStorage {

  /**
   * The stored translations.
   *
   * @var array
   */
  protected static array $translations = [];

  /**
   * A random string given to translations w/o context.
   *
   * @var string
   */
  public static string $uuid;

  /**
   * Checks whether translation exists.
   *
   * @param string      $to_langcode
   * @param string      $original_text
   * @param string|null $context
   *
   * @return bool
   */
  public static function hasTranslation(string $to_langcode, string $original_text, ?string $context = NULL): bool {
    return static::getTranslation($to_langcode, $original_text, $context) !== FALSE;
  }

  /**
   * Gets translation from temp storage. Returns false if translation does not exist.
   *
   * @param string      $to_langcode
   * @param string      $original_text
   * @param string|null $context
   *
   * @return false|string
   */
  public static function getTranslation(string $to_langcode, string $original_text, ?string $context = NULL) {
    if (!empty($context)) {
      return static::$translations[$to_langcode][$context][$original_text] ?? FALSE;
    } else {
      return static::$translations[$to_langcode][static::$uuid][$original_text] ?? FALSE;
    }
  }

  /**
   * Adds translation to temp storage.
   *
   * @param string      $to_langcode
   * @param string      $original_text
   * @param string      $translation
   * @param string|null $context
   */
  public static function addTranslation(string $to_langcode, string $original_text, string $translation, ?string $context = NULL) {
    if (!empty($context)) {
      static::$translations[$to_langcode][$context][$original_text] = $translation;
    } else {
      static::$translations[$to_langcode][static::$uuid][$original_text] = $translation;
    }
  }

}
