<?php

namespace Cherry\Translation;

use Cherry;
use Cherry\Event\EventListener;
use Twig\TwigFunction;

/**
 * Class Breadcrumb
 *
 * @package Cherry\Translation
 */
class Events extends EventListener {
  use StringTranslation;

  /**
   * {@inheritDoc}
   */
  public function twigExtensionsAlter(?array &$functions) {
    $functions[] = new TwigFunction('translate', [$this, 'getTranslation']);
  }

  /**
   * Translates string.
   *
   * @param string      $string
   * @param array       $args
   * @param array       $options
   * @param string|null $context
   *
   * @return TranslatableStringInterface
   */
  public function getTranslation(string $string, array $args = [], array $options = [], ?string $context = NULL) {
    return $this->translate($string, $args, $options, $context);
  }

}
