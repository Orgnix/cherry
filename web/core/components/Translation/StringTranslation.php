<?php

namespace Cherry\Translation;

use Cherry\Language\LanguageInterface;
use Cherry\Language\LanguageManager;

/**
 * Trait StringTranslation
 *
 * @package Cherry\Translation
 */
trait StringTranslation {

  /**
   * Translation instance, correct usage would be to enter a literal string, for example:
   *       $color = 'brown'; $itemColor = 'yellow'; $item = 'fence';
   *       StringTranslation::translate('The :color fox jumps over the :item_color :item', [':color' => $color,
   *       ':item_color' => $itemColor, ':item' => $item]); This ensures the proper handling of variables in string
   *       translations for dynamic reusage of the string. This method can also be used to stack translations, for
   *       example:
   *       $companySuffix = StringTranslation::translate('Incorporated');
   *       $companyName = StringTranslation::translate('myCompany :suffix', [':suffix' => $companySuffix]);
   *       StringTranslation::translate('Welcome to :company', [':company' => $companyName]);
   *
   * @param string $string
   * @param array $args
   * @param array $options
   * @param string|null $context
   *
   * @return TranslatableStringInterface
   */
  public function translate(string $string, array $args = [], array $options = [], ?string $context = NULL): TranslatableStringInterface {
    if (!is_string($string)) {
      \Cherry::Logger()->addError('Only strings should be entered.', 'Translation');

      return new TranslatableString(
        $string,
        '',
        [],
      );
    }

    /** @var TranslationInterface $translation */
    $translation = \Cherry::Container()->get('translation');
    /** @var LanguageManager $languageManager */
    $languageManager = \Cherry::Container()->get('language.manager');

    $to_langcode = (isset($options['language']) && $options['language'] instanceof LanguageInterface)
      ? $options['language']->getLangcode()
      : $languageManager->getCurrentLanguage()->getLangcode();

    if (!$translation->exists($string, $to_langcode, $context)) {
      if (!$translation->set($string, $string, 'en', $to_langcode, $context)) {
        \Cherry::Logger()->addFailure('Something went wrong trying to set a translation.', 'StringTranslation');
      }
    }

    return $translation->get($string, $args, $options['fallback'] ?? NULL, $to_langcode, $context);
  }

}
