<?php

namespace Cherry\Translation\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Database\Result;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Logs
 *
 * @package Cherry\Translation
 */
class Overview extends Page {

  /**
   * {@inheritDoc}
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'logs',
      'title' => $this->translate('Logs'),
      'summary' => $this->translate('Shows recent logs'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['view translations']);
  }

  /**
   * {@inheritDoc}
   */
  protected function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'page.translation.overview',
      'context' => 'page',
      'tags' => ['translation.overview'],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];
    $this->caching['max-age'] = 0;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    $current_language = \Cherry::Container()->get('language.manager')->getCurrentLanguage();
    kint($current_language);

    $storage = \Cherry::Database()
      ->select('translations')
      ->fields(NULL, ['id', 'string', 'translation', 'from_langcode', 'to_langcode'])
      ->orderBy('string', 'ASC')
      ->condition('to_langcode', $current_language->getLangcode())
      ->execute();

    if (!$storage instanceof Result) {
      return '';
    }

    $translations = $storage->fetchAllAssoc('id');
    ksort($translations);

    kint($translations);
  }

}