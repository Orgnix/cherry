<?php

namespace Cherry\Translation\Pages;

use Cherry;
use Cherry\Form\FormInterface;
use Cherry\Page\Page;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;
use Cherry\Translation\Form\TranslationEdit;

/**
 * Class Site
 *
 * @package Cherry\Config\Pages
 */
class Edit extends Page {

  /**
   * Config constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'page.translation.edit',
      'title' => $this->translate('Edit translation'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['edit translations']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.translation.edit',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * @return FormInterface
   */
  protected function editForm(int $id) {
    return new TranslationEdit($id);
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    if (!$this->hasParameter('tid')) {
      return '';
    }

    $form = $this->editForm((int) $this->get('tid'))->result();

    return $this->getRenderer()
      ->setType()
      ->setTemplate('translation')
      ->render([
        'title' => $this->translate('Edit translation'),
        'form' => $form,
      ]);
  }

}