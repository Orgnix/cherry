<?php

namespace Cherry\Translation;

use Cherry\Utils\Strings\StringableInterface;

/**
 * Interface TranslatableStringInterface
 *
 * @package Cherry\Translation
 */
interface TranslatableStringInterface extends StringableInterface {

}
