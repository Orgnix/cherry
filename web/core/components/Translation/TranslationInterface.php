<?php

namespace Cherry\Translation;

/**
 * Interface TranslationInterface
 *
 * @package Cherry\Translation
 */
interface TranslationInterface {

  /**
   * Checks whether a translation exists, and tries to set it if not.
   *
   * @param string $string
   * @param null $langcode
   * @param null $context
   *
   * @return bool
   */
  public function exists(string $string, $langcode = NULL, $context = NULL): bool;

  /**
   * Gets translation of string if it exists.
   * If fallback is TRUE and there is no translation, it will return
   *   the original string.
   *
   * @param string      $string
   * @param array       $args
   * @param bool        $fallback
   * @param null|string $langcode
   * @param null|string $context
   *
   * @return TranslatableStringInterface
   */
  public function get(string $string, $args = [], $fallback = TRUE, $langcode = NULL, $context = NULL): TranslatableStringInterface;

  /**
   * Sets string translation
   *
   * @param string      $string
   * @param string      $translation
   * @param null|string $from_langcode
   * @param null|string $to_langcode
   * @param null|string $context
   *
   * @return bool
   */
  public function set(string $string, string $translation, $from_langcode = NULL, $to_langcode = NULL, $context = NULL): bool;

}
