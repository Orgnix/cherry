<?php

namespace Cherry\Translation;

use Cherry;
use Cherry\Database\Result;
use Cherry\Language\LanguageManager;
use Cherry\Utils\Strings\StringManipulation;

/**
 * Class Translation
 *
 * @package Cherry\Translation
 */
class Translation implements TranslationInterface {

  /** @var LanguageManager $languageManager */
  protected LanguageManager $languageManager;

  /**
   * Translation constructor.
   *
   * @param LanguageManager $languageManager
   */
  public function __construct(LanguageManager $languageManager) {
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritDoc}
   */
  public function exists(string $string, $langcode = NULL, $context = NULL): bool {
    $query = \Cherry::Database()
      ->select('translations')
      ->fields(NULL, ['translation'])
      ->condition('string', $string)
      ->orderBy('string', 'ASC');

    if (!is_null($langcode)) {
      $query->condition('to_langcode', $langcode);
    }
    if (!is_null($context)) {
      $query->condition('context', $context);
    }
    $query = $query->execute();

    if (!$query instanceof Result) {
      return FALSE;
    }

    $result = $query->fetchAllAssoc();
    if (count($result) === 0) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function get(string $string, $args = [], $fallback = TRUE, $langcode = NULL, $context = NULL): TranslatableStringInterface {
    $langcode = $langcode ?: $this->languageManager->getCurrentLanguage()->getLangcode();

    /**
     * Check whether translation is already saved in temp storage.
     * That way we can skip unnecessary queries, and retrieve data from our temp storage.
     */
    if (TranslationTempStorage::hasTranslation($langcode, $string, $context)) {
      return new TranslatableString(
        $string,
        $this->replaceArgs(TranslationTempStorage::getTranslation($langcode, $string, $context), $args),
        $args,
        $this->languageManager->getLanguageByLangcode($langcode)
      );
    }

    $query = \Cherry::Database()
      ->select('translations')
      ->fields(NULL, ['translation'])
      ->condition('string', $string)
      ->orderBy('string', 'ASC');

    if (!is_null($langcode)) {
      $query->condition('to_langcode', $langcode);
    }
    if (!is_null($context)) {
      $query->condition('context', $context);
    }

    $query = $query->execute();
    if (!$query instanceof Result) {
      if ($fallback) {
        return new TranslatableString(
          $string,
          $this->replaceArgs($string, $args),
          $args,
          $this->languageManager->getLanguageByLangcode($langcode)
        );
      } else {
        return new TranslatableString(
          '',
          '',
          [],
          $this->languageManager->getLanguageByLangcode($langcode)
        );
      }
    }

    $result = $query->fetchAllAssoc();
    if (count($result) == 0) {
      if ($fallback) {
        return new TranslatableString(
          $string,
          $this->replaceArgs($string, $args),
          $args,
          $this->languageManager->getLanguageByLangcode($langcode)
        );
      } else {
        return new TranslatableString(
          '',
          '',
          [],
          $this->languageManager->getLanguageByLangcode($langcode)
        );
      }
    }

    $result = reset($result);
    TranslationTempStorage::addTranslation($langcode, $string, $result['translation'], $context);
    return new TranslatableString(
      $string,
      $this->replaceArgs($result['translation'], $args),
      $args,
      $this->languageManager->getLanguageByLangcode($langcode)
    );
  }

  /**
   * {@inheritDoc}
   */
  public function set(string $string, string $translation, $from_langcode = NULL, $to_langcode = NULL, $context = NULL): bool {
    $from_langcode = $from_langcode ?? $this->languageManager->getDefaultLanguage()->getLangcode();
    $to_langcode = $to_langcode ?? $this->languageManager->getCurrentLanguage()->getLangcode();

    // Fire an event before adding/saving the translation
    \Cherry::Event('stringTranslationPresave')
      ->fire($translation, [$string, $from_langcode, $to_langcode]);

    $trans = $this->get($string, [], FALSE);
    if ($trans == '') {
      $query = \Cherry::Database()
        ->insert('translations')
        ->values([
          'string' => $string,
          'translation' => $translation,
          'from_langcode' => $from_langcode ?: '',
          'to_langcode' => $to_langcode ?: '',
          'context' => $context ?: '',
        ])
        ->execute();
    } else {
      $query = \Cherry::Database()
        ->update('translations')
        ->condition('string', $string)
        ->condition('from_langcode', $from_langcode ?: '')
        ->condition('to_langcode', $to_langcode ?: '')
        ->condition('context', $context ?: '')
        ->values([
          'translation' => $translation,
        ])
        ->execute();
    }

    if (!$query) {
      return FALSE;
    }

    \Cherry::Cache()->invalidateTags(['translation.overview']);

    // Fire an event after adding/saving the translation
    \Cherry::Event('stringTranslationPostsave')
      ->fire($translation, [$string, $from_langcode, $to_langcode]);

    return TRUE;
  }

  /**
   * @param string $string
   * @param array  $args
   *
   * @return string|string[]
   */
  public function replaceArgs(string $string, array $args) {
    $returnString = $string;
    foreach ($args as $arg => $replacement) {
      $returnString = StringManipulation::replace($returnString, $arg, $replacement);
    }
    return $returnString;
  }

}
