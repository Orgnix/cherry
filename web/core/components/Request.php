<?php

namespace Cherry;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

/**
 * Class Request
 *
 * @package Cherry
 */
class Request extends SymfonyRequest {

  /**
   * @return bool
   */
  public function isAjax(): bool {
    return $this->isXmlHttpRequest();
  }

}
