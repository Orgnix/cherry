<?php

namespace Cherry\ExtensionManager;

use Cherry\Database\Result;
use Cherry\Utils\Strings\StringManipulation;
use Cherry\YamlReader;

/**
 * Class Extension
 *
 * @package Cherry\ExtensionManager
 */
class Extension {

  /** @var string|null $extension */
  protected ?string $extension = NULL;

  /**
   * The extension manager.
   *
   * @var \Cherry\ExtensionManager\ExtensionManagerInterface
   */
  protected ExtensionManagerInterface $extensionManager;

  /** @var array $info */
  protected array $info = [];

  /** @var string $type */
  protected string $type;

  /** @var null|InstallInterface $installObject */
  protected ?InstallInterface $installObject;

  /**
   * Extension constructor.
   *
   * @param string|null $extension
   * @param \Cherry\ExtensionManager\ExtensionManagerInterface|null $extension_manager
   */
  public function __construct(?string $extension = NULL, ?ExtensionManagerInterface $extension_manager = NULL) {
    $this->setExtension($extension);
    $this->extensionManager = $extension_manager ?? \Cherry::Container()->get('extension.manager');
  }

  /**
   * Returns machine name of extension
   *
   * @return string|null
   */
  public function getExtension(): ?string {
    return $this->extension;
  }

  /**
   * @param string|null $extension
   *
   * @return self
   */
  public function setExtension(?string $extension): self {
    $this->extension = $extension;
    $this->setInfo($this->getInfoFromFile());
    $this->setInstallObject();

    return $this;
  }

  /**
   * Returns type of extension
   *
   * @return string|null
   */
  public function getType(): ?string {
    return $this->type;
  }

  /**
   * @param string $type
   *
   * @return self
   */
  public function setType(string $type): self {
    $this->type = $type;

    return $this;
  }

  /**
   * Returns info for extension
   *
   * @return array|false|mixed|string[]
   */
  protected function getInfoFromFile() {
    return YamlReader::readExtension($this->getExtension())
        ?: YamlReader::readComponent($this->getExtension())
        ?: FALSE;
  }

  /**
   * Returns info for extension
   *
   * @return array
   */
  public function getInfo(): array {
    return $this->info;
  }

  /**
   * @param array|bool $info
   *
   * @return Extension
   */
  protected function setInfo($info): self {
    if (is_file(__DIR__ . '/../../../core/components/' . $this->getExtension() . '/extension.info.yml')) {
      $this->setType('component');
    } elseif (is_file(__DIR__ . '/../../../core/extensions/' . $this->getExtension() . '/extension.info.yml')
           || is_file(__DIR__ . '/../../../extensions/' . $this->getExtension() . '/extension.info.yml')) {
      $this->setType('extension');
    } else {
      $this->setType('');
    }

    if (!$info) {
      return $this;
    }
    $this->info = $info;

    return $this;
  }

  /**
   * Returns whether extension is installed
   *
   * @return bool
   */
  public function isInstalled(): bool {
    $extensions = $this->extensionManager->getInstalledExtensions();
    foreach ($extensions as $extension) {
      if (StringManipulation::lowercase($extension->getExtension())
        === StringManipulation::lowercase($this->getExtension())) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns contents of file
   *
   * @param string $type
   *
   * @return array|false|mixed|string[]
   */
  public function readFile(string $type = 'info') {
    switch($this->getType()) {
      case 'extension':
        return YamlReader::readExtension($this->getExtension(), $type);
      case 'component':
        return YamlReader::readComponent($this->getExtension(), $type);
    }

    return [];
  }

  /**
   * Installs extension
   *
   * @param string $type
   *
   * @return bool
   */
  public function install(string $type): bool {
    // @TODO: Validate extension, check compatibility, dependencies
    $extension_storage = \Cherry::Database()->select('extensions')
      ->condition('name', $this->getExtension());
    $result = $extension_storage->execute();
    if (!$result instanceof Result) {
      return FALSE;
    }

    if ($result->getCount() == 1) {
      $ext = \Cherry::Database()->update('extensions')
        ->values(['installed' => 1])
        ->condition('name', $this->getExtension());
    } else {
      $ext = \Cherry::Database()->insert('extensions')
        ->values([
          'type' => $type,
          'name' => $this->getExtension(),
          'installed' => 1,
        ]);
    }

    $ext_upd = \Cherry::Database()
      ->insert('extension_updates')
      ->values(['number' => -1, 'name' => $this->getExtension()]);

    if (!$ext->execute() || !$ext_upd->execute()) {
      return FALSE;
    }

    try {
      if (method_exists($this->installObject, 'install')) {
        $this->installObject->install();
      }
    } catch (\Exception $e) {
      \Cherry::Logger()->addError($e->getMessage(), 'Extension');
    }

    \Cherry::Logger()->addSuccess('Installed extension ' . $this->getExtension(), 'Extensions');
    return TRUE;
  }

  /**
   * Updates extension
   *
   * @param int|null $n
   *
   * @return bool
   */
  public function update(?int $n = NULL): bool {
    if ($n === NULL) {
      $extension_storage = \Cherry::Database()
        ->select('extension_updates')
        ->condition('name', $this->getExtension());
      $result = $extension_storage->execute();
      if (!$result instanceof Result) {
        return FALSE;
      }

      $n = reset($result);
      $n = $n['number'] + 1;
    }

    try {
      if (method_exists($this->installObject, 'update_' . $n)) {
        $this->installObject->{'update_' . $n}();
        \Cherry::Database()
          ->update('extension_updates')
          ->condition('name', $this->getExtension())
          ->values(['number' => $n])
          ->execute();
        \Cherry::Logger()->addSuccess('Updated extension ' . $this->getExtension() . ' to number ' . $n, 'Extensions');
        return TRUE;
      }
    } catch (\Exception $e) {
      \Cherry::Logger()->addFailure($e->getMessage(), 'Extension');
    }

    return FALSE;
  }

  /**
   * Uninstalls extension
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function uninstall(): bool {
    if (!$this->isInstalled()) {
      return FALSE;
    }

    // @TODO: Validate extension, skip required extensions
    $extension_storage = \Cherry::Database()->update('extensions')
      ->condition('installed', '1')
      ->condition('name', $this->getExtension())
      ->values(['installed' => 0]);
    if (!$extension_storage->execute()) {
      \Cherry::Logger()->addError('Could not uninstall extension.', 'Extension');
      return FALSE;
    }

    try {
      if (method_exists($this->installObject, 'uninstall')) {
        $this->installObject->uninstall();
      }
    } catch (\Exception $e) {
      \Cherry::Logger()->addError($e->getMessage(), 'Extension');
    }

    \Cherry::Logger()->addSuccess('Uninstalled extension ' . $this->getExtension(), 'Extensions');
    return TRUE;
  }

  /**
   * Sets the install object.
   */
  protected function setInstallObject() {
    $installObjectName = '\\Cherry\\' . $this->getExtension() . '\\Install';
    if (class_exists($installObjectName)) {
      /** @var InstallInterface $installObject */
      $this->installObject = new $installObjectName();
    } else {
      $this->installObject = NULL;
    }
  }

}