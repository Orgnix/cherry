<?php

namespace Cherry\ExtensionManager;

use Cherry\Database\Result;

/**
 * Class ExtensionManager
 *
 * @package Cherry
 */
class ExtensionManager implements ExtensionManagerInterface {

  /** @var array $installedExtensions */
  public static array $installedExtensions = [];

  /**
   * {@inheritDoc}
   */
  public function getInstalledExtensions(): array {
    if (empty(static::$installedExtensions)) {
      $extensions_storage = \Cherry::Database()
        ->select('extensions')
        ->fields(NULL, ['name', 'type'])
        ->condition('installed', '1');

      /** @var Result $extensions */
      if (!$extensions = $extensions_storage->execute()) {
        return [];
      }

      static::$installedExtensions = array_map(function($item) {
        return $this->getExtension($item['name']);
      }, $extensions->fetchAllAssoc());
    }

    return static::$installedExtensions;
  }

  /**
   * {@inheritDoc}
   */
  public function uninstallExtension(string $extension): bool {
    return $this->getExtension($extension)->uninstall();
  }

  /**
   * {@inheritDoc}
   */
  public function installExtension(string $extension, string $type): bool {
    return $this->getExtension($extension)->install($type);
  }

  /**
   * {@inheritDoc}
   */
  public function extensionInstalled(string $extension): bool {
    return $this->getExtension($extension)->isInstalled();
  }

  /**
   * {@inheritDoc}
   */
  public function getExtensionInfo(string $extension): array {
    return $this->getExtension($extension)->getInfo();
  }

  /**
   * {@inheritDoc}
   */
  public function isLatestVersion(string $extension): bool {
    // TODO
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function getCoreComponents(): array {
    return array_map(function ($item) {
      return str_replace('core/components/', '', $item);
    }, glob('core/components/*', GLOB_ONLYDIR));
  }

  /**
   * {@inheritDoc}
   */
  public function getCoreExtensions(): array {
    return array_map(function ($item) {
      return $this->getExtension(str_replace('core/extensions/', '', $item));
    }, glob('core/extensions/*', GLOB_ONLYDIR));
  }

  /**
   * {@inheritDoc}
   */
  public function getContribExtensions(): array {
    return array_map(function ($item) {
      return $this->getExtension(str_replace('extensions/', '', $item));
    }, glob('extensions/*', GLOB_ONLYDIR));
  }

  /**
   * {@inheritDoc}
   */
  public function getExtension(string $extension): Extension {
    return new Extension($extension, $this);
  }

}