<?php

namespace Cherry\ExtensionManager;

/**
 * Interface ExtensionManagerInterface.
 *
 * @package Cherry\ExtensionManager
 */
interface ExtensionManagerInterface {

  /**
   * Returns array of installed extensions
   *
   * @return \Cherry\ExtensionManager\Extension[]
   */
  public function getInstalledExtensions(): array;

  /**
   * Uninstalls extension and uses UninstallInterface instance class in namespace of extension
   *
   * @param string $extension
   *
   * @return bool
   */
  public function uninstallExtension(string $extension): bool;

  /**
   * Installs extension and uses InstallInterface instance class in namespace of extension
   *
   * @param string $extension
   * @param string $type
   *
   * @return bool
   */
  public function installExtension(string $extension, string $type): bool;

  /**
   * Returns whether the extension is installed
   *
   * @param string $extension
   *
   * @return bool
   */
  public function extensionInstalled(string $extension): bool;

  /**
   * Returns the information in the extension.yml file.
   *
   * @param string $extension
   *
   * @return array
   */
  public function getExtensionInfo(string $extension): array;

  /**
   * Checks whether extension is latest version
   *
   * @param string $extension
   *
   * @return bool
   */
  public function isLatestVersion(string $extension): bool;

  /**
   * @return array
   */
  public function getCoreComponents(): array;

  /**
   * @return \Cherry\ExtensionManager\Extension[]
   */
  public function getCoreExtensions(): array;

  /**
   * @return \Cherry\ExtensionManager\Extension[]
   */
  public function getContribExtensions(): array;

  /**
   * @param string $extension
   *
   * @return Extension
   */
  public function getExtension(string $extension): Extension;

}
