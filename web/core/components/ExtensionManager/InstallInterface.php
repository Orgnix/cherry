<?php

namespace Cherry\ExtensionManager;

/**
 * Interface InstallInterface.
 *
 * The following methods will be respected:
 *  - install    - Executed when installing.
 *  - uninstall  - Executed when uninstalling.
 *  - update_N   - Executed when running updates.
 *
 * You can use as many update_N methods as you want, starting with 0.
 * For example:
 *
 * @code
 *   public function update_0() {
 *     $article = \Cherry::EntityTypeManager()
 *                    ->getStorage('article')
 *                    ->load(1);
 *     $article->set('title', 'A new title for this article');
 *     $article->save();
 *   }
 * @endcode
 *
 * @package Cherry\ExtensionManager
 */
interface InstallInterface {}