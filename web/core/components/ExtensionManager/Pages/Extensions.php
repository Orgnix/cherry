<?php

namespace Cherry\ExtensionManager\Pages;

use Cherry;
use Cherry\ExtensionManager\ExtensionManagerInterface;
use Cherry\Page\Page;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;

/**
 * Class Extensions
 *
 * @package Cherry\ExtensionManager\Pages
 */
class Extensions extends Page {

  /**
   * Extensions constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'extensions',
      'title' => $this->translate('Extensions'),
      'summary' => $this->translate('Welcome to your Cherry Dashboard!'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['manage extensions']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.extensions',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    /** @var ExtensionManagerInterface $extensionManager */
    $extensionManager = \Cherry::Container()->get('extension.manager');
    $componentList = $extensionManager->getCoreComponents();
    $extensionList = array_merge($extensionManager->getContribExtensions(), $extensionManager->getCoreExtensions());
    $extensions = [];
    foreach ($extensionList as $extension) {
      $extensions[$extension->getExtension()] = $extension->getInfo();
      $extensions[$extension->getExtension()]['installed'] = $extension->isInstalled();
      $extensions[$extension->getExtension()]['latest'] =
        $extensionManager->isLatestVersion($extension->getExtension()); // TODO!
    }
    ksort($extensions);
    $extensions = ['core' => [
        'type' => 'core',
        'name' => 'Cherry Core',
        'version' => \Cherry::Cache()->getData('CHERRY_VERSION') . '.'
          . \Cherry::Cache()->getData('CHERRY_VERSION_RELEASE') . '.'
          . \Cherry::Cache()->getData('CHERRY_VERSION_RELEASE_MINOR') . ' '
          . \Cherry::Cache()->getData('CHERRY_VERSION_STATUS'),
        'latest' => $extensionManager->isLatestVersion('core'),
        'installed' => TRUE,
        'required' => TRUE,
        'description' => "Includes the following components: \n" . implode("\n", $componentList),
      ],
    ] + $extensions;

    $action = NULL;
    if ($this->hasParameter('ext')) {
      $extension = $extensionManager->getExtensionInfo($this->get('ext'));
      if ($this->hasParameter('action')) {
        $action = $this->get('action');
        /** @var RouteInterface $route */
        $route = \Cherry::Container()
          ->get('route')
          ->load('extension.view')
          ->setValue('ext', $this->get('ext'));
        if ($this->get('action') == 'uninstall') {
          if ($this->hasParameter('confirm')) {
            $extensionManager->uninstallExtension($this->get('ext'));
            return $this->redirect($route);
          }
        } elseif ($this->get('action') == 'install') {
          if ($this->hasParameter('confirm')) {
            $extensionManager->installExtension($this->get('ext'), $extension['type']);
            return $this->redirect($route);
          }
        }
      }
    }

    return $this->getRenderer()
      ->setType()
      ->setTemplate('extensions')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'extensions' => $extensions,
        'active' => $this->get('ext') ?? FALSE,
        'action' => $action,
      ]);
  }

}
