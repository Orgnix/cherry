<?php

namespace Cherry\Logger;

use Cherry\ExtensionManager\InstallInterface;

class Install implements InstallInterface {

  /**
   * @inheritDoc
   */
  public function condition() {
    return \Cherry::ElementManager()->getElement('logs') !== NULL;
  }

  /**
   * @inheritDoc
   */
  public function doInstall() {
    return \Cherry::ElementManager()->createElement([
      'id' => 'logs',
      'controller' => '\Cherry\Logger\Elements\Logs',
    ]);
  }
}