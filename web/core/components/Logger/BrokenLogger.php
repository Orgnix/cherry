<?php

namespace Cherry\Logger;

use Cherry;
use Cherry\Utils\Strings\StringableInterface;
use Cherry\Translation\StringTranslation;

/**
 * Class Logger
 *
 * @package Cherry\Logger
 */
class BrokenLogger implements LoggerInterface {
  use StringTranslation;

  /** Logging type constants */
  const TYPE_WARNING = 1;
  const TYPE_ERROR = 2;
  const TYPE_INFO = 3;
  const TYPE_SUCCESS = 4;
  const TYPE_FAILURE = 5;

  /**
   * @param             $message
   *    The message the log should include, should be translated beforehand!
   * @param int         $type
   *    The type of this log (constants in Logger class)
   * @param string      $category
   *    The category this log belongs to
   * @param string|null $trace
   *    The backtrace in string format.
   *
   * @return bool
   */
  protected function add($message, int $type = self::TYPE_INFO, string $category = 'php', string $trace = NULL): bool {
    $trace = $trace ?? debug_backtrace();
    $message = $message instanceof StringableInterface ? $message->__toString() : $message;
    die($message . PHP_EOL . '<pre>' . $trace . '</pre>');
  }

  /**
   * Adds a warning log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addWarning($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_WARNING, $category, $trace);
  }

  /**
   * Adds an error log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addError($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_ERROR, $category, $trace);
  }

  /**
   * Adds an info log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addInfo($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_INFO, $category, $trace);
  }

  /**
   * Adds a success log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addSuccess($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_SUCCESS, $category, $trace);
  }

  /**
   * Adds a failure log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addFailure($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_FAILURE, $category, $trace);
  }

  /**
   * @param null $id
   *
   * @return array|string
   */
  public static function typesToStrings($id = NULL) {
    $list = [
      self::TYPE_WARNING => 'warning',
      self::TYPE_ERROR => 'error',
      self::TYPE_INFO => 'info',
      self::TYPE_SUCCESS => 'success',
      self::TYPE_FAILURE => 'failure',
    ];
    if ($id !== NULL) {
      return $list[$id];
    }
    return $list;
  }

}
