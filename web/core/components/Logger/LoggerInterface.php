<?php

namespace Cherry\Logger;

/**
 * Interface LoggerInterface.
 *
 * @packager Cherry\Logger
 */
interface LoggerInterface {

  /**
   * Adds a warning log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addWarning($message, string $category = 'php', string $trace = NULL);

  /**
   * Adds an error log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addError($message, string $category = 'php', string $trace = NULL);

  /**
   * Adds an info log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addInfo($message, string $category = 'php', string $trace = NULL);

  /**
   * Adds a success log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addSuccess($message, string $category = 'php', string $trace = NULL);

  /**
   * Adds a failure log.
   *
   * @param             $message
   * @param string      $category
   * @param string|null $trace
   */
  public function addFailure($message, string $category = 'php', string $trace = NULL);

  /**
   * @param null $id
   *
   * @return array|string
   */
  public static function typesToStrings($id = NULL);

}
