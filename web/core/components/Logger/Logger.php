<?php

namespace Cherry\Logger;

use Cherry\Cache\CacheInterface;
use Cherry\Container\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cherry\Database\Database;
use Cherry\Person\Entity\Person;
use Cherry\Utils\Strings\StringableInterface;
use Cherry\Translation\StringTranslation;

/**
 * Class Logger
 *
 * @package Cherry\Logger
 */
class Logger implements LoggerInterface, ContainerInjectionInterface {
  use StringTranslation;

  /** Logging type constants */
  const TYPE_WARNING = 1;
  const TYPE_ERROR = 2;
  const TYPE_INFO = 3;
  const TYPE_SUCCESS = 4;
  const TYPE_FAILURE = 5;

  /** @var Database $database */
  protected Database $database;

  /**
   * Logger constructor.
   *
   * @param Database $database
   */
  public function __construct(Database $database) {
    $this->database = $database;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * @param             $message
   *    The message the log should include, should be translated beforehand!
   * @param int         $type
   *    The type of this log (constants in Logger class)
   * @param string      $category
   *    The category this log belongs to
   * @param string|null $trace
   *    The backtrace in string format.
   *
   * @return bool
   */
  protected function add($message, int $type = self::TYPE_INFO, string $category = 'php', string $trace = NULL): bool {
    $trace = $trace ?? debug_backtrace();
    $query = $this->database
      ->insert('logs')
      ->values([
        'id' => 0,
        'type' => $type,
        'page' => $_SERVER['REQUEST_URI'] ?? '',
        'filename' => $_SERVER['SCRIPT_NAME'] ?? '',
        'owner' => Person::getCurrentPerson(),
        'backtrace' => serialize(isSerializable($trace) ? $trace : 'Trace could not be serialized.'),
        'category' => $category,
        'message' => $message instanceof StringableInterface ? $message->__toString() : $message,
        'rendered' => 0,
        'timestamp' => time(), // @TODO: Change to something more dynamic
      ]);
    if (!$query->execute()) {
      return FALSE;
    }
    // Render this page uncacheable as soon as a message has been added.
    \Cherry::KillSwitch()->trigger();
    \Cherry::Cache()->invalidateTags(['logs']);
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function addWarning($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_WARNING, $category, $trace);
  }

  /**
   * {@inheritDoc}
   */
  public function addError($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_ERROR, $category, $trace);
  }

  /**
   * {@inheritDoc}
   */
  public function addInfo($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_INFO, $category, $trace);
  }

  /**
   * {@inheritDoc}
   */
  public function addSuccess($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_SUCCESS, $category, $trace);
  }

  /**
   * {@inheritDoc}
   */
  public function addFailure($message, string $category = 'php', string $trace = NULL) {
    $this->add($message, self::TYPE_FAILURE, $category, $trace);
  }

  /**
   * {@inheritDoc}
   */
  public static function typesToStrings($id = NULL) {
    $list = [
      self::TYPE_WARNING => 'warning',
      self::TYPE_ERROR => 'error',
      self::TYPE_INFO => 'info',
      self::TYPE_SUCCESS => 'success',
      self::TYPE_FAILURE => 'failure',
    ];
    if ($id !== NULL) {
      return $list[$id];
    }
    return $list;
  }

}
