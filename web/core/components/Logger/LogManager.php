<?php

namespace Cherry\Logger;

use Cherry\Container\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cherry\Database\Database;
use Cherry\Database\Result;
use Cherry\Person\Entity\Person;
use Cherry\Person\Entity\PersonInterface;
use Cherry\Translation\StringTranslation;

class LogManager {
  use StringTranslation;

  /** @var Database $database */
  protected Database $database;

  /**
   * Logger constructor.
   *
   * @param Database $database
   */
  public function __construct(Database $database) {
    $this->database = $database;
  }

  /**
   * Truncates logs table.
   */
  public function clear() {
    $this->database->query('TRUNCATE TABLE logs');
    \Cherry::Logger()->addInfo('Cleared all logs', 'Logger');
    \Cherry::Cache()->invalidateTags(['logs']);
  }

  /**
   * Returns unrendered logs.
   *
   * @return array|bool
   *
   * @throws \Exception
   */
  public function getUnrenderedLogs() {
    return $this->getLogs(FALSE, ['rendered' => 0]);
  }

  /**
   * Returns all logs (or based on given conditions).
   *
   * @param bool  $massage
   * @param array $conditions
   *
   * @return array|bool
   *
   * @throws \Exception
   */
  public function getLogs(bool $massage = FALSE, array $conditions = []) {
    $logs = $this->database->select('logs')
      ->orderBy('id', 'DESC');

    foreach ($conditions as $key => $value) {
      $logs->condition($key, $value);
    }

    if (!$logs_result = $logs->execute()) {
      return FALSE;
    }

    $logs = $logs_result->fetchAllAssoc();
    if ($massage) {
      foreach ($logs as &$log) {
        $log['id'] = (int) $log['id'];
        /** @var PersonInterface $owner */
        $owner = Person::load((int) $log['owner']);
        if ($owner) {
          $owner = $owner->getName();
        } else {
          $owner = 'Anonymous';
        }
        $log['owner'] = $owner;
        $log['type'] = Logger::typesToStrings($log['type']);

        if ($log['timestamp'] != 0) {
          $dateTime = new \DateTime();
          $dateTime->setTimestamp($log['timestamp']);
          $log['timestamp'] = $dateTime->format('Y-m-d H:i');
        }
      }
    } else {
      foreach ($logs as &$log) {
        if (!isset($log['id'])) {
          continue;
        }

        $log['id'] = (int) $log['id'];
      }
    }

    return $logs;
  }

  /**
   * Sets a log to be rendered.
   *
   * @param $id
   *
   * @return bool|Result
   */
  public function setRendered($id) {
    $query = $this->database->update('logs')
      ->condition('id', $id)
      ->values(['rendered' => 1]);
    return $query->execute();
  }

  /**
   * Renders logs.
   *
   * @return null
   *
   * @throws \Exception
   */
  protected function renderLogs() {
  }

}
