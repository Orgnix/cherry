<?php

namespace Cherry\Logger\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Page\Page;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;

/**
 * Class Logs
 *
 * @package Cherry\Logger\Pages
 */
class Logs extends Page {

  /**
   * {@inheritDoc}
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'logs',
      'title' => $this->translate('Logs'),
      'summary' => $this->translate('Shows recent logs'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['view logs']);
  }

  /**
   * {@inheritDoc}
   */
  protected function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'page.logs',
      'context' => 'page',
      'tags' => ['logs'],
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];


    if ($this->getRoute()->getRoute() === 'logs.clear') {
      $this->caching = [
        'key' => 'page.logs.clear',
        'context' => 'page',
        'tags' => ['logs.clear'],
        'max-age' => 0,
      ];
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Exception
   */
  public function render() {
    parent::render();

    if ($this->getRoute()->getRoute() === 'logs.clear') {
      \Cherry::Container()->get('log.manager')->clear();
      $this->redirect(\Cherry::Container()
        ->get('route', FALSE)
        ->load('logs'));
    }

    $logs = \Cherry::Container()->get('log.manager')->getLogs(TRUE);
    return $this->getRenderer()
      ->setType()
      ->setTemplate('logs')
      ->render([
        'logs' => $logs,
        'count' => count($logs),
      ]);
  }

}