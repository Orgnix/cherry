<?php

namespace Cherry\Logger\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Logger\Logger;
use Cherry\Page\Page;
use Cherry\Person\Entity\Person;
use Cherry\Person\Entity\PersonInterface;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;

/**
 * Class View
 *
 * @package Cherry\Logger\Pages
 */
class View extends Page {

  /**
   * {@inheritDoc}
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'logs',
      'title' => $this->translate('Logs'),
      'summary' => $this->translate('Shows recent logs'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['view logs']);
  }

  /**
   * {@inheritDoc}
   */
  protected function setCacheOptions($parameters = []): self {
    $this->caching = [
      'key' => 'page.logs.view',
      'context' => 'page',
      'tags' => ['logs'],
//      'max-age' => CacheBase::getDefaultCacheTime(),
      'max-age' => 0,
    ];

    if ($this->hasParameter('id')) {
      $this->caching['tags'][] = 'logs:view:' . $this->get('id');
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Exception
   */
  public function render() {
    parent::render();

    $log_storage = \Cherry::Database()
      ->select('logs', 'l')
      ->fields('l', ['type', 'page', 'filename', 'owner', 'category', 'message', 'timestamp', 'backtrace'])
      ->condition('id', $this->get('lid'))
      ->execute();
    $log_result = $log_storage->fetchAllAssoc();
    $log = reset($log_result);

    $log['type'] = Logger::typesToStrings($log['type']);
    /** @var PersonInterface $owner */
    $owner = Person::load($log['owner']) ?: Person::load(0);
    $log['owner'] = $owner->getName();
    $log['timestamp'] = date('Y-m-d H:i:s', $log['timestamp']);
    $log['backtrace'] = unserialize($log['backtrace']);

    return $this->getRenderer()
      ->setType()
      ->setTemplate('logs.view')
      ->render(['log' => $log]);
  }

}