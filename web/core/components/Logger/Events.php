<?php

namespace Cherry\Logger;

use Cherry\Cache\Cache;
use Cherry\Container\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cherry\Element\ElementInterface;
use Cherry\Event\EventListener;
use Cherry\Route\CurrentRoute;
use Cherry\Route\RouteInterface;

/**
 * Class Events.
 *
 * @package Cherry\Logger
 */
class Events extends EventListener implements ContainerInjectionInterface {

  /** @var RouteInterface $currentRoute */
  protected RouteInterface $currentRoute;

  /**
   * @param CurrentRoute $route
   */
  public function __construct(CurrentRoute $route) {
    $this->currentRoute = $route->getObject();
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function alterElementCacheOptions(?array &$cacheOptions, string $element_id) {
    if ($element_id !== 'header') {
      return;
    }

    $logs = \Cherry::ElementManager()->getElementObject('logs', [], $this->currentRoute);
    $element_tags = $cacheOptions['tags'] ?? [];
    $logs_tags = $logs->getCacheOptions()['tags'] ?? [];

    $cacheOptions['tags'] = Cache::mergeTags($element_tags, $logs_tags);
  }

  /**
   * {@inheritDoc}
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element) {
    if ($element_id !== 'header') {
      return;
    }

    $element->mergeCacheTags(['logs']);
    $variables['elements']['page_top']['logs'] = [
      'render' => \Cherry::ElementManager()
        ->getElementRender('logs', [], $this->currentRoute),
      'weight' => -50,
    ];
  }

}