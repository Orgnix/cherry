<?php

namespace Cherry\Logger\Elements;

use Cherry;
use Cherry\Container\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cherry\Element\Element;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;

/**
 * Class Logs
 *
 * @package Cherry\Logger\Elements
 */
class Logs extends Element {

  /**
   * Logs element constructor.
   *
   * @param array          $parameters
   * @param RouteInterface $route
   * @param Renderer  $renderer.
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'logs',
    ]);
    $this->setPermissions(['view logs']);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions($parameters = []): self {
    // Don't cache Element, since logs are a one time view.
    $this->caching = [
      'key' => 'element.logs',
      'context' => 'element',
      'tags' => ['element:logs'],
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    if (parent::render() === NULL) {
      return NULL;
    }
    $types = \Cherry::Logger()->typesToStrings();
    $logManager = \Cherry::Container()->get('log.manager');

    if (!$results = $logManager->getUnrenderedLogs()) {
      return NULL;
    }
    $renders = [];
    foreach ($results as $result) {
      if (!$render = $this->getRenderer()
        ->setType('logger')
        ->setTemplate($types[$result['type']] ?? 'info')) {
        \Cherry::Logger()->addError('Something went wrong trying to find the log render template file.');
        continue;
      }
      $variables = [
        'message' => $result['message'] . PHP_EOL,
      ];
      $renders[] = $render->render($variables);
      $logManager->setRendered($result['id']);
    }

    return implode('', $renders);
  }

}
