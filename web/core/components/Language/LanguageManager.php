<?php

namespace Cherry\Language;

use Exception;
use Cherry;
use Cherry\Config\Config;
use Cherry\Database\Result;

/**
 * Class LanguageManager
 *
 * @package Cherry\Language
 */
class LanguageManager {

  const CURRENT_LANGUAGE = 'current';
  const DEFAULT_LANGUAGE = 'default';

  /** @var string $defaultLangcode */
  protected static string $defaultLangcode = '';

  /** @var string currentLangcode */
  protected static string $currentLangcode = '';

  /**
   * LanguageManager constructor.
   */
  public function __construct(Config $config) {
    if (empty(static::$defaultLangcode) && empty(static::$currentLangcode)) {
      $this->setLangcode(self::DEFAULT_LANGUAGE, $config->get('site.default_langcode') ?: 'en');
      $this->setLangcode(self::CURRENT_LANGUAGE, \Cherry::CurrentPerson()->getLanguage() ?: $this->getLangcode('default'));
    }
  }

  /**
   * Returns the website's current language
   *
   * @return LanguageInterface
   */
  public function getCurrentLanguage(): LanguageInterface {
    return $this->getLanguageByLangcode($this->getLangcode('current'));
  }

  /**
   * Sets the current language for the current user.
   *
   * @param string $langcode
   */
  public function setCurrentLanguage(string $langcode) {
    $this->setLangcode(self::CURRENT_LANGUAGE, $langcode);
    try {
      \Cherry::CurrentPerson()->setValue('language', $langcode)->save();
    } catch (Exception $exception) {
      \Cherry::Logger()->addFailure($exception, 'LanguageManager');
    }
  }

  /**
   * Returns langcode of either default or current type
   *
   * @param string $type
   *
   * @return string|null
   */
  public function getLangcode(string $type): ?string {
    if (!in_array($type, [static::CURRENT_LANGUAGE, static::DEFAULT_LANGUAGE])) {
      throw new \InvalidArgumentException('Langcode type should be one of LanguageManager::CURRENT_LANGUAGE or LanguageManager::DEFAULT_LANGUAGE');
    }

    switch ($type) {
      case 'current':
        return static::$currentLangcode;
      default:
      case 'default':
        return static::$defaultLangcode;
    }
  }

  /**
   * Sets langcode of either default or current type
   *
   * @param string $type
   * @param string $langcode
   *
   * @return self
   */
  public function setLangcode(string $type, string $langcode): self {
    switch ($type) {
      case 'current':
        static::$currentLangcode = $langcode;
        break;
      default:
      case 'default':
        static::$defaultLangcode = $langcode;
        break;
    }

    return $this;
  }

  /**
   * Returns the website's default language
   *
   * @return LanguageInterface
   */
  public function getDefaultLanguage(): LanguageInterface {
    return $this->getLanguageByLangcode($this->getLangcode(self::DEFAULT_LANGUAGE));
  }

  /**
   * Sets the default language for the whole website.
   *
   * @param string $langcode
   */
  public function setDefaultLanguage(string $langcode) {
    $this->setLangcode(self::DEFAULT_LANGUAGE, $langcode);
    try {
      \Cherry::Container()->get('config')->set('site.default_langcode', $langcode);
    } catch (Exception $exception) {
      \Cherry::Logger()->addFailure($exception->getMessage(), 'LanguageManager');
    }
  }

  /**
   * Returns language object from given langcode.
   *
   * @param string $langcode
   *
   * @return LanguageInterface|bool
   */
  public function getLanguageByLangcode(string $langcode) {
    return \Cherry::Container()->get('language')->setValues($langcode);
  }

  /**
   * Returns all available languages.
   *
   * @return array|bool
   */
  public function getAvailableLanguages() {
    $query = \Cherry::Database()
      ->select('languages')
      ->fields(NULL, ['langcode', 'language', 'country'])
      ->execute();
    if (!$query instanceof Result) {
      return FALSE;
    }

    return $query->fetchAllAssoc('langcode');
  }

}