<?php

namespace Cherry\Language;

use Cherry;
use Cherry\Database\Result;
use Cherry\Translation\StringTranslation;
use Cherry\Utils\Classes\ArrayableClassTrait;

/**
 * Class Language
 *
 * @package Cherry\Language
 */
class Language implements LanguageInterface {
  use StringTranslation;
  use ArrayableClassTrait;

  /** @var string $langcode */
  protected string $langcode;

  /** @var string $language */
  protected string $language;

  /** @var string $fallbackLanguage */
  protected string $fallbackLanguage;

  /** @var string $country */
  protected string $country;

  /** @var bool $default */
  protected bool $default = FALSE;

  /**
   * {@inheritDoc}
   */
  public function getLangcode(): string {
    return $this->langcode;
  }

  /**
   * {@inheritDoc}
   */
  public function getLanguage(): string {
    // Return translated string of language label.
    return $this->translate($this->language);
  }

  /**
   * {@inheritDoc}
   */
  public function getFallbackLanguage(): string {
    return $this->fallbackLanguage;
  }

  /**
   * {@inheritDoc}
   */
  public function getCountry(): string {
    // Return translated string of language label.
    return $this->translate($this->country);
  }

  /**
   * {@inheritDoc}
   */
  public function isDefault(): bool {
    return $this->default;
  }

  /**
   * Sets language values.
   *
   * @param string $langcode
   *
   * @return bool|self
   */
  public function setValues(string $langcode) {
    $this->langcode = $langcode;
    if (!$properties = $this->getProperties()) {
      return FALSE;
    }
    $this->language = $properties['language'];
    $this->fallbackLanguage = $properties['fallback'] ?? 'en';
    $this->country = $properties['country'];
    $this->default = \Cherry::Container()->get('config')->get('site.default_langcode') === $langcode;

    return $this;
  }

  /**
   * Returns info for current langcode.
   *
   * @return array|bool
   */
  protected function getProperties() {
    $query = \Cherry::Database()
      ->select('languages')
      ->fields(NULL, ['language', 'country'])
      ->condition('langcode', $this->getLangcode())
      ->execute();
    if (!$query instanceof Result) {
      return FALSE;
    }
    $result = $query->fetchAllAssoc();

    return reset($result);
  }

}
