<?php

namespace Cherry\Language;

use Cherry\Container\ContainerInjectionInterface;
use Cherry\Element\ElementInterface;
use Cherry\Event\EventListener;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Events
 *
 * @package Cherry\Language
 */
class Events extends EventListener implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Cherry\Language\LanguageManager
   */
  protected LanguageManager $languageManager;

  /**
   * Events constructor.
   *
   * @param \Cherry\Language\LanguageManager $language_manager
   *   The language manager.
   */
  public function __construct(LanguageManager $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('language.manager'));
  }

  /**
   * {@inheritDoc}
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element) {
    if ($element_id !== 'header') {
      return;
    }

    $languages = $this->languageManager->getAvailableLanguages();
    $current_language = $this->languageManager->getCurrentLanguage()->toArray();

    $variables['elements']['navbar']['languages'] = [
      'render' => \Cherry::Container()->get('renderer')
        ->setType('core.Language')
        ->setTemplate('language-picker')
        ->render([
          'languages' => $languages,
          'current_language' => $current_language,
        ]),
      'weight' => -30,
    ];
  }

}