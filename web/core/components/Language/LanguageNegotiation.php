<?php

namespace Cherry\Language;

use Cherry\Config\Config;
use Cherry\Container\ContainerInjectionInterface;
use Cherry\Utils\Callback\Callback;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LanguageNegotiation.
 *
 * @package \Cherry\Language
 */
class LanguageNegotiation implements ContainerInjectionInterface {

  /**
   * Defines the negotiation method weights.
   * Can be defined by the user.
   *
   * @var array
   */
  public static array $negotiationWeights = [];

  /**
   * @param Config $config
   *
   * @throws \Cherry\Queue\Exception\CallbackException
   */
  public function __construct(Config $config) {
    $default_weights = [
      0 => (new Callback())->setClass(self::class)->setMethod('negotiateCookie'),
      1 => (new Callback())->setClass(self::class)->setMethod('negotiateBrowser'),
    ];

    if ($weights = $config->get('language.negotiation.weights')) {
      static::$negotiationWeights = $weights;
    } else {
      static::$negotiationWeights = $default_weights;
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config')
    );
  }

  /**
   * Negotiate language with various defined methods.
   *
   * @return string
   *   Return the negotiated language.
   *
   * @throws \Cherry\Queue\Exception\CallbackException
   */
  public static function negotiate(): string {
    $language = NULL;
    foreach (static::$negotiationWeights as $key => $value) {
      $callback = new Callback();
      $callback->setClass($value[0]);
      $callback->setMethod($value[0]);

      if (!$language = $callback->execute()) {
        continue;
      }
      break;
    }

    return $language ?: 'en';
  }

  /**
   * Negotiates language with cookie.
   *
   * @return bool|string
   */
  protected static function negotiateCookie() {
    $cookie = \Cherry::Cookie()->setValues(['name' => 'cherry_language'])->get();
    if (!$cookie) {
      return FALSE;
    }

    return $cookie;
  }

  /**
   * Negotiates language with browser.
   *
   * @return bool|string
   */
  protected static function negotiateBrowser() {
    return $_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? FALSE;
  }

}
