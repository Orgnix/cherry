<?php

namespace Cherry\Node;

use Cherry\Container\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cherry\Database\Database;
use Cherry\Database\Result;
use Cherry\Event\EventListener;
use Cherry\Node\Entity\Node;

/**
 * Class Events
 *
 * @package Cherry\Node
 */
class Events extends EventListener implements ContainerInjectionInterface {

  /**
   * The database connection.
   *
   * @var Database
   */
  protected Database $database;

  /**
   * Events constructor.
   *
   * @param Database $database
   *   The database.
   */
  public function __construct(Database $database) {
    $this->database = $database;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function addSearchResults(?array &$results, string $keyword) {
    $results['Content'] = [];
    $keyword = '%' . $keyword . '%';

    $nodes = $this->database
      ->setDelimiter('OR')
      ->select('entity__node', 'en')
      ->join('entity__person', 'ep', 'en.owner', 'ep.id')
      ->fields('en', ['id', 'title', 'body'])
      ->condition('en.title', $keyword, 'LIKE')
      ->condition('en.body', $keyword, 'LIKE')
      ->condition('ep.name', $keyword, 'LIKE')
      ->orderBy('en.id', 'DESC')
      ->limit(0, 10)
      ->execute();

    if (!$nodes instanceof Result) {
      return FALSE;
    }

    $result = $nodes->fetchAllAssoc();

    foreach ($result as $item) {
      $results['Content'][] = Node::load($item['id']);
    }
    return TRUE;
  }

}