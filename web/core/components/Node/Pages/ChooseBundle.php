<?php

namespace Cherry\Node\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Node\Form\NodeForm;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class ChooseBundle
 *
 * @package Cherry\Node\Pages
 */
class ChooseBundle extends Page {

  /**
   * Add constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.choose_bundle',
      'title' => $this->translate('Add node'),
      'summary' => $this->translate('Add page for a node'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['add node']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.node.choose_bundle',
      'context' => 'page',
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    $form = new \Cherry\Entity\Form\ChooseBundle(new Cherry\Node\Entity\Node());
    $content = $form->result();

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('add')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'entity' => [
          'type' => 'node',
          'content' => $content,
        ],
      ]);
  }

}
