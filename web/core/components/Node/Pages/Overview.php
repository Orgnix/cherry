<?php

namespace Cherry\Node\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Entity\Entity;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Overview
 *
 * @package Cherry\Node\Pages
 */
class Overview extends Page {

  /**
   * Overview constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'node.overview',
      'title' => $this->translate('Node overview'),
      'summary' => $this->translate('List of existing nodes.'),
    ]);
    parent::__construct($parameters, $route, $renderer);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.node.overview',
      'context' => 'page',
      'max-age' => CacheBase::getDefaultCacheTime(),
      'tags' => ['entity:node:overview'],
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    $manifest = \Cherry::Manifest('node')->fields([
      'id', 'bundle', 'title', 'status', 'owner',
    ]);
    $content = \Cherry::Container()->get('manifest.renderer')
      ->setManifest($manifest)
      ->setViewMode($this->get('viewmode') ?: 'table')
      ->hideField('id')
      ->noLink('owner')
      ->addActionLinks('entity')
      ->render(TRUE);

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('overview')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'entity' => [
          'type' => 'node',
          'content' => $content,
        ],
      ]);
  }

}
