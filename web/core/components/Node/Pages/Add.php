<?php

namespace Cherry\Node\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Add
 *
 * @package Cherry\Node\Pages
 */
class Add extends Page {

  /**
   * Add constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'entity.add',
      'title' => $this->translate('Add ' . $this->get('bundle')),
      'summary' => $this->translate('Add page for a ' . $this->get('bundle') . ' node'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['add ' . $this->get('bundle') . ' node']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.node.' . $this->get('bundle') . '.add',
      'context' => 'page',
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    /** @var Cherry\Node\Entity\NodeInterface $node */
    $node = \Cherry::EntityTypeManager()::getEntityClassFromType('node');
    $node->setValue('bundle', $this->get('bundle'));
    $form = \Cherry::Form($node);
    $content = $form->result();

    return $this->getRenderer()
      ->setType('core.Entity')
      ->setTemplate('add')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'entity' => [
          'type' => 'node',
          'content' => $content,
        ],
      ]);
  }

}
