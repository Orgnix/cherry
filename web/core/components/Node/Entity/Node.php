<?php

namespace Cherry\Node\Entity;

use Cherry\Entity\Entity;
use Cherry\Entity\EntityBundleableTrait;

/**
 * Class Node
 *
 * @package Cherry\Node
 */
class Node extends Entity implements NodeInterface {
  use EntityBundleableTrait;

  /**
   * Node constructor.
   *
   * @param array|null $values
   */
  public function __construct(?array $values = NULL) {
    $this->setValues($values);
    $this->setType('node');
  }

  /**
   * @return array
   */
  public static function initialFields(): array {
    return self::addBundleField() + [
      'title' => [
        'type' => 'varchar',
        'length' => 255,
        'form' => [
          'title' => 'Title',
          'type' => 'textbox',
          'name' => 'title',
          'required' => TRUE,
        ],
      ],
      'body' => [
        'type' => 'text',
        'form' => [
          'title' => 'Body',
          'type' => 'wysiwyg',
          'name' => 'body',
          'attributes' => [
            'rows' => 5,
          ],
          'required' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle(): string {
    return $this->getValue('title');
  }

  /**
   * {@inheritDoc}
   */
  public function setTitle(string $title) {
    return $this->setValue('title', $title);
  }

  /**
   * {@inheritDoc}
   */
  public function getBody(): string {
    return $this->getValue('body');
  }

  /**
   * {@inheritDoc}
   */
  public function setBody(string $body) {
    return $this->setValue('body', $body);
  }

}
