<?php

namespace Cherry\Node\Entity;

use Cherry\Entity\EntityBundleableInterface;
use Cherry\Entity\EntityInterface;

/**
 * Interface NodeInterface
 *
 * @package Cherry\Node
 */
interface NodeInterface extends EntityInterface, EntityBundleableInterface {

  /**
   * Returns node title
   *
   * @return string
   */
  public function getTitle();

  /**
   * Sets node title
   *
   * @param string $title
   *
   * @return bool
   */
  public function setTitle(string $title);

  /**
   * Returns node body
   *
   * @return string
   */
  public function getBody();

  /**
   * Sets node body
   *
   * @param string $body
   *
   * @return bool
   */
  public function setBody(string $body);

}