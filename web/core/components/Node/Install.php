<?php

namespace Cherry\Node;

use Cherry\ExtensionManager\InstallInterface;
use Cherry\Menu\Entity\Menu;

/**
 * Class Install
 *
 * @package Cherry\Node
 */
class Install implements InstallInterface {

  /**
   * {@inheritDoc}
   */
  public function condition() {
    $overview = \Cherry::EntityTypeManager()
      ->getstorage('menu')
      ->loadByProperties([
        'route' => 'node.overview',
      ]);
    return $overview !== FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function doInstall() {
    $menu = new Menu([
      'route' => 'node.overview',
      'title' => 'Content',
      'description' => 'Overview of all nodes',
      'structure' => 0,
      'icon' => 'newspaper',
      'type' => 'link',
      'translatable' => TRUE,
      'parent' => 0,
      'owner' => 1,
      'status' => 1,
    ]);
    if (!$menu->save()) {
      return FALSE;
    }
    \Cherry::Logger()->addInfo('Added menu item', 'Node');
    return TRUE;
  }

}
