<?php

namespace Cherry\Node\Form;

use Cherry;
use Cherry\Entity\EntityInterface;
use Cherry\Form\Form;
use Cherry\Form\FormInterface;
use Cherry\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class NodeForm
 *
 * @package Cherry\Node\Form
 */
class NodeForm extends Form implements FormInterface {

  /**
   * NodeForm constructor.
   *
   * @param EntityInterface|null $entity
   */
  public function __construct(?EntityInterface $entity = NULL) {
    parent::__construct($entity);

    $bundle_storage = \Cherry::EntityTypeManager()
      ->getStorage('bundle')
      ->loadByProperties(['entity_type' => 'node'], TRUE) ?? [];

    $bundles = [];
    /** @var Cherry\Entity\Entity\BundleInterface $bundle */
    foreach ($bundle_storage as $bundle) {
      $bundles[$bundle->getMachineName()] = $bundle->getLabel();
    }

    $fields = [
      'bundle' => [
        'form' => [
          'type' => 'select',
          'title' => $this->translate('Bundle'),
          'options' => $bundles,
        ],
      ],
    ];

    $submit = [
      'submit' => [
        'form' => [
          'type' => 'button',
          'text' => $this->translate('Create'),
          'attributes' => [
            'type' => 'submit',
          ],
          'classes' => [
            'btn-success'
          ],
          'handler' => [$this, 'submitForm'],
        ],
      ],
    ];

    $fields = [
      'fields' => $fields,
      'submit' => $submit,
    ];

    // Submit handler gets added at the end so it doesn't get surpassed by other fields
    \Cherry::Event('nodeFormPreBuild')
      ->fire($fields);

    $fields = $fields['fields'] + $fields['submit'];

    $this->setId('node-form');
    $this->setFields($fields);
  }

  /**
   * @param array $form
   * @param array $values
   */
  public function submitForm(array $form = [], array $values = []) {
    /** @var Cherry\Route\RouteInterface $route */
    $route = \Cherry::Container()->get('route')
      ->load('node.add')
      ->setValue('bundle', $values['bundle']);
    $url = Url::fromRoute($route);
    $response = new RedirectResponse($url);
    $response->send();
  }

}