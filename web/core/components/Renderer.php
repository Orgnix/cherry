<?php

namespace Cherry;

use Cherry\Config\Config;
use Cherry\Language\LanguageManager;
use Cherry\Logger\Logger;
use Cherry\Utils\Arrays\ArrayManipulation;
use Cherry\Utils\ClassStepTrait;
use Cherry\Utils\Strings\StringableInterface;
use Cherry\Utils\Strings\StringManipulation;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;
use Twig\TemplateWrapper;

/**
 * Class Renderer
 *
 * @package Cherry
 */
class Renderer {
  use ClassStepTrait;

  /** @var array $defaultVariables */
  protected static array $defaultVariables = [];

  /** @var string|null $type */
  protected ?string $type;

  /** @var string|null $backup */
  protected ?string $backup;

  /** @var string|null $path */
  protected ?string $path;

  /** @var string $template */
  protected string $template;

  /** @var FilesystemLoader $loader */
  protected FilesystemLoader $loader;

  /** @var Environment $twig */
  protected Environment $twig;

  /** @var string $theme_folder */
  protected string $theme_folder;

  /** @var Theme $theme */
  protected Theme $theme;

  /** @var Logger $logger */
  protected Logger $logger;

  /** @var LanguageManager $languageManager */
  protected LanguageManager $languageManager;

  /** @var Url $url */
  protected Url $url;

  /** @var Config $config */
  protected Config $config;

  /**
   * Renderer constructor.
   */
  public function __construct(Theme $theme, Logger $logger, LanguageManager $language_manager, Url $url, Config $config) {
    $this->setThemeFolder($theme->getThemeFolder());
    $this->theme = $theme;
    $this->logger = $logger;
    $this->languageManager = $language_manager;
    $this->url = $url;
    $this->config = $config;

    if (empty(static::$defaultVariables)) {
      $render_settings = Settings::getAll();
      unset($render_settings['database']);
      static::$defaultVariables = [
        'settings' => $render_settings,
        'site' => [
          'name' => $this->config->get('site.name') ?? 'Cherry',
          'version' => \Cherry::Cache()->getData('CHERRY_VERSION') . '.'
            . \Cherry::Cache()->getData('CHERRY_VERSION_RELEASE') . '.'
            . \Cherry::Cache()->getData('CHERRY_VERSION_RELEASE_MINOR') . ' '
            . \Cherry::Cache()->getData('CHERRY_VERSION_STATUS'),
          'url' => Settings::get('root.web.url'),
        ],
        'theme' => [
          'location' => Settings::get('root.web.url') . DIRECTORY_SEPARATOR . 'themes' . DIRECTORY_SEPARATOR . $this->theme->getTheme('admin'),
        ],
      ];
    }
  }

  /**
   * @return FilesystemLoader
   */
  protected function getLoader() {
    return $this->loader;
  }

  /**
   * @param FilesystemLoader $loader
   *
   * @return self
   */
  protected function setLoader(FilesystemLoader $loader) {
    $this->loader = $loader;
    return $this;
  }

  /**
   * @return Environment
   */
  protected function getTwig() {
    return $this->twig;
  }

  /**
   * @param Environment $twig
   *
   * @return self
   */
  protected function setTwig(Environment $twig) {
    $this->twig = $twig;
    return $this;
  }

  /**
   * @return string
   */
  protected function getThemeFolder() {
    return $this->theme_folder;
  }

  /**
   * @param string $theme_folder
   */
  public function setThemeFolder(string $theme_folder) {
    $this->theme_folder = $theme_folder;
  }

  /**
   * @param string $type
   * @param        $skip
   *
   * @return false|string|null
   */
  protected function checkTypePath(string $type, $skip) {
    $this->type = $type;
    $path = is_null($type) ? $this->getThemeFolder() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR :
      $this->getThemeFolder() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR .
      StringManipulation::replace($type, ['core.', 'extension.'], '') . DIRECTORY_SEPARATOR;

    if (!is_dir($path) || $skip) {
      if (StringManipulation::contains($type, 'extension')) {
        $path = Core::getExtensionsPath() . DIRECTORY_SEPARATOR . StringManipulation::replace($type, 'extension.', '')
          . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        if (!is_dir($path)) {
          $path = Core::getCoreExtensionsPath() . DIRECTORY_SEPARATOR .
            StringManipulation::replace($type, 'extension.', '') . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        }
      } elseif (StringManipulation::contains($type, 'core')) {
        $path = Core::getCoreComponentsPath() . DIRECTORY_SEPARATOR . StringManipulation::replace($type, 'core.', '')
          . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
      }
    }

    if (!is_dir($path)) {
      return FALSE;
    }

    return $path ?? FALSE;
  }

  /**
   * @param string|NULL $type
   * @param string|null $backup
   * @param bool        $skip
   *
   * @return Renderer|NULL
   */
  public function setType(string $type = '', string $backup = '', bool $skip = FALSE) {
    $this->type = $type;
    $this->backup = $backup;
    $this->path = $this->checkTypePath($type, $skip) ?: $this->checkTypePath($backup, $skip);

    $debugging = Settings::get('twig_debugging');
    $this->setLoader(new FilesystemLoader($this->path));
    $this->setTwig(new Environment($this->getLoader(), ['debug' => $debugging]));
    if ($debugging) {
      $this->twig->addExtension(new DebugExtension());
    }
    /** @var TwigExtensions $extensions */
    $extensions = Core::loadClass('\Cherry\TwigExtensions');
    $this->twig->addExtension($extensions);

    return $this;
  }

  /**
   * @return string
   */
  protected function getType() {
    return $this->type;
  }

  /**
   * @param string $template
   * @param string $backup
   *
   * @return Renderer
   */
  public function setTemplate(string $template, string $backup = '') {
    if (!is_file($this->path . StringManipulation::replace($template, '.html.twig', '') . '.html.twig')) {
      if (!is_file($this->path . StringManipulation::replace($backup, '.html.twig', '') . '.html.twig')) {
        $this->setType($this->type, $this->backup, TRUE);
      } else {
        $this->template = $backup;
      }
    } else {
      $this->template = $template;
    }
    return $this;
  }

  /**
   * @return bool|TemplateWrapper
   */
  protected function getTemplate() {
    if (!$this->step('template')) {
      throw new \InvalidArgumentException('The template needs to set before trying to retrieve it.');
    }
    try {
      return $this->getTwig()->load($this->template . '.html.twig');
    } catch (LoaderError | RuntimeError | SyntaxError $e) {
      $this->logger->addFailure($e->getMessage(), 'Renderer');
    }
    return FALSE;
  }

  /**
   * Turns render array into rendered object
   *
   * @param array $variables
   * @param null  $view_mode
   */
  protected function preRender(array &$variables, $view_mode = NULL) {
    if (!isset($variables['content'])) {
      return;
    }

    // Roll over variables
    foreach ($variables['content'] as &$item) {
      if (!is_array($item)) {
        continue;
      }

      if (!isset($item['#type'])) {
        continue;
      }

      $renderer = new static($this->theme, $this->logger, $this->languageManager, $this->url, $this->config);
      $renderer->setType($item['#type']);
      $renderer->setTemplate($item['#theme']);
      $item = $renderer->render($item['#variables'], $view_mode, FALSE);
      unset($renderer);
    }
  }

  /**
   * @param array       $variables
   * @param string|NULL $view_mode
   * @param bool        $refactor
   *
   * @return string|NULL
   */
  public function render(array $variables = [], string $view_mode = NULL, bool $refactor = TRUE): ?string {
    $template = $this->getTemplate();
    if (!$template instanceof TemplateWrapper) {
      return FALSE;
    }

    $variables = array_merge($variables, [
        'current' => [
          'person' => \Cherry::CurrentPerson(),
          'language' => $this->languageManager->getCurrentLanguage(),
          'route' => \Cherry::CurrentRoute(),
          'uri' => $this->url->getUri(),
        ],
      ], static::$defaultVariables);

    \Cherry::Event('preRender')
      ->fire($variables, [$view_mode]);

    if ($refactor) {
      $this->preRender($variables);
    }

    // Check for translatable interfaces in the variables and replace them with renderable strings.
    ArrayManipulation::recursiveAction($variables, $this, 'checkStringableInterface');

    return $template->render($variables) ?? NULL;
  }

  /**
   * Replaces TranslatableInterface items with strings.
   *
   * @param mixed $item
   */
  public function checkStringableInterface(&$item) {
    if (!$item instanceof StringableInterface) {
      return;
    }

    $item = $item->__toString();
  }

}