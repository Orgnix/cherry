<?php

namespace Cherry;

use Symfony\Component\HttpFoundation\Session\Session as SymfonySession;

/**
 * Class Session
 *
 * @package Cherry
 */
class Session extends SymfonySession {

  /**
   * @param string $name
   *
   * @return void
   */
  public function remove(string $name) {
    parent::remove($name);
    if (!isset($_SESSION[$name])) {
      return;
    }
    unset($_SESSION[$name]);
  }

}
