<?php

namespace Cherry\Manifest;

/**
 * Interface ManifestInterface
 *
 * @package Cherry\Manifest
 */
interface ManifestInterface {

  /**
   * Returns array of results
   *
   * @param bool $massage
   *
   * @return array|bool
   */
  public function result($massage = FALSE);

  /**
   * Sets offset and limit for items in query.
   *
   * @param int $limit
   * @param int $offset
   *
   * @return Manifest
   */
  public function limit(int $limit, int $offset): Manifest;

  /**
   * Sets condition parameters
   *
   * @param string $field
   * @param string $value
   * @param string $delimiter
   *
   * @return Manifest
   */
  public function condition(string $field, string $value, $delimiter = '='): Manifest;

  /**
   * @param string $field
   * @param string $direction
   *
   * @return Manifest
   */
  public function order(string $field, $direction = 'ASC'): Manifest;

  /**
   * @param array $fields
   *
   * @return Manifest
   */
  public function fields($fields = []): Manifest;

}