<?php

namespace Cherry\Manifest;

use Cherry\Form\FormElements\Button;
use Cherry\Form\FormElements\Checkbox;
use Cherry\Form\FormElements\Dropbutton;
use Cherry\Form\FormElements\Select;
use Cherry\Form\FormElements\Text;
use Cherry\Form\FormElements\Textbox;
use Cherry\Renderer;
use Cherry\Request;
use Cherry\Translation\StringTranslation;
use Cherry\Url;

/**
 * Class ManifestRenderer
 *
 * @package Cherry\Manifest
 */
class ManifestRenderer {
  use StringTranslation;

  /** @var Request $request */
  protected Request $request;

  /** @var ManifestInterface $manifest */
  protected ManifestInterface $manifest;

  /** @var Renderer $renderer */
  protected Renderer $renderer;

  /** @var string $viewMode */
  protected string $viewMode = 'table';

  /** @var array $hiddenFields */
  protected array $hiddenFields = [];

  /** @var array $noLinkFields */
  protected array $noLinkFields = [];

  /** @var string|bool $actionLinks */
  protected $actionLinks = FALSE;

  /** @var array $filters */
  protected array $filters = [];

  /**
   * EntityRenderer constructor.
   *
   * @param Renderer          $renderer
   */
  public function __construct(Renderer $renderer) {
    $this->setRenderer($renderer);
    $this->setRequest();
  }

  /**
   * @return string
   */
  protected function getViewMode(): string {
    return $this->viewMode;
  }

  /**
   * @param string $viewMode
   *
   * @return ManifestRenderer
   */
  public function setViewMode(string $viewMode): self {
    $this->viewMode = $viewMode;
    return $this;
  }

  /**
   * Returns Entity object
   *
   * @return ManifestInterface
   */
  protected function getManifest(): ManifestInterface {
    return $this->manifest;
  }

  /**
   * Sets Entity object
   *
   * @param ManifestInterface $manifest
   *
   * @return self
   */
  public function setManifest(ManifestInterface $manifest): self {
    $this->manifest = $manifest;
    return $this;
  }

  /**
   * Returns Renderer object
   *
   * @return Renderer
   */
  protected function getRenderer(): Renderer {
    return $this->renderer;
  }

  /**
   * Sets Renderer object
   *
   * @return $this
   */
  protected function setRenderer(Renderer $renderer): self {
    $this->renderer = $renderer;
    return $this;
  }

  /**
   * Returns Renderer object
   *
   * @return Request
   */
  protected function getRequest(): Request {
    return $this->request;
  }

  /**
   * Returns Renderer object
   *
   * @return self
   */
  protected function setRequest(): self {
    $this->request = \Cherry::Container()->get('request');

    return $this;
  }

  /**
   * Visually hides field, data is still available for other fields to manipulate
   *
   * @param string $field
   *
   * @return $this
   */
  public function hideField(string $field): self {
    $this->hiddenFields[$field] = TRUE;
    return $this;
  }

  /**
   * Won't show a link for this field if a link is available.
   * Think about Owner, Title, ...
   *
   * @param string $field
   *
   * @return $this
   */
  public function noLink(string $field): self {
    $this->noLinkFields[$field] = TRUE;
    return $this;
  }

  /**
   *
   *
   * @param string $entityType
   *
   * @return $this
   */
  public function addActionLinks(string $entityType): self {
    $this->actionLinks = $entityType;
    return $this;
  }

  /**
   * @return array
   */
  public function getFilters(): array {
    return $this->filters;
  }

  /**
   * @param string $key
   * @param string $fieldType
   * @param string $operator
   */
  public function addFilter(string $key, string $fieldType = 'textbox', string $operator = '=') {
    $this->filters[] = [
      'key' => $key,
      'fieldType' => $fieldType,
      'operator' => $operator,
    ];
  }

  /**
   * @param $key
   *
   * @return null|string
   */
  public function getFilterValue($key): ?string {
    if (!$this->request->request->has($key)) {
      return NULL;
    }
    return $this->request->request->get($key);
  }

  /**
   * Returns rendered Entity object
   *
   * @param bool $massage
   *
   * @return string|NULL
   */
  public function render(bool $massage = FALSE): ?string {
    $container = \Cherry::Container();
    $manifest = $this->getManifest();
    $fields = $manifest->getFields();
    foreach ($this->getFilters() as $filter) {
      $manifest->condition($filter['key'], $this->getFilterValue($filter['key']), $filter['operator']);
    }
    $results = $manifest->result($massage);

    if ($this->actionLinks !== FALSE) {
      $fields[] = 'actions';
      foreach ($results as &$result) {
        $editRoute = $container->get('route')
          ->load('entity.edit')
          ->setValue('type', $manifest->getType())
          ->setValue('eid', $result['id']);
        $cloneRoute = $container->get('route')
          ->load('entity.clone')
          ->setValue('type', $manifest->getType())
          ->setValue('eid', $result['id']);
        $deleteRoute = $container->get('route')
          ->load('entity.delete')
          ->setValue('type', $manifest->getType())
          ->setValue('eid', $result['id']);
        $dropbutton = Dropbutton::create($container);

        $result['actions'] = $dropbutton->render([
          'formId' => $manifest->getType() . '-overview',
          'key' => 'actions',
          'classes' => ['btn-primary', 'btn-sm'],
          'first' => [
            'text' => $this->translate('Edit'),
            'link' => Url::fromRoute($editRoute),
          ],
          'links' => [
            [
              'text' => $this->translate('Clone'),
              'link' => Url::fromRoute($cloneRoute),
            ],
            [
              'text' => $this->translate('Delete'),
              'link' => Url::fromRoute($deleteRoute),
            ],
          ],
        ]);
      }
    }

    $filters = [];
    foreach ($this->getFilters() as $filter) {
      switch ($filter['fieldType']) {
        case 'checkbox':
          $element = Checkbox::create($container);
          break;
        case 'button':
          $element = Button::create($container);
          break;
        case 'select':
          $element = Select::create($container);
          break;
        case 'text':
          $element = Text::create($container);
          break;
        default:
          $element = Textbox::create($container);
          break;
      }

      $filters[] = $element->render([
        'attributes' => [
          'name' => $filter['key'],
          'value' => $this->getFilterValue($filter['key']),
        ],
      ]);
    }

    return $this->getRenderer()
      ->setType('core.Manifest')
      ->setTemplate($this->getViewMode())
      ->render([
        'type' => $manifest->getType(),
        'fields' => $fields,
        'filters' => $filters,
        'values' => $results,
        'hidden' => $this->hiddenFields,
      ]);
  }

}