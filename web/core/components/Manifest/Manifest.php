<?php

namespace Cherry\Manifest;

use Cherry;
use Cherry\Database\Database;
use Cherry\Database\Result;
use Cherry\Entity\EntityInterface;
use Cherry\Entity\EntityTypeManager;
use Cherry\Entity\EntityRenderer;

/**
 * Class Manifest
 *
 * @package Cherry\Manifest
 */
class Manifest implements ManifestInterface {

  /** @var string $type */
  protected string $type;

  /** @var array $limit */
  protected array $limit = ['limit' => 20, 'offset' => 0];

  /** @var array $condition */
  protected array $condition = [];

  /** @var array $orderBy */
  protected array $orderBy = [];

  /** @var array $fields */
  protected array $fields = ['id'];

  /** @var Database $query */
  protected Database $query;

  /**
   * Manifest constructor
   *
   * @param string $type
   */
  public function __construct(string $type) {
    $this->setType($type);
  }

  /**
   * {@inheritDoc}
   */
  public function limit(int $limit, int $offset): self {
    $this->setLimit([
      'offset' => $offset,
      'limit' => $limit,
    ]);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function condition(string $field, string $value, $delimiter = '='): self {
    $this->setCondition($field, $value, $delimiter);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function order(string $field, $direction = 'ASC'): self {
    $this->setOrderBy($field, $direction);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function fields($fields = []): self {
    $this->setFields($fields);
    return $this;
  }

  /**
   * Adds field to field array.
   *
   * @param $field
   *
   * @return self
   */
  public function addField($field): self {
    $this->fields[] = $field;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function result($massage = FALSE) {
    $query = $this->query();
    if (!$query instanceof Result) {
      return FALSE;
    }
    $results = $query->fetchAllAssoc();
    if ($massage) {
      /** @var EntityInterface $entity */
      $entity = EntityTypeManager::getEntityClassFromType($this->getType());
      foreach ($results as $id => $values) {
        $results[$id] = $entity->massageProperties($values)->getValues();
        foreach ($results[$id] as &$field) {
          if (!$field instanceof EntityInterface) {
            continue;
          }
          $entityRenderer = \Cherry::EntityRenderer($field);
          $field = $entityRenderer->render();
        }
      }
    }
    return $results;
  }

  /**
   * Sets type variable.
   *
   * @param $type
   */
  protected function setType($type) {
    $this->type = $type;
  }

  /**
   * Gets type variable.
   *
   * @return string
   */
  public function getType() {
    return $this->type ?? FALSE;
  }

  /**
   * Sets limit array
   *
   * @param array $limit
   */
  protected function setLimit(array $limit) {
    $this->limit = $limit;
  }

  /**
   * Sets limit array
   *
   * @return array
   */
  protected function getLimit(): array {
    return $this->limit;
  }

  /**
   * Sets condition parameters
   *
   * @param $field
   * @param $value
   * @param $operator
   */
  protected function setCondition($field, $value, $operator) {
    $this->condition[] = [
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * Returns array of conditions
   *
   * @return array
   */
  protected function getConditions(): array {
    return $this->condition;
  }

  /**
   * Sets order field parameters
   *
   * @param string $field
   * @param string $direction
   */
  protected function setOrderBy(string $field, string $direction) {
    $this->orderBy[] = [
      'field' => $field,
      'direction' => $direction,
    ];
  }

  /**
   * Returns array of order fields
   *
   * @return array
   */
  protected function getOrderBy(): array {
    return $this->orderBy;
  }

  /**
   * Sets array of fields
   *
   * @param array $fields
   */
  protected function setFields($fields = []) {
    $this->fields = $fields;
  }

  /**
   * Returns array of fields
   *
   * @return array
   */
  public function getFields(): array {
    return $this->fields;
  }

  /**
   * Builds and executes query.
   *
   * @return Result|bool
   */
  protected function query() {
    $query = \Cherry::Database()
      ->select('entity__' . $this->getType())
      ->fields(NULL, $this->getFields());
    // Add conditions
    foreach ($this->getConditions() as $condition) {
      $query->condition($condition['field'], $condition['value'], $condition['operator']);
    }
    // Add order fields and directions
    foreach ($this->getOrderBy() as $order) {
      $query->orderBy($order['field'], $order['direction']);
    }
    // Add limits
    $limit = $this->getLimit();
    $query->limit($limit['offset'], $limit['limit']);

    // Fire alter event
    \Cherry::Event('ManifestQueryAlter')
      ->fire($query, [$this->getType()]);

    // Execute query
    if (!$result = $query->execute()) {
      return FALSE;
    }
    // Return Result if query was successful
    return $result;
  }

}