<?php

namespace Cherry\Search;

/**
 * Class Search
 *
 * @package Cherry\Search
 */
class Search {

  /** @var string $keyword */
  protected string $keyword;

  /** @var array $options */
  protected array $options;

  /**
   * Search constructor.
   *
   * @param string $keyword
   *          The keyword you are searching for.
   * @param array  $options
   *          Any options you wish to convey to the event listener.
   */
  public function __construct(string $keyword = '', array $options = []) {
    $this->setKeyword($keyword);
    $this->setOptions($options);
  }

  /**
   * Returns the set keyword
   *
   * @return string
   */
  public function getKeyword(): string {
    return $this->keyword;
  }

  /**
   * Sets the keyword, returns self
   *
   * @param string $keyword
   *
   * @return Search
   */
  public function setKeyword(string $keyword): self {
    $this->keyword = $keyword;
    return $this;
  }

  /**
   * Returns the set options
   *
   * @return array
   */
  public function getOptions(): array {
    return $this->options;
  }

  /**
   * Sets the options, returns self
   *
   * @param array $options
   *
   * @return Search
   */
  public function setOptions(array $options): self {
    $this->options = $options;
    return $this;
  }

  /**
   * Get search results from all entities
   *
   * @return array
   */
  public function getSearchResults(): array {
    $results = [];

    \Cherry::Event('addSearchResults')
      ->fire($results, [$this->getKeyword(), $this->getOptions()]);

    return $results;
  }

}