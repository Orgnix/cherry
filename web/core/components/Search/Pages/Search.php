<?php

namespace Cherry\Search\Pages;

use Cherry;
use Cherry\Cache\CacheBase;
use Cherry\Entity\EntityInterface;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;
use Cherry\Search\Search as SearchObject;

/**
 * Class Search
 *
 * @package Cherry\Page
 */
class Search extends Page {

  /**
   * Search constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'search',
      'title' => $this->translate('Search'),
      'summary' => $this->translate('Search results for :keyword', [':keyword' => 'undefined']),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['search items']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.search',
      'context' => 'search',
      'max-age' => CacheBase::getDefaultCacheTime(),
    ];

    if ($this->hasParameter('q')) {
      $this->caching['context'] .= ':query:' . $this->get('q');
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();
    $query = $this->hasParameter('q') ? $this->get('q') : '';
    $options = $this->hasParameter('options') ? unserialize($this->get('options')) : [];
    $search = new SearchObject();
    $results = $search
      ->setKeyword($query)
      ->setOptions($options)
      ->getSearchResults();

    foreach ($results as $category => &$items) {
      if (!is_array($items)) {
        continue;
      }

      foreach ($items as $key => &$item) {
        if (!$item instanceof EntityInterface) {
          continue;
        }
        $item = \Cherry::EntityRenderer($item)
          ->render([], 'search-result');
      }
    }

    return $this->getRenderer()
      ->setType('core.Search')
      ->setTemplate('search')
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'results' => $results,
      ]);
  }

}
