<?php

namespace Cherry\Search;

use Cherry;
use Cherry\Core;
use Cherry\Element\ElementInterface;
use Cherry\Entity\EntityInterface;
use Cherry\Event\EventListener;

/**
 * Class Events
 *
 * @package Cherry\Search
 */
class Events extends EventListener {

  /**
   * {@inheritDoc}
   */
  public function EntityPostSave(?EntityInterface $entity) {
    \Cherry::Cache()->invalidateTags(['page.search']);
  }

  /**
   * {@inheritDoc}
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element) {
    if ($element_id !== 'footer') {
      return;
    }

    $variables['js'][] = Core::getCoreComponentsPath() . '/Search/js/search.js';
  }

}
