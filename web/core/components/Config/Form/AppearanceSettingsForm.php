<?php

namespace Cherry\Config\Form;

use Cherry;
use Cherry\Entity\EntityInterface;
use Cherry\Form\Form;
use Cherry\Form\FormInterface;

/**
 * Class AppearanceSettingsForm
 *
 * @package Cherry\Config\Form
 */
class AppearanceSettingsForm extends Form implements FormInterface {

  /**
   * AppearanceSettingsForm constructor.
   *
   * @param EntityInterface|null $entity
   */
  public function __construct(?EntityInterface $entity = NULL) {
    parent::__construct($entity);
    $themes = \Cherry::Container()->get('theme')->getAvailableThemes();
    $options = [];
    foreach ($themes as $theme) {
      $options[$theme] = \Cherry::Container()->get('theme')->getThemeInfo($theme)['name'];
    }
    return $this->setId('appearance-settings-form')->setFields([
      'admin' => [
        'form' => [
          'type' => 'select',
          'title' => 'Admin theme',
          'default_value' => \Cherry::Container()->get('config')->get('theme.admin'),
          'options' => $options,
        ],
      ],
      'front' => [
        'form' => [
          'type' => 'select',
          'title' => 'Front theme',
          'default_value' => \Cherry::Container()->get('config')->get('theme.front'),
          'options' => $options,
        ],
      ],
      'submit' => [
        'form' => [
          'type' => 'button',
          'text' => $this->translate('Save settings'),
          'attributes' => [
            'type' => 'submit',
          ],
          'classes' => [
            'btn-success'
          ],
          'handler' => [$this, 'submitForm'],
        ],
      ],
    ]);
  }

  /**
   * @param array $form
   * @param array $values
   */
  public function submitForm(array $form = [], array $values = []) {
    unset($values['form-id']);
    try {
      \Cherry::Container()->get('config')->set('theme', $values);
    } catch (\Error $e) {
      die($e->getMessage());
    }

    \Cherry::Logger()->addSuccess('Successfully saved appearance settings.', 'appearance');
    $this->reload();
  }

}