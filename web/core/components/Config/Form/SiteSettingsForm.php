<?php

namespace Cherry\Config\Form;

use Cherry;
use Cherry\Entity\EntityInterface;
use Cherry\Form\Form;
use Cherry\Form\FormInterface;

/**
 * Class SiteSettingsForm
 *
 * @package Cherry\Config\Form
 */
class SiteSettingsForm extends Form implements FormInterface {

  /**
   * SiteSettingsForm constructor.
   *
   * @param EntityInterface|null $entity
   */
  public function __construct(?EntityInterface $entity = NULL) {
    parent::__construct($entity);
    $site_values = \Cherry::Container()->get('config')->get('site');
    $languages = \Cherry::Container()->get('language.manager')->getAvailableLanguages();
    $options = [];
    foreach ($languages as $langcode => $language) {
      $options[$langcode] = '[' . $langcode . '] ' . $language['language'] . ' - ' . $language['country'];
    }

    $fields = [
      'name' => [
        'form' => [
          'type' => 'textbox',
          'title' => $this->translate('Website name'),
          'default_value' => $site_values['name'] ?? 'Default website name',
          'attributes' => [
            'type' => 'text',
            'placeholder' => 'My Website',
          ],
        ],
      ],
      'default_langcode' => [
        'form' => [
          'type' => 'select',
          'title' => $this->translate('Default langcode'),
          'default_value' => $site_values['default_langcode'] ?? 'en',
          'options' => $options,
        ],
      ],
      'log_404' => [
        'form' => [
          'type' => 'checkbox',
          'title' => $this->translate('Log 404 errors'),
          'default_value' => $site_values['log_404'] ?? FALSE,
        ],
      ],
    ];

    $submit = [
      'submit' => [
        'form' => [
          'type' => 'button',
          'text' => $this->translate('Save settings'),
          'attributes' => [
            'type' => 'submit',
          ],
          'classes' => [
            'btn-success'
          ],
          'handler' => [$this, 'submitForm'],
        ],
      ],
    ];

    $fields = [
      'fields' => $fields,
      'submit' => $submit,
    ];

    // Submit handler gets added at the end so it doesn't get surpassed by other fields
    \Cherry::Event('siteSettingsFormPreBuild')
      ->fire($fields);

    $fields = $fields['fields'] + $fields['submit'];

    $this->setId('site-settings-form');
    $this->setFields($fields);
  }

  /**
   * @param array $form
   * @param array $values
   */
  public function submitForm(array $form = [], array $values = []) {
    try {
      unset($values['form-id']);
      if (isset($values['log_404'])) {
        $values['log_404'] = TRUE;
      } else {
        $values['log_404'] = FALSE;
      }

      // Roll over values, save them as the form element's key in site settings.
      foreach ($values as $key => $value) {
        \Cherry::Container()->get('config')->set('site.' . $key, $value);
      }
    } catch (\Error $e) {
      die($e->getMessage());
    }

    \Cherry::Logger()->addSuccess('Successfully saved site settings.', 'Site settings');
    $this->reload();
  }

}