<?php

namespace Cherry\Config\Form;

use Cherry;
use Cherry\Entity\EntityInterface;
use Cherry\Form\Form;
use Cherry\Form\FormInterface;
use Cherry\Url;

/**
 * Class SiteSettingsForm
 *
 * @package Cherry\Config\Form
 */
class ConfigForm extends Form implements FormInterface {

  /**
   * SiteSettingsForm constructor.
   *
   * @param EntityInterface|null $entity
   */
  public function __construct(?EntityInterface $entity = NULL) {
    parent::__construct($entity);
    $this->setId('config-settings-form');
    $this->setFields([
      'import' => [
        'form' => [
          'type' => 'button',
          'text' => 'Import',
          'attributes' => [
            'onclick' => 'window.location.href = \'' . Url::fromRoute(\Cherry::Container()->get('route')->load('config.import')) . '\';',
          ],
          'classes' => ['btn-success'],
        ],
      ],
      'export' => [
        'form' => [
          'type' => 'button',
          'text' => 'Export',
          'attributes' => [
            'onclick' => 'window.location.href = \'' . Url::fromRoute(\Cherry::Container()->get('route')->load('config.export')) . '\';',
          ],
          'classes' => ['btn-success'],
        ],
      ],
      'difference' => [
        'form' => [
          'type' => 'button',
          'text' => 'Difference',
          'attributes' => [
            'onclick' => 'window.location.href = \'' . Url::fromRoute(\Cherry::Container()->get('route')->load('config.import')) . '\';',
          ],
          'classes' => ['btn-success'],
        ],
      ],
    ]);
  }

}