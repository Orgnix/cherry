<?php

namespace Cherry\Config;

use Exception;
use FilesystemIterator;
use Cherry\Database\Result;
use Cherry\Settings;
use Cherry\Utils\Strings\StringManipulation;
use Cherry\Translation\StringTranslation;
use Cherry\YamlReader;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class Config
 *
 * @package Cherry
 */
class Config {
  use StringTranslation;

  /** @var array $config */
  protected static array $config = [];

  /**
   * Config constructor.
   */
  public function __construct() {
    if (count(static::$config) === 0) {
      $config_storage = \Cherry::Database()
        ->select('config')
        ->execute();
      if (!$config_storage instanceof Result) {
        static::$config = [];
      } else {
        $conf = $config_storage->fetchAllAssoc();
        foreach ($conf as $key => $item) {
          static::$config[$item['field']] = unserialize($item['value']);
        }
      }

      static::$config['extensions'] = serialize(\Cherry::Container()->get('extension.manager')->getInstalledExtensions());
    }
  }

  /**
   * Imports config from config folder.
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function import(): bool {
    if (!Cherry::Database()->query('TRUNCATE TABLE config')) {
      \Cherry::Logger()->addFailure('Something went wrong trying to truncate the config table.', 'Config');
      return FALSE;
    }

    $staged = $this->getStagedConfig();
    foreach ($staged as $key => $value) {
      if ($key === 'extensions') {
        foreach ($value as $extension) {
          \Cherry::Container()->get('extension.manager')->installExtension($extension['name'], $extension['type']);
        }
      } else {
        if (!$this->set($key, $value)) {
          \Cherry::Logger()->addFailure(
            $this->translate('Something went wrong trying to import the following config: :key', [':key' => $key]), 'Config');
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  /**
   * Retrieves config from config folder.
   *
   * @param string|NULL $name
   *
   * @return array
   */
  public function getStagedConfig(?string $name = NULL): array {
    $config = [];
    if ($name !== NULL) {
      $config[] = YamlReader::fromYamlFile(Settings::get('config.folder') . '/' . $name . '.yml');
    } else {
      $folder = Settings::get('config.folder');
      if (!is_dir($folder)) {
        \Cherry::Logger()->addError($this->translate('Config directory does not exist. Please create this folder, and give it write rights.'), 'Config');
        return $config;
      }
      $files = scandir($folder);
      foreach ($files as $file) {
        if ($file == '.' || $file == '..') {
          continue;
        }

        if (!is_file($folder . '/' . $file)) {
          continue;
        }

        // Add to config array.
        $key = str_replace('.yml', '', $file);
        $config[$key] = YamlReader::fromYamlFile($folder . '/' . $file);
      }
    }
    return $config;
  }

  /**
   * Exports config from database to config folder.
   *
   * @return bool
   */
  public function export(): bool {
    $config_folder = Settings::get('config.folder');
    if (!is_dir(Settings::get('config.folder'))) {
      \Cherry::Logger()->addError($this->translate('Config directory does not exist. Please create this folder, and give it writing rights.'), 'Config');
    }
    $di = new RecursiveDirectoryIterator($config_folder, FilesystemIterator::SKIP_DOTS);
    $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
    foreach ($ri as $file) {
      if ($file->isDir()) {
        continue;
      }
      unlink($file);
    }

    foreach ($this->getConfig() as $item) {
      $config = YamlReader::toYaml(unserialize($item['value']));
      try {
        $config_file = fopen(Settings::get('config.folder') . '/' . $item['field'] . '.yml', 'w');
        fwrite($config_file, $config);
        fclose($config_file);
      } catch (Exception $e) {
        \Cherry::Logger()->addError(
          $this->translate('Something went wrong trying to write config to file. [:field.yml]', [':field' => $item['field']]),
          'Config'
        );
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Retrieves config from database.
   *
   * @return array
   *
   * @throws \Exception
   */
  public function getConfig() {
    $config_storage = \Cherry::Database()
      ->select('config')
      ->execute();
    if (!$config_storage instanceof Result) {
      $returnArray = [];
    } else {
      $returnArray = $config_storage->fetchAllAssoc();
    }

    $returnArray[] = ['field' => 'extensions', 'value' => serialize(\Cherry::Container()->get('extension.manager')->getInstalledExtensions())];

    return $returnArray;
  }

  /**
   * Shows difference in config.
   *
   * @return array[]
   *
   * @throws \Exception
   */
  public function difference(): array {
    return [
      'live' => $this->getConfig(),
      'staged' => $this->getStagedConfig(),
    ];
  }

  /**
   * Retrieves certain config from the database.
   *
   * @param $key
   *
   * @return string|array
   *
   * @throws \Exception
   */
  public function get($key) {
    $old_key = $key;
    if (StringManipulation::contains($key, '.')) {
      $items = StringManipulation::explode($key, '.');
      $key = $items[0];
      unset($items[0]);
    }

    if (isset(static::$config[$key])) {
      $config = static::$config[$key];
    } else {
      $config_storage = \Cherry::Database()
        ->select('config')
        ->fields(NULL, ['value'])
        ->condition('field', $key);
      /** @var Result $config_result */
      if (!$config_result = $config_storage->execute()) {
        return FALSE;
      }
      $result = $config_result->fetchAllAssoc();
      if (!$result) {
        return FALSE;
      }
      $result = reset($result);
      $config = unserialize($result['value']);
    }

    if (isset($items)) {
      foreach ($items as $item) {
        if (!isset($config[$item])) {
          return FALSE;
        }
        $config = $config[$item];
      }
    }

    if (Settings::has($old_key)) {
      if (is_array($config) && !empty($config)) {
        return array_merge($config, Settings::get($old_key));
      } else {
        return Settings::get($old_key);
      }
    }

    return $config;
  }

  /**
   * Sets a certain config key to a certain value.
   *
   * @param string $key
   * @param array|string $value
   *
   * @return bool|Result
   *
   * @throws \Exception
   */
  public function set(string $key, $value) {
    if (strpos($key, '.') !== FALSE) {
      $items = StringManipulation::explode($key, '.');
      $key = $items[0];
      $item = $items[1];
    }

    $config_storage = \Cherry::Database()
      ->select('config')
      ->fields(NULL, ['value'])
      ->condition('field', $key)
      ->execute();
    if (!$config_storage instanceof Result) {
      return FALSE;
    }
    $result = $config_storage->fetchAllAssoc();
    if (count($result) > 0) {
      $result = reset($result);
      $result = unserialize($result['value']);
      if (isset($item)) {
        $result[$item] = $value;
        $value = $result;
      }

      $value = serialize($value);
      $config_query = \Cherry::Database()
        ->update('config')
        ->values(['value' => $value])
        ->condition('field', $key)
        ->execute();
    } else {
      if (isset($item)) {
        $result = [$item => $value];
        $value = $result;
      }

      $value = serialize($value);
      $config_query = \Cherry::Database()
        ->insert('config')
        ->values([
          'field' => $key,
          'value' => $value,
          ])
        ->execute();
    }
    return $config_query;
  }

}