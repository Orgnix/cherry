<?php

namespace Cherry\Config\Pages;

use Cherry;
use Cherry\Config\Form\ConfigForm;
use Cherry\Form\Form;
use Cherry\Form\FormInterface;
use Cherry\Language\Language;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Config
 *
 * @package Cherry\Page
 */
class Config extends Page {

  /** @var Form $form */
  protected Form $form;

  /** @var Language $language */
  protected Language $language;

  /**
   * Config constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->form = new Form();
    $this->language = \Cherry::Container()->get('language');
    $this->setParameters([
      'id' => 'config',
      'title' => $this->translate('Config'),
      'summary' => $this->translate('Configuration options'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['manage config']);
  }

  /**
   * @return FormInterface
   */
  protected function configForm() {
    return new ConfigForm();
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.' . $this->get('id'),
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();
    kint($this);
    if ($this->get(1) == 'export' && ($this->get(2) == 'confirm' || $this->get(2) == 'confirm=')) {
      \Cherry::Container()->get('config')->export();
    } elseif ($this->get(1) == 'import' && ($this->get(2) == 'confirm' || $this->get(2) == 'confirm=')) {
      \Cherry::Container()->get('config')->import();
    } elseif ($this->get(1) == 'difference' && ($this->get(2) == 'confirm' || $this->get(2) == 'confirm=')) {
      // TODO
    } else {
      $form = $this->configForm()->result();

      return $this->getRenderer()
        ->setType()
        ->setTemplate('config')
        ->render([
          'title' => isset($parameters['t']) ? ucfirst($parameters['t']) . ' settings' : 'Settings',
          'form' => $form,
        ]);
    }

    return NULL;
  }

}