<?php

namespace Cherry\Config\Pages;

use Cherry;
use Cherry\Config\Form\AppearanceSettingsForm;
use Cherry\Form\FormInterface;
use Cherry\Page\Page;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;

/**
 * Class Appearance
 *
 * @package Cherry\Config\Pages
 */
class Appearance extends Page {

  /**
   * Config constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'config.appearance',
      'title' => $this->translate('Appearance settings'),
      'summary' => $this->translate('Configuration options'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['manage appearance form']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.config.appearance',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * @return FormInterface
   */
  protected function appearanceForm() {
    return new AppearanceSettingsForm();
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();
    $form = $this->appearanceForm()->result();

    return $this->getRenderer()
      ->setType()
      ->setTemplate('config')
      ->render([
        'title' => $this->translate('Appearance settings'),
        'form' => $form,
      ]);
  }

}