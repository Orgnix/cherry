<?php

namespace Cherry\Config\Pages;

use Cherry;
use Cherry\Config\Form\SiteSettingsForm;
use Cherry\Form\FormInterface;
use Cherry\Page\Page;
use Cherry\Renderer;
use Cherry\Route\RouteInterface;

/**
 * Class Site
 *
 * @package Cherry\Config\Pages
 */
class Site extends Page {

  /**
   * Config constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, Renderer $renderer) {
    $this->setParameters([
      'id' => 'config.site',
      'title' => $this->translate('Site settings'),
      'summary' => $this->translate('Configuration options'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['manage site settings form']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.config.site',
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * @return FormInterface
   */
  protected function siteForm() {
    return new SiteSettingsForm();
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();
    $form = $this->siteForm()->result();

    return $this->getRenderer()
      ->setType()
      ->setTemplate('config')
      ->render([
        'title' => $this->translate('Site settings'),
        'form' => $form,
      ]);
  }

}