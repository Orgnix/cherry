<?php

namespace Cherry\Database;

use InvalidArgumentException;
use PDO;
use PDOStatement;

/**
 * Class Result
 *
 * @package Cherry\Database
 */
class Result {

  /** @var PDOStatement $result */
  protected PDOStatement $result;

  /**
   * Result constructor.
   *
   * @param PDOStatement $result
   */
  public function __construct(PDOStatement $result) {
    $this->result = $result;
  }

  /**
   * @return PDOStatement
   */
  public function getResult(): PDOStatement {
    return $this->result;
  }

  /**
   * fetchAllAssoc method
   *
   * @param string|null $key
   *
   * @return array
   */
  public function fetchAllAssoc(?string $key = NULL): array {
    $records = [];
    foreach ($this->fetchAll(PDO::FETCH_ASSOC) as $record) {
      if ($key !== NULL) {
        if (!isset($record[$key])) {
          throw new InvalidArgumentException("The key you've chosen should be in the select fields array.");
        }
        $records[$record[$key]] = $record;
      } else {
        $records[] = $record;
      }
    }

    return $records;
  }

  /**
   * fetchAll method
   *
   * @param int $mode
   *   The PDO mode to fetch results.
   *
   * @return array
   */
  public function fetchAll(int $mode = PDO::FETCH_BOTH): array {
    return $this->getResult()->fetchAll($mode);
  }

  /**
   * fetch method
   *
   * @return array
   */
  public function fetch(): array {
    return $this->getResult()->fetch();
  }

  /**
   * Returns the number of rows in this result.
   *
   * @return int
   */
  public function getCount(): int {
    return $this->getResult()->rowCount();
  }

}
