<?php

namespace Cherry\Database;

use Cherry;
use Cherry\Settings;
use Cherry\Utils\Sql\SqlFormatter;
use Cherry\Utils\Strings\StringManipulation;
use PDO;
use PDOStatement;

/**
 * Class Database
 *
 * @package Cherry\Database
 */
class Database {

  /** @var string $db */
  protected $db;

  /** @var PDO $database */
  protected $database;

  /** @var mixed $result */
  protected $result;

  /** @var array $query */
  protected $query;

  /** @var array $tables */
  protected $tables;

  /** @var array $fields */
  protected $fields;

  /** @var array $values */
  protected array $values;

  /** @var array $joins */
  protected array $joins;

  /** @var array $orderby */
  protected array $orderby;

  /** @var array $limit */
  protected array $limit = [];

  /** @var array $conditions */
  protected array $conditions = [];

  /** @var string $condition_delimiter */
  protected string $condition_delimiter = 'AND';

  /**
   * Database constructor
   *
   * @param string|null $database
   */
  public function __construct(?string $database = NULL) {
    $this->setDatabaseName($database ?? Settings::get('database.database'));
    $this->connect();
    $this->reset();
  }

  /**
   * connect function
   *     Will use caching if no custom database name was given,
   *     if a custom database was given then it will initiate
   *     a new PDO instance.
   *
   * @return self
   */
  protected function connect(): self {
    try {
      $this->database = new \PDO(
        Settings::get('database.type', 'mysql') . ':host=' . Settings::get('database.hostname')
        . ';dbname=' . $this->getDatabaseName()
        . ';port=' . Settings::get('database.port', 3306)
        . ';charset=' . Settings::get('database.charset', 'utf8mb4'),
        Settings::get('database.username'),
        Settings::get('database.password'),
        [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false,
        ],
      );
    } catch (\PDOException $e) {
      throw new \PDOException($e->getMessage(), (int) $e->getCode());
    }
    return $this;
  }

  /**
   * @return string
   */
  public function getDelimiter(): string {
    return $this->condition_delimiter;
  }

  /**
   * @param string $delimiter
   *
   * @return self
   */
  public function setDelimiter(string $delimiter): self {
    $this->condition_delimiter = $delimiter;

    return $this;
  }

  /**
   * @return string|null
   */
  public function getDatabaseName(): ?string {
    return $this->db;
  }

  /**
   * @param string $database
   *
   * @return self
   */
  public function setDatabaseName(string $database): self {
    $this->db = $database;

    return $this;
  }

  /**
   * @return array
   */
  public static function getFieldTypes(): array {
    return [
      'INT', 'TINYINT', 'SMALLINT', 'MEDIUMINT', 'BIGINT', 'DECIMAL', 'FLOAT', 'DOUBLE', 'BIT',
      'CHAR', 'VARCHAR', 'BINARY', 'VARBINARY', 'TINYBLOB', 'BLOB', 'MEDIUMBLOB', 'LONGBLOB', 'TINYTEXT', 'TEXT', 'MEDIUMTEXT', 'LONGTEXT', 'ENUM', 'SET',
      'DATE', 'TIME', 'DATETIME', 'TIMESTAMP', 'YEAR',
      'GEOMETRY', 'POINT', 'LINESTRING', 'POLYGON', 'GEOMETRYCOLLECTION', 'MULTILINESTRING', 'MULTIPOINT', 'MULTIPOLYGON',
    ];
  }

  /**
   * @param string $table
   * @param null $alias
   * @param null $options
   *
   * @return Exists
   */
  public function exists(string $table, $alias = NULL, $options = NULL): Exists {
    return new Exists($table, $alias, $options);
  }

  /**
   * @param $table
   * @param null $alias
   * @param null $options
   *
   * @return CreateOrUpdate
   */
  public function createOrUpdate($table, $alias = NULL, $options = NULL): CreateOrUpdate {
    return new CreateOrUpdate($table, $alias, $options);
  }

  /**
   * @param string $table
   * @param null   $alias
   * @param null   $options
   *
   * @return Select
   */
  public function select(string $table, $alias = NULL, $options = NULL): Select {
    return new Select($table, $alias, $options, $this->getDelimiter(), $this->getDatabaseName());
  }

  /**
   * @param string $table
   * @param null   $alias
   * @param null   $options
   *
   * @return Update
   */
  public function update(string $table, $alias = NULL, $options = NULL): Update {
    return new Update($table, $alias, $options);
  }

  /**
   * @param string $table
   * @param null   $alias
   * @param null   $options
   *
   * @return Insert
   */
  public function insert(string $table, $alias = NULL, $options = NULL): Insert {
    return new Insert($table, $alias, $options);
  }

  /**
   * @param string $table
   * @param null   $alias
   * @param null   $options
   *
   * @return Delete
   */
  public function delete(string $table, $alias = NULL, $options = NULL): Delete {
    return new Delete($table, $alias, $options);
  }

  /**
   * @param string $query
   *
   * @return Query
   */
  public function query(string $query): Query {
    return new Query($query);
  }

  /**
   * Adds a new, or modifies an existing, field in a given table with given options.
   *
   * @param string $table
   * @param string $field
   * @param array  $options
   *
   * @return Cherry\Database\Query
   */
  public function field(string $table, string $field, array $options): Query {
    $result = \Cherry::Database()->query("SHOW COLUMNS FROM `" . $table . "` LIKE '%" . $field . "%'");
    $exists = $result->count() > 0;

    if ($exists) {
      // If it exists, modify column
      $query = \Cherry::Database()->query('ALTER TABLE ' . $table
        . ' MODIFY COLUMN ' . \Cherry::Database()::createFieldQuery($field, $options));
    } else {
      // If it doesn't exist, add column
      $query = \Cherry::Database()->query('ALTER TABLE ' . $table
        . ' ADD ' . \Cherry::Database()::createFieldQuery($field, $options));
    }

    return $query;
  }

  /**
   * Removes a given field from given table.
   *
   * @param string $table
   * @param string $field
   *
   * @return Query
   */
  public function removeField(string $table, string $field): Query {
    return \Cherry::Database()->query('ALTER TABLE ' . $table
      . ' DROP COLUMN' . $field);
  }

  /**
   * @param string $field_name
   * @param array  $options
   *
   * @return string
   */
  public static function createFieldQuery(string $field_name, array $options): string {
    if (isset($options['type'])
      && (StringManipulation::contains($options['type'], 'text')
      || StringManipulation::contains($options['type'], 'blob'))
      && !isset($options['length'])) {
      $field = '`' . $field_name . '` ' . $options['type'] . ' ';
    } else {
      $field = '`' . $field_name . '` ' . $options['type'] . '(' . ($options['length'] ?? 255) . ') ';
    }
    $field .= ($options['null'] ?? 'NOT NULL') . ' ';
    $field .= isset($options['unique']) && $options['unique'] == TRUE ? 'UNIQUE ' : '';
    $field .= (isset($options['default_value']) ? 'DEFAULT \'' . $options['default_value'] . '\'' : '') . ' ';

    return trim($field);
  }

  /**
   * @param PDOStatement $query
   * @param array        $values
   *
   * @return Result|bool
   */
  protected function executeQuery(PDOStatement $query, array $values = []) {
    if (Settings::get('debugging')) {
      echo '<pre style="color:#fff;">';
      echo SqlFormatter::format($query->queryString);
      echo '</pre>';
    }

    if (isset(\Cherry::$queries[$query->queryString])) {
      \Cherry::$queries[$query->queryString]++;
    } else {
      \Cherry::$queries[$query->queryString] = 1;
    }
    try {
      if (Settings::get('debugging')) {
        d([$query->queryString => $values]);
      }
      if (count($values) > 0) {
        $result = $query->execute($values);
      } else {
        $result = $query->execute();
      }
    } catch (\PDOException $e) {
      if (Settings::get('debugging')) {
        kint($e->getMessage());
        $this->reset();
        return FALSE;
      }
    }
    if (isset($result) && $result !== FALSE) {
      $this->result = $result;

      $this->reset();
      return new Result($query);
    }

    $this->reset();
    return FALSE;
  }

  /**
   * Resets \Cherry\Database object.
   *
   * @return self
   */
  protected function reset(): self {
    $this->tables = [];
    $this->fields = [];
    $this->joins = [];
    $this->values = [];
    $this->orderby = [];
    $this->conditions = [];
    $this->limit = [];
    $this->query = [
      'useQuery' => FALSE,
      'query' => [],
    ];
    $this->joins = [
      'isJoined' => FALSE,
      'join' => [],
    ];

    return $this;
  }

}
