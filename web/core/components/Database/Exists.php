<?php

namespace Cherry\Database;

/**
 * Class Exists
 *
 * @package Cherry\Database
 */
class Exists extends Select {

  /**
   * Builds and executes the query.
   *
   * @return bool
   */
  public function execute() {
    $result = parent::execute();
    return $result->getCount() > 0;
  }

}