<?php

namespace Cherry\Database;

/**
 * Class CreateOrUpdate
 *
 * @package Cherry\Database
 */
class CreateOrUpdate extends Update {

  /**
   * Builds and executes the query.
   *
   * @return Result|bool
   */
  public function execute() {
    $exists = $this->exists($this->tables[0]['table']);
    foreach ($this->conditions as $condition) {
      $exists->condition($condition['field'], $condition['value'], $condition['operator']);
    }
    $result = $exists->execute();

    // Record exists, update existing record
    if ($result) {
      $query = $this->update($this->tables[0]['table'])
        ->values($this->values);
      foreach ($this->conditions as $condition) {
        $query->condition($condition['field'], $condition['value'], $condition['operator']);
      }
    }
    // Record does not exist, insert new one
    else {
      $query = $this->insert($this->tables[0]['table'])
        ->values($this->values);
    }
    return $query->execute();
  }

}
