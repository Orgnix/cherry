<?php

namespace Cherry\Database;

use Cherry\Settings;
use PDOStatement;

/**
 * Class Query
 *
 * @package Cherry\Database
 */
class Query extends Database {

  /**
   * Query constructor.
   *
   * @param $query
   */
  public function __construct($query, $database = NULL) {
    parent::__construct($database);
    $this->execute($query);
  }

  /**
   * Returns the amount of rows
   *
   * @return int
   */
  public function count(): int {
    return $this->result->rowCount();
  }

  /**
   * Executes the query.
   *
   * @param $query
   *
   * @return Query
   */
  protected function execute($query) {
    if (Settings::get('debugging')) {
      kint($query);
    }
    if ($result = $this->database->query($query)) {
      $this->query = [
        'useQuery' => TRUE,
        'query' => $result,
      ];
      $this->result = $result;
    }

    return $this;
  }

}