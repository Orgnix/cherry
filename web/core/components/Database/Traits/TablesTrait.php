<?php

namespace Cherry\Database\Traits;

/**
 * Trait TablesTrait.
 *
 * @package Cherry\Database\Traits
 */
trait TablesTrait {

  /** @var array $tables */
  protected $tables;

  /**
   * @param      $table
   * @param null $alias
   * @param      $options
   */
  public function addTable($table, $alias = NULL, $options = NULL) {
    $this->tables[] = ['table' => $table, 'alias' => $alias, 'options' => $options];
  }

  /**
   * @return string
   */
  protected function getTables(): string {
    $tables = '';
    foreach ($this->tables as $table) {
      if ($tables !== '') {
        $tables .= ', ';
      }
      $tables .= $table['table'];
      if ($table['alias'] !== NULL) {
        $tables .= ' ' . $table['alias'];
      }
    }
    return $tables;
  }

}
