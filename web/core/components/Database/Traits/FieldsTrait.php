<?php

namespace Cherry\Database\Traits;

/**
 * Trait FieldsTrait.
 *
 * @package Cherry\Database\Traits
 */
trait FieldsTrait {

  /** @var array $fields */
  protected $fields;

  /**
   * fields method.
   *
   * @param mixed $table_alias
   * @param array $fields
   *
   * @return self
   */
  public function fields($table_alias = NULL, array $fields = []): self {
    if (is_array($table_alias) && count($fields) === 0) {
      $fields = $table_alias;
      $table_alias = NULL;
    }
    $this->fields[] = ['table_alias' => $table_alias, 'fields' => $fields];
    return $this;
  }

  /**
   * Returns fields.
   *
   * @return array
   */
  protected function getFields() {
    return $this->fields;
  }

}
