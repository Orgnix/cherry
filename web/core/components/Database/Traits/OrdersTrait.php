<?php

namespace Cherry\Database\Traits;

/**
 * Trait OrdersTrait.
 *
 * @package Cherry\Database\Traits
 */
trait OrdersTrait {

  /** @var array $orderby */
  protected array $orderby;

  /**
   * orderBy method
   *
   * @param string $field
   * @param string $direction
   *
   * @return self
   */
  public function orderBy(string $field, string $direction): self {
    $this->orderby[] = ['field' => $field, 'direction' => $direction];
    return $this;
  }

  /**
   * Returns the sorts.
   *
   * @return string
   */
  protected function getOrderBy(): string {
    $orderby = '';
    foreach ($this->orderby as $order) {
      if ($orderby !== '') {
        $orderby .= ', ';
      }
      $orderby .= $order['field'] . ' ' . $order['direction'];
    }
    if ($orderby !== '') {
      $orderby = ' ORDER BY ' . $orderby;
    }
    return $orderby;
  }

}
