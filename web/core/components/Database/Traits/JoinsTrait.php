<?php

namespace Cherry\Database\Traits;

/**
 * Trait JoinsTrait.
 *
 * @package Cherry\Database\Traits
 */
trait JoinsTrait {

  /** @var array $joins */
  protected array $joins;

  /**
   * @return string
   */
  protected function getJoins(): string {
    $joins = '';
    if (!$this->joins['isJoined']) {
      return $joins;
    }

    foreach ($this->joins['join'] as $join) {
      $joins .= ' ' . $join['type'] . ' JOIN ' . $join['table'] . ' ' . $join['alias'] . ' ON ' . $join['field_a'] . ' = ' . $join['field_b'];
    }

    return $joins;
  }

  /**
   * Create a joined query
   *
   * @param string $table
   * @param string $alias
   * @param string $field_a
   * @param string $field_b
   * @param string $type
   *
   * @return self
   */
  public function join(string $table, string $alias, string $field_a, string $field_b, string $type = 'LEFT'): self {
    $this->joins['isJoined'] = TRUE;
    $this->joins['join'][] = [
      'type' => $type,
      'table' => $table,
      'alias' => $alias,
      'field_a' => $field_a,
      'field_b' => $field_b,
    ];

    return $this;
  }

}
