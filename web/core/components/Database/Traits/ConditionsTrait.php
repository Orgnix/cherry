<?php

namespace Cherry\Database\Traits;

/**
 * Trait ConditionsTrait.
 *
 * @package Cherry\Database\Traits
 */
trait ConditionsTrait {

  /** @var array $conditions */
  protected array $conditions = [];

  /** @var string $condition_delimiter */
  protected string $condition_delimiter = 'AND';

  /**
   * @return string
   */
  protected function getConditions(): string {
    $conditions = '';
    foreach ($this->conditions as $key => $condition) {
      if ($conditions !== '') {
        $conditions .= ' ' . $this->condition_delimiter . ' ';
      }
      $conditions .= $condition['field'] . ' ' . $condition['operator'] . ' :condition_placeholder_' . $key;
    }
    if ($conditions !== '') {
      $conditions = ' WHERE ' . $conditions;
    }
    return $conditions;
  }

  /**
   * condition method
   *
   * @param string      $field
   * @param null|string $value
   * @param string      $operator
   *
   * @return self
   */
  public function condition(string $field, ?string $value = NULL, string $operator = '='): self {
    $this->conditions[] = [
      'field' => $field,
      'operator' => $operator,
      'value' => $value
    ];
    return $this;
  }

}
