<?php

namespace Cherry\Database\Traits;

use Cherry\Entity\Entity;

/**
 * Trait LimitsTrait.
 *
 * @package Cherry\Database\Traits
 */
trait LimitsTrait {

  /** @var array $limit */
  protected array $limit = [];

  /**
   * limit method
   *
   * @param int $offset
   * @param int $limit
   *
   * @return self
   */
  public function limit(int $offset = 0, int $limit = Entity::CARDINALITY_DEFAULT): self {
    $this->limit = [
      'offset' => $offset,
      'limit' => $limit,
    ];
    return $this;
  }

  /**
   * @return string
   */
  protected function getLimit(): string {
    $limit = '';
    if (isset($this->limit['offset']) && isset($this->limit['limit'])) {
      $limit = ' LIMIT :limit OFFSET :offset';
    }
    return $limit;
  }

}
