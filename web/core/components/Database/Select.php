<?php

namespace Cherry\Database;

use Cherry\Database\Traits\ConditionsTrait;
use Cherry\Database\Traits\FieldsTrait;
use Cherry\Database\Traits\JoinsTrait;
use Cherry\Database\Traits\LimitsTrait;
use Cherry\Database\Traits\OrdersTrait;
use Cherry\Database\Traits\TablesTrait;
use Cherry\Utils\Strings\StringManipulation;

/**
 * Class Select
 *
 * @package Cherry\Database
 */
class Select extends Database {
  use ConditionsTrait;
  use FieldsTrait;
  use JoinsTrait;
  use LimitsTrait;
  use OrdersTrait;
  use TablesTrait;

  /**
   * Select constructor.
   *
   * @param string      $table
   * @param string|null $alias
   * @param null        $options
   * @param string      $condition_delimiter
   * @param string|null $database
   */
  public function __construct(string $table, string $alias = NULL, $options = NULL, string $condition_delimiter = 'AND', string $database = NULL) {
    parent::__construct($database);
    $this->setDelimiter($condition_delimiter);
    $this->addTable($table, $alias, $options);
  }

  /**
   * Allows someone to debug the query.
   *
   * @return array
   */
  public function debugQuery() {
    $tables = $this->getTables();
    $fields = '';
    foreach ($this->getFields() as $field_array) {
      foreach ($field_array['fields'] as $field) {
        if ($fields !== '') {
          $fields .= ', ';
        }
        if (!is_null($field_array['table_alias'])) {
          $fields .= $field_array['table_alias'] . '.' . $field;
        } else {
          $fields .= $field;
        }
      }
    }
    $joins = $this->getJoins();
    $conditions = $this->getConditions();
    $orderby = $this->getOrderBy();
    if ($fields === '') {
      $fields = '*';
    }
    $limit = $this->getLimit();

    $values = [];
    foreach ($this->conditions as $key => $condition) {
      $values['condition_placeholder_' . $key] = $condition['value'];
    }
    if (isset($this->limit['offset']) && isset($this->limit['limit'])) {
      $values['limit'] = $this->limit['limit'];
      $values['offset'] = $this->limit['offset'];
    }
    return [
      'query' => 'SELECT ' . $fields . ' FROM ' . $tables . $joins . $conditions . $orderby . $limit,
      'values' => $values,
    ];
  }

  /**
   * Builds and executes the query.
   *
   * @return Result|bool
   */
  public function execute() {
    $tables = $this->getTables();
    $fields = '';
    foreach ($this->getFields() as $field_array) {
      foreach ($field_array['fields'] as $field) {
        if ($fields !== '') {
          $fields .= ', ';
        }
        if (!is_null($field_array['table_alias'])) {
          $fields .= $field_array['table_alias'] . '.' . $field;
        } else {
          $fields .= $field;
        }
      }
    }
    $joins = $this->getJoins();
    $conditions = $this->getConditions();
    $orderby = $this->getOrderBy();
    if ($fields === '') {
      $fields = '*';
    }
    $limit = $this->getLimit();
    $prep = $this->database->prepare('SELECT ' . $fields . ' FROM ' . $tables . $joins . $conditions . $orderby . $limit);

    $values = [];
    foreach ($this->conditions as $key => $condition) {
      $values['condition_placeholder_' . $key] = $condition['value'];
    }
    if (isset($this->limit['offset']) && isset($this->limit['limit'])) {
      $values['limit'] = $this->limit['limit'];
      $values['offset'] = $this->limit['offset'];
    }

    return $this->executeQuery($prep, $values);
  }

}