<?php

namespace Cherry\Database;

use Cherry\Database\Traits\ConditionsTrait;
use Cherry\Database\Traits\LimitsTrait;
use Cherry\Database\Traits\TablesTrait;
use Cherry\Utils\Strings\StringManipulation;

/**
 * Class Delete
 *
 * @package Cherry\Database
 */
class Delete extends Database {
  use ConditionsTrait;
  use TablesTrait;
  use LimitsTrait;

  /**
   * Delete constructor.
   *
   * @param      $table
   * @param null $alias
   * @param null $options
   */
  public function __construct($table, $alias = NULL, $options = NULL) {
    parent::__construct($this->getDatabaseName());
    $this->addTable($table, $alias, $options);
  }

  /**
   * Builds and executes the query.
   *
   * @return Result|bool
   */
  public function execute() {
    $tables = $this->getTables();
    $conditions = $this->getConditions();
    $limit = $this->getLimit();
    $prep = $this->database->prepare('DELETE FROM ' . $tables . $conditions . $limit);

    $values = [];
    foreach ($this->conditions as $key => $condition) {
      $values['condition_placeholder_' . $key] = $condition['value'];
    }

    return $this->executeQuery($prep, $values);
  }

}