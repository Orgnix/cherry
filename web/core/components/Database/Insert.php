<?php

namespace Cherry\Database;

use Cherry\Database\Traits\TablesTrait;

/**
 * Class Insert
 *
 * @package Cherry\Database
 */
class Insert extends Database {
  use TablesTrait;

  /**
   * Update constructor.
   *
   * @param string      $table
   * @param string|null $alias
   * @param array|null  $options
   */
  public function __construct(string $table, ?string $alias = NULL, ?array $options = NULL) {
    parent::__construct($this->getDatabaseName());
    $this->addTable($table, $alias, $options);
  }

  /**
   * values method.
   *
   * @param array $data
   *
   * @return self
   */
  public function values(array $data = []): self {
    $this->values = $data;

    return $this;
  }

  /**
   * Builds and executes the query.
   *
   * @return Result|bool
   */
  public function execute() {
    $fields_array = [];
    $fields = '';
    $iterator = 0;
    foreach ($this->values as $field => $value) {
      if ($fields !== '') {
        $fields .= ', ';
      }
      $fields .= ':value_placeholder_' . $iterator;

      $fields_array[] = $field;

      $this->values['value_placeholder_' . $iterator] = $value;
      unset($this->values[$field]);
      $iterator++;
    }
    $prep = $this->database->prepare('INSERT INTO ' . $this->tables[0]['table'] . ' (' . implode(', ', $fields_array) . ') VALUES (' . $fields . ')');

    return $this->executeQuery($prep, $this->values);
  }

}