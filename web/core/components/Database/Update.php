<?php

namespace Cherry\Database;

use Cherry\Database\Traits\ConditionsTrait;
use Cherry\Database\Traits\TablesTrait;
use Cherry\Utils\Strings\StringManipulation;

/**
 * Class Update
 *
 * @package Cherry\Database
 */
class Update extends Database {
  use ConditionsTrait;
  use TablesTrait;

  /**
   * Update constructor.
   *
   * @param string      $table
   * @param string|null $alias
   * @param array|null  $options
   */
  public function __construct(string $table, ?string $alias = NULL, ?array $options = NULL) {
    parent::__construct($this->getDatabaseName());
    $this->addTable($table, $alias, $options);
  }

  /**
   * values method.
   *
   * @param array $data
   *
   * @return self
   */
  public function values(array $data = []): self {
    $this->values = $data;

    return $this;
  }

  /**
   * Allows someone to debug the query.
   *
   * @return array
   */
  public function debugQuery() {
    $conditions = [];
    foreach ($this->conditions as $key => $condition) {
      $conditions['condition_placeholder_' . $key] = $condition['value'];
    }
    $iterator = 0;
    $values = [];
    foreach ($this->values as $field => $value) {
      $values['value_placeholder_' . $iterator] = $value;
      $iterator++;
    }
    $values = array_merge($values, $conditions);
    return [
      'query' => 'UPDATE ' . $this->tables[0]['table'] . ' SET ' . $this->getValues() . $this->getConditions(),
      'values' => $values,
    ];
  }

  /**
   * Builds and executes the query.
   *
   * @return Result|bool
   */
  public function execute() {
    $conditions = [];
    foreach ($this->conditions as $key => $condition) {
      $conditions['condition_placeholder_' . $key] = $condition['value'];
    }
    $iterator = 0;
    $values = [];
    foreach ($this->values as $field => $value) {
      $values['value_placeholder_' . $iterator] = $value;
      $iterator++;
    }
    $values = array_merge($values, $conditions);

    $prep = $this->database->prepare('UPDATE ' . $this->tables[0]['table'] . ' SET ' . $this->getValues() . $this->getConditions());

    return $this->executeQuery($prep, $values);
  }

  /**
   * @return string
   */
  protected function getValues(): string {
    $values = '';
    $iterator = 0;
    foreach ($this->values as $field => $value) {
      if ($values !== '') {
        $values .= ', ';
      }

      $values .= $field . ' = :value_placeholder_' . $iterator;
      $iterator++;
    }

    return $values;
  }

}