<?php

namespace Cherry;

use Exception;
use Cherry\Utils\Strings\StringManipulation;

/**
 * Class Settings
 *
 * @package Cherry
 */
class Settings {

  /** @var array $settings */
  public static array $settings = [];

  /** @var string $subsite */
  public static string $subsite = '';

  /**
   * Sets setting array
   *
   * @param array|null $settings
   */
  public static function setSettings(?array &$settings = NULL) {
    $all_settings = &$GLOBALS['cherry_settings'];
    // Assume this is a commandline, and use subsite set by the command.
    if (!isset($_SERVER['HTTP_HOST']) || !isset($_SERVER['REQUEST_URI'])) {
      if (!defined('SUBSITE')) {
        throw new Exception('When executing commandline code, you must set the subsite name via a constant with key "SUBSITE".');
      }
      static::$subsite = SUBSITE;
    }
    // This is not a commandline, and we expect to find the correct server variables set.
    elseif (!is_array($settings)) {
      foreach ($all_settings as $subsite => $values) {
        if (StringManipulation::contains($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], $values['url'])) {
          static::$subsite = $subsite;
        }
      }
    }
    static::$settings = $settings ?? $all_settings[static::$subsite];
  }

  /**
   * @param string $key
   *
   * @return bool
   */
  public static function has(string $key): bool {
    if (StringManipulation::contains($key, '.')) {
      $keys = StringManipulation::explode($key, '.');
      $return = static::$settings;
      foreach ($keys as $item) {
        $return = &$return[$item];
      }
      return !empty($return);
    }
    return isset(static::$settings[$key]);
  }

  /**
   * @param string $key
   *                  Key can be for example: category.item for recursive array keys
   * @param mixed $default
   *                  Default value in case setting doesn't exist
   *
   * @return false|mixed
   */
  public static function get(string $key, $default = FALSE) {
    if (StringManipulation::contains($key, '.')) {
      $keys = StringManipulation::explode($key, '.');
      $return = static::$settings;
      foreach ($keys as $item) {
        $return = &$return[$item];
      }
      return $return ?: $default ?: FALSE;
    }
    return static::$settings[$key] ?? $default ?: FALSE;
  }

  /**
   * @return array
   */
  public static function getAll(): array {
    return static::$settings;
  }

}