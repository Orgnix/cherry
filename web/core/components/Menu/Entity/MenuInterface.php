<?php

namespace Cherry\Menu\Entity;

use Cherry\Entity\EntityInterface;

/**
 * Interface MenuInterface
 *
 * @package Cherry\Menu
 */
interface MenuInterface extends EntityInterface {

  /**
   * @return string
   */
  public function getTitle();

  /**
   * @param string $title
   *
   * @return bool
   */
  public function setTitle(string $title);

  /**
   * @return string
   */
  public function getDescription();

  /**
   * @param string $description
   *
   * @return bool
   */
  public function setDescription(string $description);

  /**
   * @return string
   */
  public function getRoute();

  /**
   * @param string $route
   *
   * @return bool
   */
  public function setRoute(string $route);

  /**
   * @return string
   */
  public function getMenuType();

  /**
   * @param string $type
   *
   * @return bool
   */
  public function setMenuType(string $type);

  /**
   * @return MenuInterface|bool
   */
  public function getParent();

  /**
   * @param int $parent
   *
   * @return bool
   */
  public function setParent(int $parent);

  /**
   * @return mixed
   *
   * @throws \Exception
   */
  public function getChildren();

  /**
   * @param bool $translatable
   *
   * @return bool
   */
  public function setTranslatable(bool $translatable);

}