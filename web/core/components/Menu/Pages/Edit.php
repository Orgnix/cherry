<?php

namespace Cherry\Menu\Pages;

use Exception;
use Cherry;
use Cherry\Form\Form;
use Cherry\Menu\Entity\Menu;
use Cherry\Page\Page;
use Cherry\Route\RouteInterface;

/**
 * Class Menu
 *
 * @package Cherry\Page
 */
class Edit extends Page {

  /** @var Form $form */
  protected Form $form;

  /**
   * Config constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    $this->setParameters([
      'id' => 'menu',
      'title' => $this->translate('Menu'),
      'summary' => $this->translate('Menu options'),
    ]);
    parent::__construct($parameters, $route, $renderer);
    $this->setPermissions(['edit menu link']);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.' . $this->get('id'),
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * @param int $id
   *
   * @return bool|Form
   */
  protected function defaultForm(int $id) {
    try {
      $menuObject = Menu::load($id, FALSE);
    } catch (Exception $e) {
      return FALSE;
    }
    if (!$menuObject) {
      return FALSE;
    }
    return \Cherry::Form($menuObject);
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    if (!$this->hasParameter('eid')) {
      return NULL;
    }

    $form = $this->defaultForm((int)$this->get('eid'));
    if (!$form) {
      $result = '';
    } else {
      $result = $form->result();
    }
    return $this->getRenderer()
      ->setType()
      ->setTemplate('menu')
      ->render([
        'form' => $result,
      ]);
  }

}
