<?php

namespace Cherry\Menu;

use Cherry;
use Cherry\Element\ElementInterface;
use Cherry\Entity\EntityInterface;
use Cherry\Event\EventListener;

/**
 * Class MenuRender
 *
 * @package Cherry\Menu
 */
class Events extends EventListener {

  /**
   * {@inheritDoc}
   */
  public function EntityPostSave(?EntityInterface $entity) {
    if ($entity->getType() !== 'menu') {
      return;
    }

    \Cherry::Cache()->invalidateTags(['element:header']);
  }

  /**
   * {@inheritDoc}
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element) {
    if ($element_id !== 'header') {
      return;
    }

    // Load menu items
    $menus = \Cherry::Manifest('menu')
      ->fields(['id', 'title', 'description', 'route', 'type', 'parent', 'icon'])
      ->condition('status', 1)
      ->order('structure')
      ->result();

    if (!is_array($menus)) {
      $variables['menu'] = [];
    }

    // Loop over menus and add to array
    foreach ($menus as $key => $menu) {
      if ($menu['parent'] != 0 && isset($menus[$menu['parent']])) {
        $menus[$menu['parent']]['children'][] = $menu;
        unset($menus[$key]);
      }
    }

    // Add menu to variable array
    $variables['menu'] = $menus;
  }

}
