<?php

namespace Cherry\Queue\Exception;

/**
 * Class CallbackException.
 *
 * @package Cherry\Queue\Exception
 */
class CallbackException extends \Exception {
}