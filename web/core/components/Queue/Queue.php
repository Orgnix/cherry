<?php

namespace Cherry\Queue;

use Cherry\Container\ContainerInjectionInterface;
use Cherry\Database\Database;
use Cherry\Utils\Callback\Callback;
use Cherry\Utils\Callback\CallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Queue.
 *
 * @package Cherry\Queue
 */
class Queue implements ContainerInjectionInterface {

  /**
   * The database object.
   *
   * @var Database
   */
  protected Database $database;

  /**
   * Queue constructor.
   *
   * @param Database $database
   *   The database object.
   */
  public function __construct(Database $database) {
    $this->database = $database;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('database'));
  }

  /**
   * Add an item to Cherry's Queue.
   *
   * @param string $action
   *   The unique name of the action.
   * @param CallbackInterface $callback
   *   The callback for it.
   */
  public function add(string $action, CallbackInterface $callback, int $progress = 0, int $goal = 100, bool $permanent = FALSE) {
    if ($this->exists($action)) {
      return FALSE;
    }

    return $this->database
      ->insert('queue')
      ->values([
        'action' => $action,
        'class' => $callback->getClass() ?: 0,
        'method' => $callback->getMethod(),
        'parameters' => serialize($callback->getParameters()),
        'permanent' => $permanent ? 1 : 0,
        'progress' => $progress,
        'goal' => $goal,
      ])->execute();
  }

  /**
   * Update an existing item.
   *
   * @param string $action
   * @param int $progress
   *
   * @return bool|\Cherry\Database\Result
   */
  public function update(string $action, int $progress = 0) {
    if (!$this->exists($action)) {
      return FALSE;
    }

    return $this->database->update('queue')
      ->values([
        'progress' => $progress,
      ])
      ->condition('action', $action)
      ->execute();
  }

  /**
   * Delete an existing item.
   *
   * @param $action
   *
   * @return bool|\Cherry\Database\Result
   */
  public function delete($action) {
    if (!$this->exists($action)) {
      return FALSE;
    }

    return $this->database->delete('queue')
      ->condition('action', $action)
      ->execute();
  }

  /**
   * Returns callback based on action name.
   *
   * @param string $action
   *
   * @return array|bool
   *
   * @throws Exception\CallbackException
   */
  public function load(string $action) {
    if (!$this->exists($action)) {
      return FALSE;
    }

    $callback = new Callback();

    $query = $this->database->select('queue')
      ->condition('action', $action)
      ->execute();
    $results = $query->fetchAllAssoc();

    $queue = $query->getCount() > 0 ? reset($results) : FALSE;
    if ($queue) {
      $callback->setClass($queue['class']);
      $callback->setMethod($queue['method']);
      $callback->setParameters(unserialize($queue['parameters']));
    }

    return [$callback, $queue];
  }

  /**
   * Execute callback.
   *
   * @param string $action
   *
   * @return bool
   *
   * @throws \Cherry\Queue\Exception\CallbackException
   */
  public function execute(string $action): bool {
    list($callback, $queue) = $this->load($action) ?: [FALSE, FALSE];
    if (!$callback) {
      return FALSE;
    }

    // Remove queue from list
    if (boolval($queue['permanent']) === FALSE) {
      $this->delete($action);
    }
    return $callback->execute();
  }

  /**
   * Checks whether a given action exists.
   *
   * @param string $action
   *
   * @return bool
   */
  protected function exists(string $action): bool {
    $query = $this->database->select('queue')
      ->condition('action', $action)
      ->execute();

    return $query->getCount() > 0;
  }

}
