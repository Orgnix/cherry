<?php

namespace Cherry\Queue;

use Cherry\Container\ContainerInjectionInterface;
use Cherry\Database\Database;
use Cherry\Database\Result;
use Cherry\Logger\LoggerInterface;
use Cherry\Queue\Exception\CallbackException;
use Cherry\Utils\Callback\Callback;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Cron.
 *
 * @package Cherry\Queue
 */
class Cron implements ContainerInjectionInterface {

  /**
   * The database object.
   *
   * @var Database
   */
  protected Database $database;

  /**
   * The logger object.
   *
   * @var LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Cron constructor.
   *
   * @param Database $database
   *   The database object.
   * @param LoggerInterface $logger
   *   The logger object.
   */
  public function __construct(Database $database, LoggerInterface $logger) {
    $this->database = $database;
    $this->logger = $logger;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('logger')
    );
  }

  /**
   * Executes given cron tasks.
   */
  public function execute() {
    foreach ($this->getTasks() as $task) {
      $callback = new Callback();
      $callback->setClass($task['class']);
      $callback->setMethod($task['method']);
      $callback->setParameters(unserialize($task['parameters']));

      try {
        $callback->execute();
      } catch (CallbackException $e) {
        $this->logger->addInfo('Something went wrong executing cron task ' . $task['name'] . '. See the next log for more information.');
        $this->logger->addError($e->getMessage());
      }
    }
  }

  /**
   * Returns tasks that have to be executed on cron.
   *
   * @return array
   */
  protected function getTasks(): array {
    $query = $this->database
      ->select('cron')
      ->execute();

    if (!$query instanceof Result) {
      return [];
    }

    return $query->fetchAllAssoc();
  }

}
