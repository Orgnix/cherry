<?php

namespace Cherry\Permission;

use Cherry\Database\Result;

/**
 * Class PermissionManager
 *
 * @package Cherry\Permissions
 */
class PermissionManager {

  /**
   * Returns Permission object
   *
   * @param $permission
   *
   * @return Permission
   */
  public function getPermission($permission): Permission {
    $object = new Permission();
    return $object->load($permission);
  }

  /**
   * Returns all permissions
   *
   * @return bool|array
   */
  public function getAllPermissions() {
    $storage = \Cherry::Database()
      ->select('permissions')
      ->fields(NULL, ['permission'])
      ->execute();
    if (!$storage instanceof Result) {
      return FALSE;
    }

    $permissions = [];
    $results = $storage->fetchAllAssoc();
    foreach ($results as $result) {
      $permissions[] = $this->getPermission($result['permission']);
    }

    return $permissions;
  }

}
