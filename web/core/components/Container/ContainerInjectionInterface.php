<?php

namespace Cherry\Container;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface ContainerInjectionInterface
 *
 * @package Cherry\Container
 */
interface ContainerInjectionInterface {

  /**
   * Creates a new object with given parameters.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return mixed
   */
  public static function create(ContainerInterface $container);

}
