<?php

namespace Cherry\Container;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface ArgumentsAwareContainerInjectionInterface
 *
 * @package Cherry\Container
 */
interface ArgumentsAwareContainerInjectionInterface {

  /**
   * Instantiates a new instance of the current class, with custom (non-service) arguments.
   *
   * @param ContainerInterface $container
   *   The container object.
   * @param array $arguments
   *   The arguments that were passed to instantiate this class.
   *
   * @return mixed
   */
  public static function create(ContainerInterface $container, array $arguments);

}
