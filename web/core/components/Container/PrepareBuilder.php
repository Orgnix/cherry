<?php

namespace Cherry\Container;

use Cherry\Core;
use Cherry\ExtensionManager\ExtensionManager;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\FileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PrepareBuilder.
 *
 * @package Cherry\Container
 */
class PrepareBuilder {

  /**
   * The container builder.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder
   */
  public static ContainerBuilder $builder;

  /**
   * PrepareBuilder constructor.
   *
   * @param ContainerBuilder $builder
   *   The container builder.
   */
  public function __construct(ContainerBuilder $builder) {
    static::$builder = $builder;
  }

  /**
   * @return ContainerBuilder
   */
  public static function getBuilder(): ContainerBuilder {
    return static::$builder;
  }

  /**
   * Loads file into the builder.
   *
   * @throws \Exception
   */
  public function load(string $filename, FileLocator $locator, ?string $loader = NULL) {
    if (!$loader) {
      $loader = new YamlFileLoader(static::getBuilder(), $locator);
    } else {
      if (is_subclass_of($loader, FileLoader::class)) {
        $loader = new $loader(static::getBuilder(), $locator);
      } else {
        throw new \InvalidArgumentException('Class does not extend \Symfony\Component\DependencyInjection\Loader\FileLoader');
      }
    }

    $loader->load($filename);
  }

  /**
   * Prepares the container.
   *
   * @throws \Exception
   */
  public function prepare() {
    $extensionManager = new ExtensionManager();
    $locator = new FileLocator(Core::getCoreComponentsPath() . DIRECTORY_SEPARATOR . 'Database');
    $this->load('component.services.yml', $locator);
    $locator = new FileLocator(Core::getCoreComponentsPath());
    $this->load('core.services.yml', $locator);

    foreach ($extensionManager->getCoreExtensions() as $extension) {
      if (!$extension->isInstalled()) {
        continue;
      }

      $path = Core::getCoreExtensionsPath() . DIRECTORY_SEPARATOR . $extension->getExtension();
      if (!file_exists($path . DIRECTORY_SEPARATOR . 'extension.services.yml')) {
        continue;
      }
      $locator = new FileLocator($path);
      $this->load('extension.services.yml', $locator);
    }

    // Loop over contrib extensions and prepare services.
    foreach ($extensionManager->getContribExtensions() as $extension) {
      if (!$extension->isInstalled()) {
        continue;
      }

      $path = Core::getExtensionsPath() . DIRECTORY_SEPARATOR . $extension->getExtension();
      if (!file_exists($path . DIRECTORY_SEPARATOR . 'extension.services.yml')) {
        continue;
      }
      $locator = new FileLocator($path);
      $this->load('extension.services.yml', $locator);
    }
  }

  /**
   * @return \Symfony\Component\DependencyInjection\ContainerInterface
   *
   * @throws \Exception
   */
  public function getContainer(): ContainerInterface {
    return static::$builder->get('service_container');
  }

}
