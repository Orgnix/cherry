<?php

namespace Cherry\Event;

use Exception;

/**
 * Interface EventManagerInterface
 *
 * @package Cherry\Event
 */
interface EventManagerInterface {

  /**
   * Returns static event array
   *
   * @return array
   */
  public static function getEventListeners(): array;

  /**
   * Sets static events array
   *
   * @return void
   */
  public static function setEventListeners();

  /**
   * Installs all defined event listeners.
   * Requires a full cache clear to install new ones.
   *
   * @throws Exception
   */
  public function installEventListeners();

  /**
   * Installs single event with given parameters
   *
   * @param string $event
   * @param string $label
   * @param array $values
   *
   * @throws Exception
   */
  public function installEventListener(string $event, string $label, array $values);

}
