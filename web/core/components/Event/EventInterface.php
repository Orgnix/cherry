<?php

namespace Cherry\Event;

/**
 * Interface EventInterface
 *
 * @package Cherry\Event
 */
interface EventInterface {

  /**
   * Returns the event's name.
   *
   * @return string
   */
  public function getEventName();

  /**
   * Fires the event and runs through all listeners/subscribers.
   *
   * @param mixed $variables
   *                This would be the main variables (passed by reference)
   * @param array $otherArgs
   *                Any additional arguments as array, will be passed to the listener like so:
   *                fire($variables, ['arg1', ['arg2']]) => myListener($variables, 'arg1', ['arg2'])
   *
   * @return bool
   */
  public function fire(&$variables = [], array $otherArgs = []): bool;

  /**
   * Fires the event and runs through all listeners/subscribers.
   *
   * @param mixed $variables
   *                This would be the main variables (not passed by reference)
   * @param array $otherArgs
   *                Any additional arguments as array, will be passed to the listener like so:
   *                fire($variables, ['arg1', ['arg2']]) => myListener($variables, 'arg1', ['arg2'])
   *
   * @return bool
   */
  public function fireWithoutReference($variables = [], array $otherArgs = []): bool;

  /**
   * Sends information to listener
   *
   * @param array $listener
   * @param mixed $variables
   *                This would be the main variables (passed by reference)
   * @param array $otherArgs
   *                Any additional arguments as array, will be passed to the listener like so:
   *                fire($variables, ['arg1', ['arg2']]) => myListener($variables, 'arg1', ['arg2'])
   *
   * @return bool
   */
  public function sendListener(array $listener, &$variables, array &$otherArgs): bool;

  /**
   * Sets the name of the event to be fired.
   *
   * @param string $eventName
   *
   * @return self
   */
  public function setEventName(string $eventName): self;

}