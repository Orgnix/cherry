<?php

namespace Cherry\Event;

use Cherry\Cache\CacheInterface;
use Cherry\Container\PrepareBuilder;
use Cherry\Element\ElementInterface;
use Cherry\Entity\EntityInterface;
use Cherry\Request;

/**
 * interface EventListenerInterface
 *
 * @package Cherry\Event
 */
interface EventListenerInterface {

  /**
   * alterElementCacheOptions event
   *
   * @param array|null $cacheOptions
   * @param string     $element_id
   *
   * @return mixed
   */
  public function alterElementCacheOptions(?array &$cacheOptions, string $element_id);

  /**
   * siteSettingsFormPreBuild event
   *
   * @param array|null $fields
   *
   * @return mixed
   */
  public function siteSettingsFormPreBuild(?array &$fields);

  /**
   * postInit event
   *
   * @param \Cherry\Request $request
   *
   * @return mixed
   */
  public function postInit(Request $request);

  /**
   * pagePreRender event
   *
   * @param array|null $variables
   * @param string     $page_id
   *
   * @return mixed
   */
  public function pagePreRender(?array &$variables, string $page_id);

  /**
   * elementPreRender event
   *
   * @param array|null       $variables
   * @param string           $element_id
   * @param ElementInterface $element
   *
   * @return mixed
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element);

  /**
   * addSearchResults event
   *
   * @param array|null $results
   * @param string     $keyword
   *
   * @return mixed
   */
  public function addSearchResults(?array &$results, string $keyword);

  /**
   * preRender event
   *
   * @param array       $variables
   * @param string|null $view_mode
   */
  public function preRender(array &$variables, ?string $view_mode);

  /**
   * FormAlter event
   *
   * @param array  $form
   * @param string $form_id
   */
  public function FormAlter(array &$form, string $form_id);

  /**
   * FormPreSubmitAlter event
   *
   * @param array $values
   * @param array $form
   * @param string $form_id
   *
   * @return mixed
   */
  public function FormPreSubmitAlter(array &$values, array $form, string $form_id);

  /**
   * entityChooseBundleForm event
   *
   * @param array|null $form
   * @param string     $form_id
   *
   * @return mixed
   */
  public function entityChooseBundleForm(?array &$form, string $form_id);

  /**
   * stringTranslationPresave event
   *
   * @param array       $variables
   * @param string|null $string
   * @param string|null $from_langcode
   * @param string|null $to_langcode
   */
  public function stringTranslationPresave(&$variables, ?string $string, ?string $from_langcode, ?string $to_langcode);

  /**
   * EntityPreDelete event
   *
   * @param EntityInterface|null $entity
   */
  public function EntityPreDelete(?EntityInterface $entity);

  /**
   * EntityPostDelete event
   *
   * @param EntityInterface|null $entity
   */
  public function EntityPostDelete(?EntityInterface $entity);

  /**
   * EntityPreSave event
   *
   * @param EntityInterface|null $entity
   */
  public function EntityPreSave(?EntityInterface $entity);

  /**
   * EntityPostSave event
   *
   * @param EntityInterface|null $entity
   */
  public function EntityPostSave(?EntityInterface $entity);

  /**
   * preRetrieve event
   *
   * @param array|null $information
   *
   * @return mixed
   */
  public function preRetrieve(?array $information = []);

  /**
   * postRetrieve event
   *
   * @param EntityInterface|null $data
   *
   * @return mixed
   */
  public function postRetrieve(?EntityInterface $data);

  /**
   * prepareContainer event
   *
   * @param PrepareBuilder $builder
   *
   * @return mixed
   */
  public function prepareContainer(PrepareBuilder $builder);

  /**
   * preTransmit event
   *
   * @param array|null $information
   *
   * @return mixed
   */
  public function preTransmit(?array $information = []);

  /**
   * postTransmit event
   * Gets fired before saving the entity to Cherry
   *
   * @param EntityInterface|null $data
   *
   * @return mixed
   */
  public function postTransmit(?EntityInterface $data);

  /**
   * preSend event
   * Gets fired right before the response object is sent to the front-end
   *
   * @param mixed $response
   *
   * @return mixed
   */
  public function preSend(&$response);

  /**
   * twigExtensionsAlter event
   * Gets fired when adding new Twig extensions
   *
   * @param array|null $functions
   *
   * @return mixed
   */
  public function twigExtensionsAlter(?array &$functions);

}
