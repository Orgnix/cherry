<?php

namespace Cherry\Event;

use Exception;
use Cherry\Database\Result;
use Cherry\ExtensionManager\ExtensionManagerInterface;
use Cherry\Translation\StringTranslation;
use Cherry\YamlReader;

/**
 * Class EventManager
 *
 * @package Cherry\Event
 */
class EventManager implements EventManagerInterface {
  use StringTranslation;

  /** @var array $events */
  protected static array $events = [];

  /**
   * {@inheritDoc}
   */
  public static function getEventListeners(): array {
    return static::$events;
  }

  /**
   * {@inheritDoc}
   */
  public static function setEventListeners() {
    $storage = \Cherry::Database()
      ->select('event_listeners')
      ->fields(NULL, ['event', 'label', 'service', 'class'])
      ->execute();
    if (!$storage instanceof Result) {
      return;
    }
    $results = $storage->fetchAllAssoc();

    if (count($results) === 0) {
      \Cherry::Cache()->clearAllCaches();
    }

    $events = [];
    foreach ($results as $result) {
      if (!isset($events[$result['event']])) {
        $events[$result['event']] = [];
      }

      $events[$result['event']][] = $result;
    }

    static::$events = $events;
  }

  /**
   * {@inheritDoc}
   */
  public function installEventListeners() {
    /** @var ExtensionManagerInterface $extensionManager */
    $extensionManager = \Cherry::Container()->get('extension.manager');
    $extensions = $extensionManager->getInstalledExtensions();

    $this->installCoreEventListeners();

    foreach ($extensions as $extension) {
      $info = $extension->getInfo();

      // Continue to next extension in case this one doesn't have a .info file.
      if (!$info || !isset($info['event_listeners'])) {
        continue;
      }

      $events = $info['event_listeners'];
      foreach ($events as $event => $listener) {
        if (isset($listener['listeners'])) {
          foreach ($listener['listeners'] as $listener_key => $listener_values) {
            $this->installEventListener($event, $listener_key, $listener_values);
          }
        } else {
          $this->installEventListener($event, $extension->getExtension(), $listener);
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function installEventListener(string $event, string $label, array $values) {
    try {
      \Cherry::Database()
        ->insert('event_listeners')
        ->values([
          'id' => NULL,
          'event' => $event,
          'label' => $label,
          'service' => $values['service'] ?? '',
          'class' => $values['class'] ?? '',
        ])->execute();
    } catch(Exception $e) {
      \Cherry::Logger()->addError($e->getMessage(), 'EventManager');
    }
  }

  /**
   * Installs core event listeners.
   *
   * @throws \Exception
   */
  protected function installCoreEventListeners() {
    $info = YamlReader::readCore();
    $events = $info['event_listeners'];
    foreach ($events as $event => $listener) {
      if (isset($listener['listeners'])) {
        foreach ($listener['listeners'] as $listener_key => $listener_values) {
          $this->installEventListener($event, $listener_key, $listener_values);
        }
      } else {
        $this->installEventListener($event, 'core', $listener);
      }
    }
  }

}