<?php

namespace Cherry\Event;

use Exception;
use Cherry\Container\ArgumentsAwareContainerInjectionInterface;
use Cherry\Logger\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cherry\Core;
use Cherry\Translation\StringTranslation;

/**
 * Class Event
 *
 * @package Cherry\Events
 */
class Event implements EventInterface, ArgumentsAwareContainerInjectionInterface {
  use StringTranslation;

  /** @var string $eventName */
  protected string $eventName;

  /** @var ContainerInterface $container */
  protected ContainerInterface $container;

  /** @var LoggerInterface $logger */
  protected LoggerInterface $logger;

  /**
   * Event constructor.
   *
   * @param string|null $eventName
   * @param ContainerInterface $container
   * @param LoggerInterface $logger
   */
  public function __construct(ContainerInterface $container, LoggerInterface $logger, string $eventName) {
    $this->setEventName($eventName);
    $this->container = $container;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $arguments) {
    return new static(
      $container,
      $container->get('logger'),
      ...$arguments
    );
  }

  /**
   * {@inheritDoc}
   */
  public function fire(&$variables = [], array $otherArgs = []): bool {
    foreach ($this->getListeners() as $key => $listener) {
      $this->sendListener($listener, $variables, $otherArgs);
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function fireWithoutReference($variables = [], array $otherArgs = []): bool {
    foreach ($this->getListeners() as $key => $listener) {
      $this->sendListener($listener, $variables, $otherArgs);
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function sendListener(array $listener, &$variables, array &$otherArgs): bool {
    if (isset($listener['service']) && !empty($listener['service'])) {
      /** @var EventListenerInterface $class */
      $class = $this->container->get($listener['service']);
    } elseif (isset($listener['class']) && !empty($listener['class'])) {
      /** @var EventListenerInterface $class */
      $class = Core::loadClass($listener['class']);
    } else {
      $this->logger->addError(
        $this->translate('No service or class defined for :eventName event in :label listener.', [
          ':eventName' => $this->getEventName(),
          ':label' => $listener['label'],
        ]),
        'Event');
      return FALSE;
    }
    if (!$class instanceof EventListenerInterface) {
      $this->logger->addError('EventListener ' . $listener['class'] . ' must be instance of \Cherry\Event\EventListenerInterface', 'Event');
      return FALSE;
    }
    try {
      if (!is_array($otherArgs)) {
        $this->logger->addError('The other arguments have to be of the array format.', 'Event');
        return FALSE;
      }

      if (!method_exists($class, $this->getEventName())) {
        $this->logger->addError(
          $this->translate(':eventName method does not exist in :label listener class.', [
              ':eventName' => $this->getEventName(),
              ':label' => $listener['label'],
            ]),
          'Event');
        return FALSE;
      }
      // Call the listener class' method
      $class->{$this->getEventName()}($variables, ...$otherArgs);
    } catch (Exception $exception) {
      $this->logger->addError($exception->getMessage(), 'Event');
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns array of classes and methods with listeners.
   *
   * @return array
   */
  protected function getListeners(): array {
    $listeners = EventManager::getEventListeners();

    return $listeners[$this->getEventName()] ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function getEventName(): string {
    return $this->eventName;
  }

  /**
   * {@inheritDoc}
   */
  public function setEventName(string $eventName): self {
    $this->eventName = $eventName;
    return $this;
  }

}
