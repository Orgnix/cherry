<?php

namespace Cherry\Event;

use Cherry\Cache\CacheInterface;
use Cherry\Container\PrepareBuilder;
use Cherry\Element\ElementInterface;
use Cherry\Entity\EntityInterface;
use Cherry\Request;
use Cherry\Translation\StringTranslation;

/**
 * Class EventListener
 *
 * Only dummy methods will be in here to provide a template for events in child classes.
 *
 * @package Cherry\Event
 */
class EventListener implements EventListenerInterface {
  use StringTranslation;

  /**
   * {@inheritDoc}
   */
  public function alterElementCacheOptions(?array &$cacheOptions, string $element_id) {}

  /**
   * {@inheritDoc}
   */
  public function siteSettingsFormPreBuild(?array &$fields) {}

  /**
   * {@inheritDoc}
   */
  public function postInit(Request $request) {}

  /**
   * {@inheritDoc}
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element) {}

  /**
   * {@inheritDoc}
   */
  public function pagePreRender(?array &$variables, string $page_id) {}

  /**
   * {@inheritDoc}
   */
  public function addSearchResults(?array &$results, string $keyword) {}

  /**
   * {@inheritDoc}
   */
  public function preRender(?array &$variables, ?string $view_mode) {}

  /**
   * {@inheritDoc}
   */
  public function preSend(&$response) {}

  /**
   * {@inheritDoc}
   */
  public function FormAlter(?array &$form, string $form_id) {}

  /**
   * {@inheritDoc}
   */
  public function FormPreSubmitAlter(array &$values, array $form, string $form_id) {}

  /**
   * {@inheritDoc}
   */
  public function entityChooseBundleForm(?array &$form, string $form_id) {}

  /**
   * {@inheritDoc}
   */
  public function stringTranslationPresave(&$variables, ?string $string, ?string $from_langcode, ?string $to_langcode) {}

  /**
   * {@inheritDoc}
   */
  public function EntityPreDelete(?EntityInterface $entity) {}

  /**
   * {@inheritDoc}
   */
  public function EntityPostDelete(?EntityInterface $entity) {}

  /**
   * {@inheritDoc}
   */
  public function EntityPreSave(?EntityInterface $entity) {}

  /**
   * {@inheritDoc}
   */
  public function EntityPostSave(?EntityInterface $entity) {}

  /**
   * {@inheritDoc}
   */
  public function preRetrieve(?array $information = []) {}

  /**
   * {@inheritDoc}
   */
  public function prepareContainer(PrepareBuilder $builder) {}

  /**
   * {@inheritDoc}
   */
  public function postRetrieve(?EntityInterface $data) {}

  /**
   * {@inheritDoc}
   */
  public function preTransmit(?array $information = []) {}

  /**
   * {@inheritDoc}
   */
  public function postTransmit(?EntityInterface $data) {}

  /**
   * {@inheritDoc}
   */
  public function twigExtensionsAlter(?array &$functions) {}

}
