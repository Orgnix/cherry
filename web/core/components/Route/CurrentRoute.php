<?php

namespace Cherry\Route;

/**
 * Class CurrentRoute.
 *
 * @package Cherry\Route
 */
class CurrentRoute {

  /** @var RouteInterface $route */
  protected RouteInterface $route;

  /**
   * CurrentRoute constructor
   */
  public function __construct() {
    $this->route = Route::getCurrent();
  }

  /**
   * Returns current route object.
   *
   * @return RouteInterface
   */
  public function getObject(): RouteInterface {
    return $this->route;
  }

  /**
   * Returns current route name.
   *
   * @return string
   */
  public function getRoute(): string {
    return $this->getObject()->getRoute();
  }

  /**
   * Returns route parameters.
   *
   * @return array
   */
  public function getParameters(): array {
    return $this->getObject()->getParameters();
  }

  /**
   * Returns route URL.
   *
   * @param bool $replaceParams
   *
   * @return array
   */
  public function getUrl(bool $replaceParams = TRUE): array {
    return $this->getObject()->getUrl($replaceParams);
  }

  /**
   * Returns route URI.
   *
   * @return string|string[]
   */
  public function getUri() {
    return $this->getObject()->getUri();
  }

  /**
   * Returns value for given key if it exists, or false if it doesn't.
   *
   * @param string|int $key
   *
   * @return mixed
   */
  public function getValue($key) {
    return $this->getObject()->getValue($key);
  }

}
