<?php

namespace Cherry\Route;

use Cherry;
use Cherry\Container\ContainerInjectionInterface;
use Cherry\Event\EventListener;
use Cherry\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\TwigFunction;

/**
 * Class Breadcrumb
 *
 * @package Cherry\Route
 */
class Events extends EventListener implements ContainerInjectionInterface {

  /** @var RouteInterface $route */
  protected RouteInterface $route;

  /**
   * TwigExtensions constructor.
   *
   * @param RouteInterface $route
   */
  public function __construct(RouteInterface $route) {
    $this->route = $route;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('route', FALSE)
    );
  }

  /**
   * {@inheritDoc}
   */
  public function twigExtensionsAlter(?array &$functions) {
    $functions[] = new TwigFunction('route', [$this, 'getRoute']);
  }

  /**
   * Creates url from route
   *
   * @param string $route
   * @param array  $values
   *
   * @return null|string
   */
  public function getRoute(string $route, array $values = []): ?string {
    $route = $this->route->load($route);
    if (!$route) {
      return NULL;
    }
    foreach ($values as $key => $value) {
      $route = $route->setValue($key, $value);
    }
    return Url::fromRoute($route);
  }

}
