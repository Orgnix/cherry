<?php

namespace Cherry\Route;

use Cherry\Bags\ParameterBag;
use Cherry\Bags\ValueBag;
use Cherry\Database\Result;
use Cherry\Element\ElementInterface;
use Cherry\Page\PageInterface;
use Cherry\Utils\Callback\Callback;
use Cherry\Renderer;
use Cherry\Settings;
use Cherry\Utils\Classes\ArrayableClassTrait;
use Cherry\Utils\Strings\StringManipulation;
use Cherry\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Route
 *
 * @package Cherry\Route
 */
class Route implements RouteInterface {
  use ArrayableClassTrait;
  use ParameterBag;
  use ValueBag {
    setValues as setValueBagValues;
    setValue as setValueBagValue;
  }

  /** @var RouteInterface $current */
  protected static RouteInterface $current;

  /** @var string $route */
  protected string $route;

  /** @var string $controller */
  protected string $controller;

  /** @var string $url */
  protected string $url;

  /** @var bool $rest */
  protected bool $rest = FALSE;

  /** @var bool $page */
  protected bool $page = FALSE;

  /** @var Request $request */
  protected Request $request;

  /** @var Renderer $renderer */
  protected Renderer $renderer;

  /**
   * Route constructor.
   *
   * @param Request $request
   * @param Renderer $renderer
   */
  public function __construct(Request $request, Renderer $renderer) {
    $this->request = $request::createFromGlobals();
    $this->renderer = $renderer;
  }

  /**
   * Returns either bool or route object based on route string
   *
   * @param string $route
   *
   * @return bool|self
   */
  public function load(string $route) {
    $query = \Cherry::Database()
      ->select('routes')
      ->condition('route', $route)
      ->execute();
    if (!$query instanceof Result) {
      return FALSE;
    }
    $result = $query->fetchAllAssoc();
    $result = reset($result);
    if (!is_array($result)) {
      return FALSE;
    }

    return $this->setValues($route, $result['controller'], unserialize($result['parameters']), $result['url'], $result['rest'] ?? FALSE, $result['page'] ?? FALSE);
  }

  /**
   * Sets static current property
   *
   * @param RouteInterface $route
   */
  public static function setCurrent(RouteInterface $route) {
    static::$current = $route;
  }

  /**
   * Returns current route
   *
   * @return RouteInterface
   */
  public static function getCurrent(): RouteInterface {
    return static::$current;
  }

  /**
   * @return null|PageInterface|ElementInterface
   */
  public function getObject() {
    $controller = $this->getController();
    if (!class_exists($controller)) {
      return NULL;
    }
    $parameters = Url::getRefactoredParameters($this);
    /** @var PageInterface $object */
    $object = new $controller($parameters, $this, $this->renderer);
    if (!$object instanceof PageInterface && !$object instanceof ElementInterface) {
      return NULL;
    }

    return $object;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    $parameters = Url::getRefactoredParameters($this);
    $callback = new Callback();
    $callback->setClass($this->controller);
    $callback->setMethod('render');
    $callback->setClassParameters([$parameters, $this, $this->renderer]);
    return \Cherry::Cache()->getContentData($this->getObject()->getCacheOptions(), $callback);
  }

  /**
   * {@inheritDoc}
   */
  public function setValue(string $key, $value): self {
    switch ($key) {
      case 'parameters':
        $this->setParameters($value);
        break;
      case 'controller':
        $this->controller = $value;
        break;
      case 'route':
        $this->route = $value;
        break;
      case 'url':
        $this->url = $value;
        break;
      case 'rest':
        $this->rest = $value;
        break;
      case 'page':
        $this->page = $value;
        break;
      default:
        $this->setValueBagValue($key, $value);
        break;
    }
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setValues(string $route, string $controller, array $parameters, string $url, bool $rest = FALSE, bool $page = FALSE, ?array $values = NULL): self {
    $this->route = $route;
    $this->controller = $controller;
    $this->setParameters($parameters);
    $this->url = $url;
    $this->rest = $rest;
    $this->page = $page;
    if (!is_null($values)) {
      $this->setValueBagValues($values);
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getRoute(): string {
    return $this->route;
  }

  /**
   * Returns whether route is a rest call
   *
   * @return bool
   */
  public function isRest(): bool {
    return $this->rest ?? FALSE;
  }

  /**
   * Returns whether route is a page
   *
   * @return bool
   */
  public function isPage(): bool {
    return $this->page ?? FALSE;
  }

  /**
   * Returns whether route is an element
   *
   * @return bool
   */
  public function isElement(): bool {
    return $this->element ?? FALSE;
  }

  /**
   * Returns current route
   *
   * @return string
   */
  public function getController(): string {
    return $this->controller;
  }

  /**
   * {@inheritDoc}
   */
  public function getUrl(bool $replaceParams = TRUE) {
    return Settings::get('root.web.url') . $this->getUri($replaceParams);
  }

  /**
   * {@inheritDoc}
   */
  public function getUri(bool $replaceParams = TRUE) {
    return $this->replaceUrlParams($replaceParams, $this->url);
  }

  /**
   * @param bool   $replaceParams
   * @param string $url
   *
   * @return string|string[]
   */
  protected function replaceUrlParams(bool $replaceParams, string $url) {
    if (!$replaceParams) {
      return $url;
    }
    $current_params = [];
    foreach ($this->values as $param => $value) {
      if (isset($this->parameters[$param])) {
        if (is_null($value)) {
          $value = '';
        } elseif (!is_string($value) && !is_int($value)) {
          continue;
        }
        $url = StringManipulation::replace($url, '{' . $param . '}', $value ?? NULL);
      } else {
        $url = \Cherry::Container()->get('url')->addParamsToUrl($param, $value, $url, $current_params);
        $current_params[$param] = $value;
      }
    }
    return $url;
  }

  /**
   * Shorthand function to check whether route exists.
   *
   * @param string $route
   *
   * @return bool
   */
  public function routeExists(string $route): bool {
    $route = $this->load($route);
    return $route !== FALSE;
  }

  /**
   * Saves route to database (Insert / update depending on current status)
   */
  public function save() {
    if ($this->routeExists($this->route)) {
      $query = \Cherry::Database()
        ->update('routes')
        ->condition('route', $this->route)
        ->values([
          'controller' => $this->controller,
          'parameters' => serialize($this->parameters),
          'url' => $this->url,
        ]);
      return $query->execute();
    }

    $query = \Cherry::Database()
      ->insert('routes')
      ->values([
        'route' => $this->route,
        'controller' => $this->controller,
        'parameters' => serialize($this->parameters),
        'url' => $this->url,
      ]);
    return $query->execute();
  }

}
