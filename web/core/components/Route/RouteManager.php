<?php

namespace Cherry\Route;

use Cherry\Utils\Arrays\ArrayManipulation;
use Cherry\Database\Result;
use Cherry\ExtensionManager\ExtensionManagerInterface;
use Cherry\Person\Entity\Person;
use Cherry\Utils\Strings\StringManipulation;
use Cherry\Translation\StringTranslation;
use Cherry\Url;
use Cherry\YamlReader;
use Mimey\MimeTypes;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RouteManager
 *
 * @package Cherry\Route
 */
class RouteManager {
  use StringTranslation;

  /** @var ExtensionManagerInterface $extensionManager */
  protected ExtensionManagerInterface $extensionManager;

  /**
   * RouteManager constructor.
   *
   * @param ExtensionManagerInterface $extension_manager
   */
  public function __construct(ExtensionManagerInterface $extension_manager) {
    $this->extensionManager = $extension_manager;
  }

  /**
   * Installs routes from extension.routing.yml files
   * Requires a full cache clear to install new routes.
   */
  public function installRoutes() {
    $extensions = $this->extensionManager->getInstalledExtensions();
    $this->installCoreRoutes();
    foreach ($extensions as $extension) {
      $routing = YamlReader::readExtension($extension->getExtension(), 'routing');
      if (!$routing) {
        $routing = YamlReader::readComponent($extension->getExtension(), 'routing');
      }
      if (!$routing) {
        continue;
      }
      foreach ($routing as $route => $options) {
        $routeStorage = \Cherry::Container()->get('route')
          ->setValues($route, $options['controller'], $options['parameters'] ?? [], $options['url'], $options['rest'] ?? FALSE);
        if (!$routeStorage->save()) {
          \Cherry::Logger()->addFailure($this->translate('Something went wrong trying to add a route [:route]', [':route', $route]), 'RouteManager');
        }
      }
    }
  }

  /**
   * Installs core routes.
   */
  protected function installCoreRoutes() {
    $routing = YamlReader::readCore('routing');
    foreach ($routing as $route => $options) {
      $routeStorage = \Cherry::Container()->get('route')
        ->setValues($route, $options['controller'], $options['parameters'] ?? [], $options['url'], $options['rest'] ?? FALSE);
      if (!$routeStorage->save()) {
        \Cherry::Logger()->addFailure($this->translate('Something went wrong trying to add a route [:route]', [':route', $route]), 'RouteManager');
      }
    }
  }

  /**
   * Checks whether route exists in database
   *
   * @param string $route
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function routeExists(string $route): bool {
    $query = \Cherry::Database()
      ->select('routes')
      ->condition('route', $route)
      ->execute();
    if (!$query instanceof Result) {
      return FALSE;
    }
    $results = $query->fetchAllAssoc();
    if (count($results) > 1) {
      \Cherry::Logger()->addError(
        $this->translate('More than one routes with name :route found.', [':route' => $route]),
        'RouteManager'
      );
      return FALSE;
    }

    return count($results) === 1;
  }

  /**
   * Loads route based on given route name as string.
   *
   * @param string $route
   *
   * @return mixed
   */
  public function loadRoute(string $route) {
    return \Cherry::Container()->get('route')->load($route);
  }

  /**
   * Matches given URL with route, parameters are stripped away in this method.
   *
   * @param string              $url
   *                              Url should not contain the base url, filter this out.
   * @param RouteInterface|null $fallback
   *                              Fallback route to use when main route is not available
   *                              Use null to not use a fallback.
   *
   * @return RedirectResponse|RouteInterface|null
   */
  public function routeMatch(string $url, ?RouteInterface $fallback = NULL) {
    $route = NULL;
    $query = \Cherry::Database()
      ->select('routes')
      ->execute();

    // Remove query parameters, https and http from URL.
    $url = StringManipulation::regex_replace($url, '/\?(.*$)/', '');
    $url = StringManipulation::replace($url, 'https://', '');
    $url = StringManipulation::replace($url, 'http://', '');

    $results = $query->fetchAllAssoc();

    // First check whether this actual url exists without checking parameters.
    foreach ($results as $result) {
      $result['parameters'] = unserialize($result['parameters']);
      if ($result['url'] === $url) {
        return $this->loadRoute($result['route']);
      }
    }

    // If literal url doesn't exist, replace with parameters and check again.
    if (!$route) {
      foreach ($results as $result) {
        $result['parameters'] = unserialize($result['parameters']);
        if (count($result['parameters']) > 0) {
          $exploded_route_url = ArrayManipulation::removeEmptyEntries(StringManipulation::explode($result['url'], '/'), TRUE, TRUE);
          $exploded_url = ArrayManipulation::removeEmptyEntries(StringManipulation::explode($url, '/'), TRUE, TRUE);
          $reworked_parameters = [];
          foreach ($result['parameters'] as $key => $id) {
            if (!isset($exploded_url[$id])) {
              continue;
            }

            $exploded_route_url[$id] = $exploded_url[$id];
            $reworked_parameters[$key] = $exploded_url[$id];
          }
          if ($exploded_route_url === $exploded_url) {
            /** @var RouteInterface $route */
            $route = $this->loadRoute($result['route']);
            foreach ($reworked_parameters as $key => $value) {
              $route = $route->setValue($key, $value);
            }
            return $route;
          }
        }
      }
    }

    // Automatically redirect to login page if person is not logged in.
    $person = Person::getCurrentPerson();
    if ($person === 0
      && ($route instanceof RouteInterface && $route->getRoute() !== 'login')) {
      $route = $this->loadRoute('login');
      $url = Url::fromRoute($route);
      $redirect = new RedirectResponse($url);
      return $redirect->send();
    }

    // Install routes if the error route isn't found.
    if (!$this->routeExists('error')) {
      $this->installRoutes();
    }

    if (!$route instanceof RouteInterface && !$route instanceof RedirectResponse) {
      $route = $fallback;
    }

    if (!$route instanceof RouteInterface && !$route instanceof RedirectResponse) {
      $route = $this->loadRoute('error')->setValue('key', '404');
    }

    // Return route, fallback or 404 error page.
    return $route;
  }

}
