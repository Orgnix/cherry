<?php

namespace Cherry\Bags;

/**
 * Trait ValueBag
 *
 * @package Cherry\Bags
 */
trait ValueBag {

  /** @var array $values */
  protected array $values = [];

  /**
   * Returns bool, checks if value exists in bag.
   *
   * @param $key
   *
   * @return bool
   */
  public function hasValue($key): bool {
    return isset($this->values[$key]);
  }

  /**
   * Returns all values.
   *
   * @return array
   */
  public function getValues(): array {
    return $this->values;
  }

  /**
   * Sets all values.
   *
   * @param array|null $values
   *
   * @return self
   */
  public function setValues(?array $values): self {
    $this->values = $values ?? [];

    return $this;
  }

  /**
   * Returns value for given key if it exists, or false if it doesn't.
   *
   * @param string|int $key
   *
   * @return mixed
   */
  public function getValue($key) {
    if (!$this->hasValue($key)) {
      return FALSE;
    }

    return $this->values[$key];
  }

  /**
   * Sets a single value.
   *
   * @param $key
   * @param $value
   *
   * @return self
   */
  public function setValue($key, $value): self {
    $this->values[$key] = $value;

    return $this;
  }

}
