<?php

namespace Cherry\Bags;

use Cherry\Utils\Strings\StringManipulation;

/**
 * Trait ParameterBag
 *
 * @package Cherry\Bags
 */
trait ParameterBag {

  /** @var array $parameters */
  protected array $parameters = [];

  /**
   * Returns bool, checks if parameter exists in bag.
   *
   * @param $key
   *
   * @return bool
   */
  public function hasParameter($key): bool {
    return isset($this->parameters[$key]);
  }

  /**
   * Returns all parameters.
   *
   * @return array
   */
  public function getParameters(): array {
    return $this->parameters;
  }

  /**
   * Sets all parameters.
   *
   * @param array $parameters
   *
   * @return self
   */
  public function setParameters(array $parameters): self {
    $this->parameters = $parameters;

    return $this;
  }

  /**
   * Sets cursor for parameter if key exists.
   *
   * @param string|int $key
   *
   * @return mixed
   */
  public function getParameter($key) {
    if (!$this->hasParameter($key)) {
      return FALSE;
    }

    return $this->parameters[$key];
  }

  /**
   * Sets a single parameter.
   *
   * @param string $key
   * @param $value
   *
   * @return self
   */
  public function setParameter(string $key, $value): self {
    if (StringManipulation::contains($key, '.')) {
      $keys = StringManipulation::explode($key, '.');
      $key = end($keys);
      $return = &$this->parameters;
      foreach ($keys as $item) {
        $return = &$return[$item];
        if ($item === $key) {
          $return = $value;
        }
      }
      return $this;
    }
    $this->parameters[$key] = $value;
    return $this;
  }

}
