<?php

namespace Cherry\Form;

use Cherry;
use Cherry\Core;
use Cherry\Event\EventListener;
use Twig\TwigFunction;

/**
 * Class Events
 *
 * @package Cherry\Form
 */
class Events extends EventListener {

  /**
   * {@inheritDoc}
   */
  public function twigExtensionsAlter(?array &$functions) {
    $functions[] = new TwigFunction('element', [$this, 'getElement'], ['is_safe' => ['html']]);
    $functions[] = new TwigFunction('formElement', [$this, 'getFormElement'], ['is_safe' => ['html']]);
  }

  /**
   * Returns element content without having to add |raw to it (cleaner twig code)
   *
   * @param string|null $content
   *
   * @return null|string
   */
  public function getElement(?string $content): ?string {
    return $content;
  }

  /**
   * Creates a new form element
   *
   * @param string $element
   * @param array  $variables
   *
   * @return null|string
   */
  public function getFormElement(string $element, array $variables = []): ?string {
    $formElementString = '\\Cherry\\Form\\FormElements\\' . ucfirst($element);
    $formElement = Core::loadClass($formElementString);
    if (!$formElement instanceof FormElement) {
      return NULL;
    }

    return $formElement->render($variables);
  }

}
