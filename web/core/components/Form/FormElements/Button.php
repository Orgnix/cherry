<?php

namespace Cherry\Form\FormElements;

use Cherry;
use Cherry\Form\FormElement;

/**
 * Class Button
 *
 * @package Cherry\Form\FormElements
 */
class Button extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    parent::render($variables);

    return $this->renderer->setType('core.Form')
      ->setTemplate('button')
      ->render($variables);
  }

}
