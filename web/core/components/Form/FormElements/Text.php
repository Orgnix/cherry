<?php

namespace Cherry\Form\FormElements;

use Cherry;
use Cherry\Form\FormElement;

/**
 * Class Text
 *
 * @package Cherry\Form\FormElements
 */
class Text extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    parent::render($variables);

    return $this->renderer->setType('core.Form')
      ->setTemplate('text')
      ->render($variables);
  }

}
