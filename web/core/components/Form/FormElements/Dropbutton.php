<?php

namespace Cherry\Form\FormElements;

use Cherry;
use Cherry\Form\FormElement;

/**
 * Class Dropbutton
 *
 * @package Cherry\Form\FormElements
 */
class Dropbutton extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    parent::render($variables);

    return $this->renderer->setType('core.Form')
      ->setTemplate('dropbutton')
      ->render($variables);
  }

}
