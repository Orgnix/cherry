<?php

namespace Cherry\Form\FormElements;

use Cherry;
use Cherry\Form\FormElement;

/**
 * Class Checkbox
 *
 * @package Cherry\Form\FormElements
 */
class Checkbox extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    parent::render($variables);

    return $this->renderer->setType('core.Form')
      ->setTemplate('checkbox')
      ->render($variables);
  }

}
