<?php

namespace Cherry\Form\FormElements;

use Cherry;
use Cherry\Core;
use Cherry\Form\FormElement;

/**
 * Class Select
 *
 * @package Cherry\Form\FormElements
 */
class Select extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    parent::render($variables);

    if (isset($variables['#options_callback'])) {
      $variables['options'] = Core::Callback($variables['#options_callback']);
    }

    return $this->renderer->setType('core.Form')
      ->setTemplate('select')
      ->render($variables);
  }

}
