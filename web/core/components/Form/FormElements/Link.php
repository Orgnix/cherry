<?php

namespace Cherry\Form\FormElements;

use Cherry;
use Cherry\Form\FormElement;

/**
 * Class Link
 *
 * @package Cherry\Form\FormElements
 */
class Link extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    parent::render($variables);

    return $this->renderer->setType('core.Form')
      ->setTemplate('link')
      ->render($variables);
  }

}
