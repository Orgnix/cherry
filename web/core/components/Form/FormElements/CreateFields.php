<?php

namespace Cherry\Form\FormElements;

use Cherry;
use Cherry\Form\FormElement;

/**
 * Class CreateFields
 *
 * @package Cherry\Form\FormElements
 */
class CreateFields extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    parent::render($variables);

    return $this->renderer->setType('core.Form')
      ->setTemplate('create_fields')
      ->render($variables);
  }

}
