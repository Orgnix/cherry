<?php

namespace Cherry\Form\FormElements;

use Cherry;
use Cherry\Form\FormElement;

/**
 * Class Wysiwyg
 *
 * @package Cherry\Form\FormElements
 */
class Wysiwyg extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    parent::render($variables);

    return $this->renderer->setType('core.Form')
      ->setTemplate('wysiwyg')
      ->render($variables);
  }

}
