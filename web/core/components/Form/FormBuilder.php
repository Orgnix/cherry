<?php

namespace Cherry\Form;

use Cherry;
use Cherry\Core;
use Cherry\Entity\EntityBundleableInterface;
use Cherry\Entity\EntityInterface;
use Cherry\Form\FormElements\Hidden;
use Cherry\Translation\StringTranslation;

/**
 * Class FormBuilder
 *
 * @package Cherry
 */
class FormBuilder {
  use StringTranslation;

  /** @var array $values */
  protected array $values;

  /** @var EntityInterface|null $entity */
  protected ?EntityInterface $entity = NULL;

  /**
   * Returns form elements in array format and fires an event.
   *
   * @return string
   */
  public function result(): string {
    $build = $this->build();
    $render = '<form method="post" name="form-' . $this->getId() . '">';
    $formIdElement = Core::loadClass(Hidden::class);
    $render .= $formIdElement->render([
      'formId' => $this->getId(),
      'key' => 'form-id',
      'attributes' => [
        'name' => 'form-id',
        'value' => $this->getId(),
      ],
    ]);
    if ($this->getEntity() instanceof EntityInterface) {
      $render .= $formIdElement->render([
        'formId' => $this->getId(),
        'key' => 'entity-type',
        'attributes' => [
          'name' => 'entity-type',
          'value' => $this->getEntity()->getType(),
        ],
      ]);
      if ($this->getEntity() instanceof EntityBundleableInterface
        && ($this->getEntity()->id() === NULL || $this->getEntity()->id() === 0)) {
        $render .= $formIdElement->render([
          'formId' => $this->getId(),
          'key' => 'bundle',
          'attributes' => [
            'name' => 'bundle',
            'value' => $this->getEntity()->bundle(),
          ],
        ]);
      }
      $render .= $formIdElement->render([
        'formId' => $this->getId(),
        'key' => 'entity-id',
        'attributes' => [
          'name' => 'entity-id',
          'value' => $this->getEntity()->id(),
        ],
      ]);
    }

    // Add values
    foreach ($build as $key => &$element) {
      if ($key === 'bundle' && $this->getId() !== 'entity-choose-bundle-form') {
        continue;
      }
      $element['key'] = $key;
      $element['formId'] = $this->getId();
      if ($key !== 'submit') {
        $element['attributes']['name'] = $key;
        if (isset($element['attributes']['multiple'])
          && ($element['attributes']['multiple'] === 'multiple' || $element['attributes']['multiple'] === TRUE)) {
          $element['attributes']['name'] .= '[]';
        }
      }
      $value = $this->getValue($key);
      if ($this->getEntity() instanceof EntityInterface) {
        $field = $this->getEntity()->getAllFields()[$key] ?? [];
        if (isset($field['serialize']) && $field['serialize'] === TRUE) {
          $value = unserialize($value);
        }
      }
      $element['attributes']['value'] = $value ?: NULL;
      if ($element['attributes']['value'] === NULL && isset($element['default_value'])) {
        $element['attributes']['value'] = $element['default_value'];
      }
      if ($key === 'bundle') {
        $element['attributes']['value'] = \Cherry::CurrentRoute()->getValue('bundle');
      }
    }

    // Remove element if it still exists from the last foreach
    if (isset($element)) {
      unset($element);
    }
    // Remove key if it still exists from the last foreach
    if (isset($key)) {
      unset($key);
    }

    \Cherry::Event('FormAlter')
      ->fire($build, [$this->getId()]);

    // Add to render array
    foreach ($build as $key => $element) {
      if ($key === 'bundle' && $this->getId() !== 'entity-choose-bundle-form') {
        continue;
      }
      $type = ucfirst($element['type']);
      /** @var FormElementInterface $className */
      $className = '\\Cherry\\Form\\FormElements\\' . $type;
      /** @var FormElement $elementClass */
      $elementClass = $className::create(\Cherry::Container());
      $render .= $elementClass->render($element);
    }

    $render .= '</form>';
    return $render;
  }

  /**
   * Builds the form before returning to requester.
   *
   * @return array
   */
  public function build(): array {
    $elements = [];
    foreach ($this->getFields() as $field => $values) {
      if (!isset($values['form'])) {
        continue;
      }
      $elements[$field] = $values['form'];
    }

    if ($this->getEntity() instanceof EntityInterface
      && $this->getId() !== 'entity-choose-bundle-form') {
      $this->addSubmitElement($elements);
    }

    return $elements;
  }

  /**
   * Returns array of fields.
   *
   * @return null|array
   */
  protected function getFields(): ?array {
    return NULL;
  }

  /**
   * Returns Form ID.
   *
   * @return null|string
   */
  protected function getId(): ?string {
    return NULL;
  }

  /**
   * @param string $key
   *
   * @return string|null
   */
  protected function getValue(string $key): ?string {
    return NULL;
  }

  /**
   * Adds submit element (only used on Entity forms)
   *
   * @param array $elements
   */
  protected function addSubmitElement(array &$elements) {
    $elements['submit'] = [
      'type' => 'button',
      'text' => $this->translate('Save', [], [], 'verb'),
      'attributes' => [
        'type' => 'submit',
      ],
      'classes' => [
        'btn-success'
      ],
    ];
  }

  /**
   * Returns entity object.
   *
   * @return EntityInterface|null
   */
  protected function getEntity(): ?EntityInterface {
    return $this->entity;
  }

  /**
   * Sets entity object.
   *
   * @param EntityInterface $entity
   *
   * @return Form
   */
  protected function setEntity(EntityInterface $entity): self {
    $this->entity = $entity;

    return $this;
  }

}
