<?php

namespace Cherry\Form;

use Cherry\Route\RouteInterface;
use Exception;
use Cherry;
use Cherry\Core;
use Cherry\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class Form
 *
 * @package Cherry\Form
 */
class Form extends FormBuilder implements FormInterface {

  /** @var array $values */
  protected array $values = [];

  /** @var array $fields */
  protected array $fields = [];

  /** @var string $id */
  protected string $id;

  /**
   * Form constructor.
   *
   * @param EntityInterface|null|bool $entity
   */
  public function __construct($entity = NULL) {
    if ($entity instanceof EntityInterface) {
      $this->setEntity($entity);
      $this->setFields($this->getEntity()->getAllFields());
      if ($entity->getValues() !== []) {
        $this->setId('entity-edit-form');
        $this->setValues($entity->getValues() ?: []);
      } else {
        $this->setId('entity-add-form');
      }
    }
  }

  /**
   * Fire events and submit handler
   *
   * @param array  $form
   * @param string $formId
   *
   * @return bool
   */
  public function submit(array &$form, string $formId): bool {
    // Fire FormPreSubmitAlter event
    $values = $_POST;
    \Cherry::Event('FormPreSubmitAlter')
      ->fire($values, [&$form, $formId]);

    $entity = $this->getEntity();

    if (!isset($form['submit']['handler'])
      && $entity !== NULL
      && $this->getId() !== 'entity-choose-bundle-form') {
      $fields = $entity->getAllFields();
      foreach ($values as $key => $value) {
        if (!$entity->hasField($key)) {
          continue;
        }
        if (isset($fields[$key]['serialize']) && $fields[$key]['serialize'] === TRUE) {
          $value = serialize($value);
        }
        $entity->setValue($key, $value);
      }

      return $entity->save();
    }

    $handler = $form['submit']['handler']
      ?? $form['submit']['form']['handler']
      ?? FALSE;

    // Fire submit handler
    if (!$handler) {
      return FALSE;
    }

    try {
      // Attempt to call the submit handler
      Core::Callback($handler, [&$form, $values]);
    } catch (Exception $e) {
      \Cherry::Logger()->addError($e->getMessage(), 'Form Submit');
    }

    return TRUE;
  }

  /**
   * Default submit handler does nothing because there is nothing to handle!
   *
   * @param array $form
   * @param array $values
   */
  public function submitForm(array $form, array $values = []) {
    // Template method
  }

  /**
   * {@inheritDoc}
   */
  protected function getFields(): array {
    return $this->fields;
  }

  /**
   * Sets fields array.
   *
   * @param array $values
   *
   * @return self
   */
  public function setFields(array $values): self {
    $this->fields = $values;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  protected function getValue(string $key): ?string {
    return $this->values[$key] ?? NULL;
  }

  /**
   * Returns array of values.
   *
   * @return array
   */
  protected function getValues(): array {
    return $this->values;
  }

  /**
   * Sets value array.
   *
   * @param array $values
   *
   * @return self
   */
  public function setValues(array $values): self {
    $this->values = $values;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  protected function getId(): string {
    return $this->id;
  }

  /**
   * Sets form ID.
   *
   * @param string $id
   *
   * @return self
   */
  public function setId(string $id): self {
    $this->id = $id;

    return $this;
  }

  /**
   * Redirect on submit.
   *
   * @param \Cherry\Route\RouteInterface|string $destination
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  protected function redirect($destination): RedirectResponse {
    if ($destination instanceof RouteInterface) {
      $destination_url = $destination->getUrl();
    } else {
      switch ($destination) {
        case '<self>':
          $route = \Cherry::CurrentRoute();
          $destination_url = $route->getUrl();
          break;
        case '<front>':
        default:
          $route = \Cherry::Container()->get('route')->load('dashboard');
          $destination_url = $route->getUrl();
          break;
      }
    }

    $response = new RedirectResponse($destination_url);
    return $response->send();
  }

  /**
   * Reloads page.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  protected function reload(): RedirectResponse {
    return $this->redirect('<self>');
  }

}