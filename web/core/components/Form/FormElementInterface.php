<?php

namespace Cherry\Form;

/**
 * Class FormElement
 *
 * @package Cherry\Form
 */
interface FormElementInterface {

  /**
   * Renders Form Element.
   *
   * @param array $variables
   *
   * @return string|null
   */
  public function render($variables = []);

}