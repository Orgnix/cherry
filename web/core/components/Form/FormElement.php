<?php

namespace Cherry\Form;

use Cherry\Container\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cherry\Renderer;

/**
 * Class FormElement
 *
 * @package Cherry\Form
 */
class FormElement implements FormElementInterface, ContainerInjectionInterface {

  /** @var Renderer $renderer */
  protected Renderer $renderer;

  /**
   * FormElement constructor.
   *
   * @param Renderer $renderer
   *   The renderer.
   */
  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritDoc}
   */
  public function render($variables = []) {
    \Cherry::Event('formElementPreRender')
      ->fire($variables);
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('renderer'));
  }

}
