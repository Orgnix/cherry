<?php

namespace Cherry\Form;

use Cherry\Entity\EntityInterface;
use Cherry\ExtensionManager\Extension;
use Cherry\ExtensionManager\ExtensionManagerInterface;
use Cherry\YamlReader;

/**
 * Class FormManager
 *
 * @package Cherry\Form
 */
class FormManager {

  /** @var ExtensionManagerInterface $extensionManager */
  protected ExtensionManagerInterface $extensionManager;

  /**
   * FormManager constructor.
   *
   * @param ExtensionManagerInterface $extension_manager
   */
  public function __construct(ExtensionManagerInterface $extension_manager) {
    $this->extensionManager = $extension_manager;
  }

  /**
   * Returns form object by form id
   *
   * @param string $formId
   *   The form ID.
   * @param EntityInterface|string|null $entity
   *   This can be either an entity or the entity type as a string.
   * @param null $entityId
   *   If the previous parameter was the entity type, you can enter
   *   an entity ID here so the entity will be loaded in the method.
   *
   * @return mixed
   */
  public function get(string $formId, $entity = NULL, $entityId = NULL) {
    $info = [];
    $extensions = $this->extensionManager->getInstalledExtensions();
    $core = YamlReader::readCore();
    foreach ($extensions as $extension) {
      $services = $extension->readFile();

      if (!is_array($services) || count($services) === 0) {
        continue;
      }

      if (!isset($services['forms'][$formId])) {
        continue;
      }

      $info = $services['forms'][$formId];
      break;
    }

    if (count($info) === 0 && isset($core['services'][$formId])) {
      $info = $core['forms'][$formId];
    } elseif (count($info) === 0) {
      throw new \Error('The form [' . $formId . '] is not declared.');
    }

    if (!class_exists($info['class'])) {
      throw new \Error('The form [' . $formId . '] class does note exist.');
    }

    if ($entity !== NULL && $entityId !== NULL && $entityId != 0) {
      if (!$entity instanceof EntityInterface) {
        $entity = \Cherry::EntityTypeManager()
          ->getStorage($entity)
          ->loadByProperties([
            'id' => $entityId,
          ]);
      }

      // Empty the entity's values, so we don't accidentally overwrite values we shouldn't alter.
      $entity->setValues(['id' => $entityId]);
      return new $info['class']($entity);
    } elseif ($entity !== NULL && $entityId == 0) {
      if (!$entity instanceof EntityInterface) {
        $entity = \Cherry::EntityTypeManager()
          ->getStorage($entity)
          ->getObject();
      }

      return new $info['class']($entity);
    }

    return new $info['class'];
  }

}
