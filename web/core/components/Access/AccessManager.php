<?php

namespace Cherry\Access;

use Cherry\Container\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cherry\Entity\EntityInterface;
use Cherry\Permission\PermissionManager;
use Cherry\Person\Entity\PersonInterface;
use Cherry\Route\RouteInterface;

/**
 * Class AccessManager.
 *
 * @package Cherry\Access
 */
class AccessManager implements ContainerInjectionInterface {

  /**
   * The current person.
   *
   * @var PersonInterface
   */
  protected PersonInterface $currentPerson;

  /**
   * The permission manager.
   *
   * @var PermissionManager
   */
  protected PermissionManager $permissionManager;

  /**
   * AccessManager constructor.
   *
   * @param PersonInterface   $current_person
   *   The current person.
   * @param PermissionManager $permissionManager
   *   The permission manager.
   */
  public function __construct(PersonInterface $current_person, PermissionManager $permissionManager) {
    $this->currentPerson = $current_person;
    $this->permissionManager = $permissionManager;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      \Cherry::CurrentPerson(),
      $container->get('permission.manager')
    );
  }

  /**
   * Checks whether current person has access to a given entity.
   *
   * @param EntityInterface $entity
   *   The entity object.
   * @param string          $action
   *   The action (e.g.: view, edit, delete, ...)
   *
   * @return bool
   */
  public function hasAccess(EntityInterface $entity, string $action = 'view'): bool {
    $permissions = $entity->getPermissions();
    if (isset($permissions[$action])) {
      if ($this->currentPerson->hasPermission($permissions[$action])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Checks whether current person has access to a given entity.
   *
   * @param RouteInterface $route
   *   The route object.
   *
   * @return bool
   */
  public function canVisit(RouteInterface $route): bool {
    $permissions = $route->getObject()->getPermissions();
    foreach ($permissions as $permission) {
      if (!$this->currentPerson->hasPermission($permission)) {
        return FALSE;
      }
    }
    return TRUE;
  }

}
