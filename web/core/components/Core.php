<?php

namespace Cherry;

use Exception;
use Cherry\Cache\CacheInterface;
use Cherry\Utils\Callback\Callback;
use Cherry\Utils\Strings\StringManipulation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Core
 *
 * @package Cherry
 */
class Core {

  /** @var array $disallowedEnvironmentKeys */
  protected static array $disallowedEnvironmentKeys = [
    'SCRIPT_FILENAME',
    'REMOTE_PORT',
    'SystemRoot',
    'PATH',
    'OPENSSL_CONF',
    'WINDIR',
    'PATHEXT',
    'COMSPEC',
  ];

  /**
   * Sets Cherry's exception handler.
   */
  public function setSystemSpecifics() {
    @set_exception_handler([$this, 'Exception']);
    Settings::setSettings();
  }

  /**
   * Logs exceptions through Cherry's Logger class.
   *
   * @param Exception $exception
   */
  public function Exception($exception) {
    \Cherry::Logger()->addError($exception->getMessage(), 'Exception', $exception->getTraceAsString());
    kint($exception);
    kint($exception->getTraceAsString());
  }

  /**
   * Generates random uuid.
   *
   * @return string
   */
  public static function createUUID() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000,
      mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
  }

  /**
   * Returns currently active Cache class.
   *
   * @return CacheInterface
   */
  public static function getCacheClass(ContainerInterface $container) {
    $cacheClass = Settings::get('cache_backend');
    $cacheClass = Core::loadClass($cacheClass);

    // Return default database Cache class in case the custom one is not an instance of CacheInterface.
    if (!$cacheClass instanceof CacheInterface) {
      return $container->get('cache.backend.db');
    }

    return $cacheClass;
  }

  /**
   * Returns environment variable
   *
   * @param string $key
   * @param bool   $allow_all
   *
   * @return mixed
   */
  public static function getEnv(string $key, $allow_all = TRUE) {
    if (!$allow_all) {
      if (in_array($key, static::$disallowedEnvironmentKeys)) {
        return FALSE;
      }
    }
    return $_ENV[$key];
  }

  /**
   * Returns environment variable
   *
   * @param string $key
   * @param mixed  $value
   *
   * @return mixed
   */
  public static function setEnv(string $key, $value) {
    $_ENV[$key] = $value;
    return $value;
  }

  /**
   * Returns path of core relative to web root
   *
   * @return string
   */
  public static function getCorePath(): string {
    return StringManipulation::replace( __DIR__, DIRECTORY_SEPARATOR . 'components', '');
  }

  /**
   * Returns path of core relative to web root
   *
   * @return string
   */
  public static function getCoreComponentsPath(): string {
    return static::getCorePath() . DIRECTORY_SEPARATOR . 'components';
  }

  /**
   * Returns the URL of the core extensions.
   *
   * @return string
   */
  public static function getCoreComponentsUrl(): string {
    return Settings::get('root.web.url') . '/core/components';
  }

  /**
   * Returns path of core extensions relative to core path
   *
   * @return string
   */
  public static function getCoreExtensionsPath(): string {
    return static::getCorePath() . DIRECTORY_SEPARATOR . 'extensions';
  }

  /**
   * Returns the URL of the core extensions.
   *
   * @return string
   */
  public static function getCoreExtensionsUrl(): string {
    return Settings::get('root.web.url') . '/core/extensions';
  }

  /**
   * Returns path of core extensions relative to core path
   *
   * @return string
   */
  public static function getExtensionsPath(): string {
    return str_replace(DIRECTORY_SEPARATOR . 'core ' . DIRECTORY_SEPARATOR . 'components', '', __DIR__) . DIRECTORY_SEPARATOR . 'extensions';
  }

  /**
   * @param array $items
   * @param array $params
   *
   * @return mixed
   *
   * @throws Queue\Exception\CallbackException
   */
  public static function Callback(array $items, array $params = []) {
    $callback = static::loadClass(Callback::class);
    $callback->setParameters($params);

    // Callback has class and method.
    if (count($items) > 1) {
      if (gettype($items[0]) === 'object') {
        $items[0] = get_class($items[0]);
      }
      $callback->setClass($items[0]);
      $callback->setMethod($items[1]);
      return $callback->execute();
    }
    // Callback is a plain function.
    elseif (count($items) === 1) {
      $callback->setMethod($items[0]);
      return $callback->execute();
    }

    return FALSE;
  }

  /**
   * Loads class.
   *
   * @param mixed $class
   *   Either the class (ClassExample::class) or service.
   *
   * @return object|bool
   */
  public static function loadClass($class, array $params = []) {
    if (is_string($class)) {
      if (\Cherry::isInitialized()) {
        $container = \Cherry::Container();
        if ($container->has($class)) {
          return $container->get($class);
        } elseif (class_exists($class)) {
          if (in_array('Cherry\Container\ContainerInjectionInterface', class_implements($class))) {
            return $class::create($container);
          } elseif (in_array('Cherry\Container\ArgumentsAwareContainerInjectionInterface', class_implements($class))) {
            return $class::create($container, $params);
          }

          return new $class(...$params);
        }
      } elseif (class_exists($class)) {
        return new $class(...$params);
      }
    } elseif (is_object($class)) {
      return $class;
    }

    return FALSE;
  }

}
