<?php

use Cherry\Language\Language;
use Cherry\Renderer;

/**
 * Include Settings
 */
require_once __DIR__ . '/components/Settings.php';

/** Require Cherry component */
require_once 'Cherry.php';

/**
 * Translation service, correct usage would be to enter a literal string, for example:
 *       $color = 'brown'; $itemColor = 'yellow'; $item = 'fence';
 *       StringTranslation::translate('The :color fox jumps over the :item_color :item', [':color' => $color,
 *       ':item_color' => $itemColor, ':item' => $item]); This ensures the proper handling of variables in string
 *       translations for dynamic reusage of the string. This method can also be used to stack translations, for
 *       example:
 *       $companySuffix = StringTranslation::translate('Incorporated');
 *       $companyName = StringTranslation::translate('myCompany :suffix', [':suffix' => $companySuffix]);
 *       StringTranslation::translate('Welcome to :company', [':company' => $companyName]);
 *
 * @param string $string
 * @param array $args
 * @param array $options
 * @param null $context
 *
 * @return string
 */
function translate(string $string, array $args = [], array $options = [], $context = NULL): string {
  /** @var Language $language */
  $language = \Cherry::Container()->get('language.manager')->getCurrentLanguage();
  return $language->translate($string, $args, $options, $context);
}

/**
 * Renders render array.
 *
 * @param array       $variables
 * @param string|null $view_mode
 * @param bool        $refactor
 *
 * @return string|NULL
 */
function render(array $variables = [], string $view_mode = NULL, bool $refactor = TRUE): ?string {
  /** @var Renderer $renderer */
  $renderer = \Cherry::Container()->get('renderer');
  return $renderer->render($variables, $view_mode, $refactor);
}

/**
 * Will attempt to serialize variable, and return bool based
 * on success or failure.
 *
 * @param $variable
 *   The variable you wish to test.
 *
 * @return bool
 *   Success or failure.
 */
function isSerializable($variable): bool {
  try {
    serialize($variable);
    return TRUE;
  } catch(Exception $e) {
    return FALSE;
  }
}


if (!\function_exists('kint')) {
  /**
   * Alias of Kint::dump().
   *
   * @return int|string
   */
  function kint() {
    if (\Cherry::isInitialized() &&
      \Cherry::CurrentPerson()->hasPermission('debug kint')) {
      \Cherry::KillSwitch()->trigger();
      $args = \func_get_args();
      return \call_user_func_array(array('Kint', 'dump'), $args);
    }
    return NULL;
  }

  Kint::$aliases[] = 'kint';
}
