<?php

namespace Cherry\Package;

use Cherry\Event\EventListener;

/**
 * Class Events
 *
 * @package Cherry\Package
 */
class Events extends EventListener {

  /** @var PackageManager $packageManager */
  protected PackageManager $packageManager;

  /**
   * Events constructor.
   *
   * @param PackageManager $package_manager
   */
  public function __construct(PackageManager $package_manager) {
    $this->packageManager = $package_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function addSearchResults(?array &$results, string $keyword) {
    $results['Packages'] =  $this->packageManager->searchPackage($keyword);
  }

}