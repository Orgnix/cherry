<?php

namespace Cherry\Package\Entity;

use Cherry\Entity\EntityInterface;

/**
 * Interface PackageInterface
 *
 * @package Cherry\Package
 */
interface PackageInterface extends EntityInterface {

  /**
   * Getter for the 'title' value
   *
   * @return mixed
   */
  public function getTitle();

  /**
   * Setter for the 'title' value
   *
   * @param string $title
   *
   * @return self
   */
  public function setTitle(string $title): self;

  /**
   * Getter for the 'storage' value
   *
   * @return mixed
   */
  public function getStorageSpace();

  /**
   * Setter for the 'storage' value
   *
   * @param string $storage
   *
   * @return self
   */
  public function setStorageSpace(string $storage): self;

  /**
   * Getter for the 'bandwidth' value
   *
   * @return mixed
   */
  public function getBandwidth();

  /**
   * Setter for the 'bandwidth' value
   *
   * @param string $bandwidth
   *
   * @return self
   */
  public function setBandwidth(string $bandwidth): self;

}
