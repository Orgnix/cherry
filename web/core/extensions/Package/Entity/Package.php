<?php

namespace Cherry\Package\Entity;

use Cherry\Entity\Entity;

/**
 * Class Package
 *
 * @package Cherry\Package
 */
class Package extends Entity implements PackageInterface {

  /**
   * Package constructor.
   *
   * @param null|array $values
   */
  public function __construct($values = NULL) {
    $this->setValues($values);
    $this->setType('package');
  }

  /**
   * @return array
   */
  public static function initialFields(): array {
    return [
      'title' => [
        'type' => 'varchar',
        'length' => 255,
        'default_value' => '',
      ],
      'storage' => [
        'type' => 'varchar',
        'length' => 255,
        'default_value' => '1 GB',
      ],
      'bandwidth' => [
        'type' => 'varchar',
        'length' => 255,
        'default_value' => '50 GB',
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle() {
    $this->getValue('title');
  }

  /**
   * {@inheritDoc}
   */
  public function setTitle(string $title): self {
    return $this->setValue('title', $title);
  }

  /**
   * {@inheritDoc}
   */
  public function getStorageSpace() {
    $this->getValue('storage');
  }

  /**
   * {@inheritDoc}
   */
  public function setStorageSpace(string $storage): self {
    return $this->setValue('storage', $storage);
  }

  /**
   * {@inheritDoc}
   */
  public function getBandwidth() {
    $this->getValue('bandwidth');
  }

  /**
   * {@inheritDoc}
   */
  public function setBandwidth(string $bandwidth): self {
    return $this->setValue('bandwidth', $bandwidth);
  }

}
