<?php

namespace Cherry\Package;

use Cherry\Database\Result;
use Cherry\Package\Entity\Package;
use Cherry\Package\Entity\PackageInterface;

/**
 * Class PackageManager
 *
 * @package Cherry\Package
 */
class PackageManager {

  /**
   * Returns package
   *
   * @param int $id
   *
   * @return bool|PackageInterface
   */
  public function getPackage(int $id) {
    return Package::load($id);
  }

  /**
   * Finds package by given string
   *
   * @param string $string
   *
   * @return array
   */
  public function searchPackage(string $string): array {
    $string = '%' . $string . '%';
    $query = \Cherry::Database()
      ->select('entity__package')
      ->fields(NULL, ['id'])
      ->condition('title', $string, 'LIKE')
      ->orderBy('id', 'DESC')
      ->limit(0, 10)
      ->execute();
    if (!$query instanceof Result) {
      return [];
    }

    $items = [];
    $results = $query->fetchAllAssoc();
    foreach ($results as $result) {
      $items[$result['id']] = $this->getPackage($result['id']);
    }

    return $items;
  }

}
