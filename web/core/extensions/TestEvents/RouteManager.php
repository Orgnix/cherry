<?php

namespace Cherry\TestEvents;

use Cherry\ExtensionManager\ExtensionManagerInterface;
use Cherry\Route\RouteManager as RouteManagerBase;

/**
 * Overrides the default RouteManager.
 */
class RouteManager extends RouteManagerBase {

  /**
   * {@inheritDoc}
   */
  public function __construct(ExtensionManagerInterface $extension_manager) {
    parent::__construct($extension_manager);

    d('Overridden route.manager service as a demo in \Cherry\TestEvents\RouteManager', $this);
  }

}
