<?php

namespace Cherry\TestEvents;

use Cherry\Event\EventListener;

/**
 * Class TestEvents
 *
 * @package Cherry\TestEvents
 */
class TestEvents extends EventListener {

  /**
   * {@inheritDoc}
   */
  public function FormPreSubmitAlter(array &$values, array $form, string $form_id) {
    kint($values);
    kint($form);
    kint($form_id);
  }

  /**
   * {@inheritDoc}
   */
  public function FormAlter(?array &$form, string $form_id) {
    kint($form);
    kint($form_id);
  }

  /**
   * {@inheritDoc}
   */
  public function preRender(?array &$variables, ?string $view_mode) {
    // Check whether page has an id (some don't!)
    if (!isset($variables['page']['id'])) {
      return;
    }

    // Check whether page ID matches the one we wish to alter
    if ($variables['page']['id'] != 'dashboard') {
      return;
    }

    // Change the page title to add a test message
    $variables['page']['title'] = $variables['page']['title'] . " - TEST!! Turn off the TestEvents module to remove this example.";
  }

}
