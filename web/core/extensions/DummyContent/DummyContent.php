<?php

namespace Cherry\DummyContent;

use Cherry;
use Cherry\Article\Entity\Article;
use Cherry\Entity\Entity;
use Cherry\Page\Page;
use Cherry\Person\Entity\Person;
use Cherry\Route\RouteInterface;

/**
 * Class DummyContent
 *
 * @package Cherry\DummyContent
 */
class DummyContent extends Page {

  /**
   * DummyContent constructor.
   */
  public function __construct(array &$parameters, RouteInterface $route, \Cherry\Renderer $renderer) {
    parent::__construct($parameters, $route, $renderer);
    $this->setParameters([
      'id' => 'dummycontent',
      'title' => $this->translate('DummyContent'),
      'summary' => $this->translate('Creates some dummy content, users, ...'),
    ]);
  }

  /**
   *
   */
  public function createPerson() {
    $extensionManager = \Cherry::Container()->get('extension.manager');
    if (!$extensionManager->extensionInstalled('Person')) {
      return 'Person extension is not installed.';
    }
    $person = new Person([
      'status' => Entity::PUBLISHED,
      'owner' => 1,
      'name' => 'Admin',
      'password' => '$2y$10$vw4KCNOucAF4bjcTsrnIZO7/KAtWBHj9bMKGy4U4riVvOyZ9dLi4e',
    ]);
    $person->save();
    return 'Created new person \'Admin\'.';
  }

  public function createArticle() {
    $extensionManager = \Cherry::Container()->get('extension.manager');
    if (!$extensionManager->extensionInstalled('Article')) {
      return 'Article extension is not installed.';
    }
    $article = new Article([
      'status' => Entity::PUBLISHED,
      'owner' => 1,
      'title' => 'My first article',
      'body' => 'This is my first article!
      
      Lorem ipsum, and what not :-)',
    ]);
    $article->save();
    return 'Created new article \'My first article\'';
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheOptions(): self {
    $this->caching = [
      'key' => 'page.' . $this->get('id'),
      'context' => 'page',
      'max-age' => 0,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    parent::render();

    $defaultMessage = 'Do you wish to create dummy content?';
    $message = $defaultMessage;
    $confirm = FALSE;
    if ($this->hasParameter('confirm')) {
      $confirm = TRUE;
      if ($this->hasParameter('t')) {
        if ($this->get('t') == 'person') {
          $message = $this->createPerson();
        } elseif ($this->get('t') == 'article') {
          $message = $this->createArticle();
        }
      }
    }

    return Cherry::Container()->get('renderer', FALSE)
      ->setType('extension.DummyContent')
      ->setTemplate($this->get('id'))
      ->render([
        'page' => [
          'id' => $this->get('id'),
          'title' => $this->get('title'),
          'summary' => $this->get('summary'),
        ],
        'message' => $message,
        'showButtons' => $message === $defaultMessage,
        'buttons' => ['article' => 'Article', 'person' => 'Person'],
        'confirm' => $confirm,
      ]);
  }

}