<?php

namespace Cherry\DummyContent;

use Cherry\ExtensionManager\InstallInterface;
use Cherry\Menu\Entity\Menu;
use Cherry\Menu\Entity\MenuInterface;

/**
 * Class Install
 *
 * @package Cherry\DummyContent
 */
class Install implements InstallInterface {

  /**
   * Checks whether the page already exists
   *
   * @return bool
   */
  public function uninstall(): bool {
    $menus = \Cherry::EntityTypeManager()
      ->getStorage('menu')
      ->loadByProperties([
        'route' => 'dummycontent'
      ]);

    /** @var MenuInterface $menu */
    foreach ($menus as $menu) {
      if (!$menu->delete()) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Creates page in database.
   *
   * @return bool
   */
  public function install(): bool {
    $menu = new Menu([
      'route' => 'dummycontent',
      'title' => 'Dummy Content',
      'description' => 'Create dummy content',
      'structure' => 0,
      'type' => 'link',
      'translatable' => TRUE,
      'parent' => 0,
      'owner' => 1,
      'status' => 1,
    ]);
    if (!$menu->save()) {
      return FALSE;
    }
    \Cherry::Logger()->addInfo('Added menu item for DummyContent', 'DummyContent');

    return TRUE;
  }

}