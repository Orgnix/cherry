<?php

namespace Cherry\Conversation;

use Cherry;
use Cherry\Element\ElementInterface;
use Cherry\Event\EventListener;

/**
 * Class Events
 *
 * @package Cherry\Conversation
 */
class Events extends EventListener {

  /**
   * {@inheritDoc}
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element) {
    if ($element_id !== 'header') {
      return;
    }

    $variables['elements']['navbar']['conversations'] = [
      'render' => \Cherry::Container()->get('renderer')
        ->setType('extension.Conversation')
        ->setTemplate('conversation')
        ->render($variables),
      'weight' => -25,
      ];
  }

}
