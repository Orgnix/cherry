<?php

namespace Cherry\Conversation\Entity;

use Cherry\Entity\EntityInterface;
use Cherry\Person\Entity\PersonInterface;

/**
 * Interface ConversationInterface
 *
 * @package Cherry\Conversation
 */
interface ConversationInterface extends EntityInterface {

  /**
   * Gets the initiator of the conversation
   *
   * @return mixed
   */
  public function getSender();

  /**
   * Sets the initiator of the conversation
   *
   * @param PersonInterface $sender
   *
   * @return mixed
   */
  public function setSender(PersonInterface $sender);

  /**
   * Gets the receiver of the conversation
   *
   * @return mixed
   */
  public function getReceiver();

  /**
   * Sets the receiver of the conversation
   *
   * @param PersonInterface $receiver
   *
   * @return mixed
   */
  public function setReceiver(PersonInterface $receiver);

}