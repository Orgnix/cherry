<?php

namespace Cherry\Shield;

use Cherry\Request;
use Cherry\Utils\Arrays\ArrayManipulation;
use Cherry\Event\EventListener;
use Cherry\Settings;

/**
 * Class Events
 *
 * @package Cherry\Shield
 */
class Events extends EventListener {

  /**
   * {@inheritDoc}
   */
  public function postInit(Request $request) {
    // Don't activate shield in CLI environments.
    if (defined('STDIN')) {
      return;
    }
    $valid_passwords = Settings::get('shield', ['cherry' => 'cherry']);
    $valid_users = array_keys($valid_passwords);

    $user = $_SERVER['PHP_AUTH_USER'] ?? '';
    $pass = $_SERVER['PHP_AUTH_PW'] ?? '';

    $validated = ArrayManipulation::contains($valid_users, $user) && $pass == $valid_passwords[$user];

    if (!$validated) {
      header('WWW-Authenticate: Basic realm="Cherry\'s restricted area."');
      header('HTTP/1.0 401 Unauthorized');

      die(translate('Not authorized'));
    }
  }

}
