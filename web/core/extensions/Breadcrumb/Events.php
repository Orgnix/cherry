<?php

namespace Cherry\Breadcrumb;

use Cherry;
use Cherry\Utils\Arrays\ArrayManipulation;
use Cherry\Element\ElementInterface;
use Cherry\Event\EventListener;
use Cherry\Renderer;
use Cherry\Route\RouteManager;
use Cherry\Settings;
use Cherry\Utils\Strings\StringManipulation;
use Cherry\Translation\StringTranslation;

/**
 * Class Breadcrumb
 *
 * @package Cherry\Breadcrumb
 */
class Events extends EventListener {
  use StringTranslation;

  /** @var RouteManager $routeManager */
  protected RouteManager $routeManager;

  /** @var Renderer $renderer */
  protected Renderer $renderer;

  /**
   * Breadcrumb constructor.
   *
   * @param RouteManager $route_manager
   *   The route manager.
   * @param Renderer $renderer
   *   The renderer instance.
   */
  public function __construct(RouteManager $route_manager, Renderer $renderer) {
    $this->routeManager = $route_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritDoc}
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element) {
    if ($element_id !== 'header') {
      return;
    }

    $current_route = \Cherry::CurrentRoute();
    $home = $this->translate('Home');

    $items_raw = StringManipulation::explode($current_route->getUri(), '/');
    $items_raw = [$home] + ArrayManipulation::removeEmptyEntries($items_raw);
    $last_item = end($items_raw);
    $items = [];

    $url = '';
    foreach ($items_raw as $item) {
      if ($item !== $home) {
        $url = $url . '/' . $item;
      }

      // capitalize keys by default
      // @TODO: make this optional (a setting?)
      $key = StringManipulation::capitalize($item);

      $items[$key] = [
        'url' => $item === 'Home' ? Settings::get('root.web.url') . '/' : Settings::get('root.web.url') . $url,
        'link' => $item !== $last_item && !($item === 'Home' && $current_route->getRoute() === 'dashboard'),
      ];

      $uri = StringManipulation::replace($items[$key]['url'], Settings::get('root.web.url'), '');

      $item_route = $this->routeManager->routeMatch($uri);
      if ($item_route->getRoute() === 'error') {
        $items[$key]['link'] = FALSE;
      }
    }

    $variables['elements']['page_top']['breadcrumb'] = [
      'render' => $this->renderer
        ->setType('extension.Breadcrumb')
        ->setTemplate('breadcrumb')
        ->render(['items' => $items]),
      'weight' => -40,
    ];
  }
}