<?php

namespace Cherry\Environment;

use Cherry\Settings;

/**
 * Class Environment
 *
 * @package Cherry\Environment
 */
class Environment {

  /** @var string|null $environment */
  protected ?string $environment = NULL;

  /** @var array $changes */
  protected array $changes = [];

  /**
   * Environment constructor.
   *
   * @param string|null $environment
   */
  public function __construct(?string $environment = NULL) {
    $this->setEnvironment($environment ?? $this->getEnvironment());
    $this->setChanges([]);
  }

  /**
   * Returns environment
   *
   * @return array|false|mixed
   */
  public function getEnvironment() {
    return $this->environment
      ?: Settings::get('environment')
      ?: \Cherry::Container()->get('config')->get('site.environment')
      ?: 'master';
  }

  /**
   * Sets environment in object
   *
   * @param string $environment
   *
   * @return self
   */
  public function setEnvironment(string $environment): Environment {
    $this->environment = $environment;

    return $this;
  }

  /**
   * Returns last git changes.
   *
   * @return array|null
   */
  public function getChanges(): ?array {
    return $this->changes;
  }

  /**
   * Sets changes array
   *
   * @param array $changes
   *
   * @return self
   */
  public function setChanges(array $changes = []): Environment {
    $this->changes = $changes;

    return $this;
  }

}