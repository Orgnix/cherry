<?php

namespace Cherry\Environment;

use Cherry\ExtensionManager\InstallInterface;

class Install implements InstallInterface {

  /**
   * Invalidate footer element.
   */
  public function install() {
    \Cherry::Cache()->invalidateTags(['element:footer']);
  }

  /**
   * Invalidate footer element.
   */
  public function uninstall() {
    \Cherry::Cache()->invalidateTags(['element:footer']);
  }

}
