<?php

namespace Cherry\Environment;

use Cherry\Container\PrepareBuilder;
use Cherry\Core;
use Cherry\Element\ElementInterface;
use Cherry\Event\EventListener;
use Symfony\Component\Config\FileLocator;

/**
 * Class Events
 *
 * @package Cherry\Environment
 */
class Events extends EventListener {

  /** @var Environment $environment */
  protected Environment $environment;

  /**
   * Events constructor.
   */
  public function __construct() {
    $this->environment = new Environment();
  }

  /**
   * {@inheritDoc}
   */
  public function elementPreRender(?array &$variables, string $element_id, ElementInterface &$element) {
    if (\Cherry::CurrentPerson()->id() == 0) {
      return;
    }

    if (!isset($element_id)) {
      return;
    }

    switch ($element_id) {
      case 'footer':
        $variables['elements']['page_bottom']['environment'] = [
          'render' => \Cherry::Container()->get('renderer')
            ->setType('extension.Environment')
            ->setTemplate('bar')
            ->render([
              'environment' => $this->environment->getEnvironment(),
              'last_changes' => $this->environment->getChanges(),
            ]),
          'weight' => -25,
        ];
        break;
      case 'header':
        $variables['css'][] = Core::getCoreExtensionsUrl() . '/Environment/css/bar.css';
        break;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function siteSettingsFormPreBuild(?array &$fields) {
    $default_value = \Cherry::Container()->get('config')->get('site.environment') ?? 'master';
    $fields['fields']['environment'] = [
      'form' => [
        'type' => 'select',
        'title' => $this->translate('Environment'),
        'default_value' => $default_value,
        'options' => [
          'master' => 'Master',
          'release' => 'Release',
          'develop' => 'Develop',
        ],
      ],
    ];
  }

  /**
   * @param PrepareBuilder $builder
   *
   * @return mixed|void
   *
   * @throws \Exception
   */
  public function prepareContainer(PrepareBuilder $builder) {
    $environment = \Cherry::Container()->get('config')->get('site.environment') ?? 'master';
    if (strpos($environment, 'develop') !== FALSE) {
      $path = Core::getCoreExtensionsPath() . DIRECTORY_SEPARATOR . 'Environment';
      $locator = new FileLocator($path);
      $builder->load('development.services.yml', $locator);
    }
  }

}